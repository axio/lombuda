package at.lombuda.entities.enums;

import org.apache.jena.rdf.model.Resource;

import at.lombuda.daos.consts.voc.LombudaVoc;

public enum CodeListType {
    APPROACHES("approach", LombudaVoc.LIST_APPROACHES), ACCOUNTS("account", LombudaVoc.LIST_APPROACHES), BPI("budget profile index", LombudaVoc.LIST_BPI);

    private String singularName;
    private Resource resource;

    private CodeListType(String singularName, Resource resource) {
        this.singularName = singularName;
        this.resource = resource;
    }

    public String getSingularName() {
        return singularName;
    }

    public Resource getResource() {
        return resource;
    }
}

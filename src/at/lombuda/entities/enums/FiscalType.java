package at.lombuda.entities.enums;

import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;

import at.lombuda.daos.consts.voc.LombudaVoc;

public enum FiscalType {
    /**
     * revenue or - in German "<B>Einnahme</B>"
     */
    REV("Revenue", "Einnahme"), 

    /**
     * expenditure or - in German "<B>Ausgabe</B>"
     */
    EXP("Expenditure", "Ausgabe");

    private Resource resource;
    private String humanReadableNameEn;
    private String humanReadableNameDe;

    private FiscalType(String humanReadableNameEn, String humanReadableNameDe) {
        resource = ResourceFactory.createResource( LombudaVoc.NS_FT_CODES + this.name() ); 
        this.humanReadableNameEn = humanReadableNameEn;
        this.humanReadableNameDe = humanReadableNameDe;
    }

    /**
     * @return The dereferenceable RDF resource for this fiscal type
     */
    public Resource getResource() {
        return resource;
    }

    /**
     * @return the human readable name for this fiscal type in English language
     */
    public String getHumanReadableNameEn() {
        return humanReadableNameEn;
    }

    /**
     * @return the human readable name for this fiscal type in German language
     */
    public String getHumanReadableNameDe() {
        return humanReadableNameDe;
    }
}
package at.lombuda.entities.enums;

import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;

import at.lombuda.daos.consts.voc.LombudaVoc;

public enum FiscalState {
    /**
     * budget or - in German "<B>Voranschlag</B>"
     */
    VA("Budget", "Voranschlag"), 

    /**
     * statement of account or - in German "<B>Rechnungsabschluss</B>"
     */
    RA("Statement of Account", "Rechnungsabschluss");

    private Resource resource;
    private String humanReadableNameEn;
    private String humanReadableNameDe;

    private FiscalState(String humanReadableNameEn, String humanReadableNameDe) {
        resource = ResourceFactory.createResource( LombudaVoc.NS_FS_CODES + this.name() ); 
        this.humanReadableNameEn = humanReadableNameEn;
        this.humanReadableNameDe = humanReadableNameDe;
    }

    /**
     * @return The dereferenceable RDF resource for this fiscal state
     */
    public Resource getResource() {
        return resource;
    }

    /**
     * @return the human readable name for this fiscal state in English language
     */
    public String getHumanReadableNameEn() {
        return humanReadableNameEn;
    }

    /**
     * @return the human readable name for this fiscal state in German language
     */
    public String getHumanReadableNameDe() {
        return humanReadableNameDe;
    }
}
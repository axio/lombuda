package at.lombuda.entities.enums;

public enum MessageType {
	STANDARD("",""), SUCCESS, WARNING, INFO, ALERT, SECONDARY;
	
	private String alertStyleClassName;
	private String labelStyleClassName;
	
	private MessageType() {
		this.alertStyleClassName = this.labelStyleClassName = this.name().toLowerCase();
	}
	
	private MessageType(String alertStyleClassName, String labelStyleClassName) {
		this.alertStyleClassName = alertStyleClassName;
		this.labelStyleClassName = labelStyleClassName;
	}
	
	public String getAlertStyleClassName() {
		return alertStyleClassName;
	}
	
	public String getLabelStyleClassName() {
		return labelStyleClassName;
	}
}

package at.lombuda.entities;

/**
 * This class holds the counted number of
 * a specific kind of code after statistical
 * analysis of the backed dataset 
 */
public class CodeCount {
    private String codeNotation;
    private int count;

    /**
     * @param codeNotation the given notation of a code (should be 
     *        its singular name in capital letters, e.g. "APPROACH")
     * @param count the number of counted distinct codes within the
     *        backed dataset
     */
    public CodeCount(String codeNotation, int count) {
        this.codeNotation = codeNotation;
        this.count = count;
    }

    /**
     * @return the given notation of a code (should be its singular
     *         name in capital letters, e.g. "APPROACH")
     */
    public String getCodeNotation() {
        return codeNotation;
    }

    /**
     * @return the name of a code (singular, in lower case)
     */
    public String getCodeName() {
        return codeNotation.toLowerCase().replace('_', ' ');
    }

    /**
     * @return the number of counted distinct codes within the
     *         backed dataset
     */
    public int getCount() {
        return count;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "CodeCount [" + codeNotation + " = " + count + "]";
    }
}
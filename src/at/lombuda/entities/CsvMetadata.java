package at.lombuda.entities;

import java.util.EnumMap;

/**
 * Metadata about data available as CSV incl.
 * column names or, in case some columns are
 * missing, their fixed values.
 */
public class CsvMetadata {
    private BCE budgetaryClassificationEncoding;
    private EnumMap<ATTR, String> fixedValues;
    private EnumMap<ATTR, String> columnNames;

    /**
     * Creates a new metadata set for a given CSV.
     * The metadata is initially empty and inconsistent.
     * In order to fill it properly, following steps
     * should be followed:
     * <ul>
     * <li>Add fixed values for the given CSV with 
     *     {@link #setFixedValueFor(ATTR, String)}
     * <li>Set the budgetary classification encoding
     *     in the given CSV with 
     *     {@link #setBudgetaryClassificationEncoding(BCE)}
     * <li>Add column names of the given CSV with
     *     {@link #setColumnNameFor(ATTR, String)}
     * <li>Check the metadata set for consistency with
     *     {@link #isConsistent()}
     * </ul>
     */
    public CsvMetadata() {
        // create maps
        fixedValues = new EnumMap<ATTR, String>(ATTR.class);
        columnNames = new EnumMap<ATTR, String>(ATTR.class);
    }

    /**
     * @param bce How the budgetary classifications are encoded as CSV
     */
    public void setBudgetaryClassificationEncoding(BCE bce) {
        this.budgetaryClassificationEncoding = bce;
    }

    /**
     * Sets a fixed value for a given attribute, iff
     * the value is not <code>null</code> or empty.
     * Leading and trailing whitespace is removed 
     * automatically.
     * 
     * @param attribute an attribute
     * @param value its fixed value for this metadata
     * 
     * @throws IllegalStateException If a column name is 
     *         already present for the given attribute in 
     *         this metadata
     */
    public void setFixedValueFor(ATTR attribute, String value) {
        if(hasColumnNameFor(attribute))
            throw new IllegalStateException(
                    "Cannot accept fixed value for present column of attribute '"+attribute+"': "
                            +getColumnNameFor(attribute));

        if(value!= null && !value.trim().isEmpty())
            fixedValues.put(attribute, value.trim());
    }

    /**
     * Sets the column name for a given attribute, iff
     * the value is not <code>null</code> or empty.
     * Leading and trailing whitespace is removed 
     * automatically.
     * 
     * @param attribute an attribute
     * @param value its column name in the CSV
     * 
     * @throws IllegalStateException If a fixed value is 
     *         already present for the given attribute in 
     *         this metadata
     */
    public void setColumnNameFor(ATTR attribute, String value) {
        if(hasFixedValueFor(attribute))
            throw new IllegalStateException(
                    "Cannot accept column name for present fixed value of attribute '"+attribute+"': "
                            +getFixedValueFor(attribute));
        
        if(value!= null && !value.trim().isEmpty())
            columnNames.put(attribute, value.trim());
    }

    /**
     * @param attribute an attribute
     * @return Whether a fixed value for the given
     *         attribute exists in this metadata or
     *         not. If not, the given attribute 
     *         <i>should</i> be present as a column
     *         name.
     */
    public boolean hasFixedValueFor(ATTR attribute) {
        return fixedValues.get(attribute) != null;
    }

    /**
     * @param attribute an attribute
     * @return A fixed value for this attribute that 
     *         is not contained in the given CSV. 
     *         Otherwise the result is <code>null</code> 
     *         and the given attribute <i>should</i> be
     *         present as a column.
     */
    public String getFixedValueFor(ATTR attribute) {
        return fixedValues.get(attribute);
    }

    /**
     * @param attribute an attribute
     * @return Whether a column name for the given
     *         attribute exists in this metadata or
     *         not. If not, the given attribute 
     *         <i>should</i> be present as a fixed
     *         value.
     */
    public boolean hasColumnNameFor(ATTR attribute) {
        return columnNames.get(attribute) != null;
    }

    /**
     * @param attribute an attribute
     * @return The column name for this attribute in 
     *         the CSV, if it exists. Otherwise the 
     *         result is <code>null</code> and the 
     *         given attribute <i>should</i> be
     *         present as a fixed value in this
     *         metadata.
     */
    public String getColumnNameFor(ATTR attribute) {
        return columnNames.get(attribute);
    }

    /**
     * @return How the budgetary classifications are 
     *         encoded as CSV
     */
    public BCE getBudgetaryClassificationEncoding() {
        return budgetaryClassificationEncoding;
    }

    /**
     * @return Whether the budgetary classifications are 
     *         encoded in a combined way in the CSV or not
     */
    public boolean hasCombinedBce() {
        return budgetaryClassificationEncoding == BCE.combined;
    }

    /**
     * @return Whether the budgetary classifications are 
     *         separated encoded as CSV or not
     */
    public boolean hasSeparatedBce() {
        return budgetaryClassificationEncoding == BCE.separated;
    }

    /**
     * Checks for existence of a definition for the given 
     * attribute in this metadata (a column name or a fixed
     * value).
     * 
     * @param attribute the attribute that's definition 
     *         should be checked
     * @return Whether the given attribute has a definition 
     *         in this metadata or not
     */
    public boolean hasAttributeDefinition(ATTR attribute) {
        return hasColumnNameFor(attribute) || hasFixedValueFor(attribute);
    }

    /**
     * Checks this metadata for consistency in a semantic way.
     * 
     * @return Whether this metadata is consistent or not
     */
    public boolean isConsistent() {
        if(budgetaryClassificationEncoding == null)
            return false;

        for(ATTR attribute:ATTR.values()) {
            if(!attribute.isOptional() && !hasAttributeDefinition(attribute))
                return false;
        }

        if(budgetaryClassificationEncoding == BCE.combined) {
            if(!hasAttributeDefinition(ATTR.budgetaryClass))
                return false;
        } else if (budgetaryClassificationEncoding == BCE.separated) {
            if(!hasAttributeDefinition(ATTR.functionalClass) || !hasAttributeDefinition(ATTR.economicClass) || 
                    !(hasAttributeDefinition(ATTR.budgetaryIndicator) || hasAttributeDefinition(ATTR.fiscalType)))
                return false;
        } else
            return false;

        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "The given "+(isConsistent()?"consistent":"INCONSISTENT")+" metadata for a CSV file consists of:\n"
                + "   BCE          = "+ budgetaryClassificationEncoding + "\n"
                + "   fixed values = "+ fixedValues + "\n"
                + "   column names = " + columnNames;
    }

    /**
     * All attributes that can be contained as CSV
     */
    public enum ATTR {
        sourceUrl, sourceDesc, fiscalYear, municipality, fiscalState,
        budgetaryClass(true), budgetaryIndicator(true), fiscalType(true), functionalClass(true), economicClass(true), 
        amount, description(true);

        private boolean optional;
        private ATTR() {
            optional = false;
        }

        private ATTR(boolean isOptional) {
            optional = isOptional;
        }

        /**
         * @return Whether this attribute can be considered as optional
         */
        public boolean isOptional() {
            return optional;
        }
    }

    /**
     * The budgetary classification encoding of the individual data rows
     */
    public enum BCE {
        combined, separated
    }
}
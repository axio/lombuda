package at.lombuda.entities;

import java.math.BigDecimal;

/**
 * Instances of this class represent a single aggregation 
 * within a budget profile (German 'Rechnungsquerschnitt') 
 * of a municipality according to Austria's VRV
 */
public class BudgetProfileAggregation {
    private BPAType type;
    private String index;
    private String label;
    private BigDecimal amount = new BigDecimal("0");
    private BigDecimal amountForPublicCorporations = new BigDecimal("0");

    /**
     * Constructs a budget profile aggregation without 
     * any sums (they are initially <code>0</code>).
     * 
     * @param type The type of this aggregation
     * @param index The index of this aggregation 
     *              according to the VRV
     * @param label The label of this aggregation
     *              according to the VRV
     */
    public BudgetProfileAggregation(BPAType type, String index,
            String label) {
        this.type = type;
        this.index = index;
        this.label = label;
    }

    /**
     * @return The type of this aggregation
     */
    public BPAType getType() {
        return type;
    }

    /**
     * @return The index of this aggregation 
     *         according to the VRV
     */
    public String getIndex() {
        return index;
    }

    /**
     * @return The label of this aggregation
     *         according to the VRV
     */
    public String getLabel() {
        return label;
    }

    /**
     * @return The calculated amount of this 
     *         aggregation
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * @param amount The calculated amount of 
     *               this aggregation to set
     */
    public void setAmount(double amount) {
        this.amount = new BigDecimal(""+amount);
    }

    /**
     * @param amount The calculated amount of 
     *               this aggregation to set
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * @return The calculated amount of this
     *         aggregation for public corporations 
     *         only
     */
    public BigDecimal getAmountForPublicCorporations() {
        return amountForPublicCorporations;
    }

    /**
     * @param amountForPublicCorporations The calculated amount of 
     *        this aggregation for public corporations only to set
     */
    public void setAmountForPublicCorporations(
            double amountForPublicCorporations) {
        this.amountForPublicCorporations = new BigDecimal(""+amountForPublicCorporations);
    }

    /**
     * @param amountForPublicCorporations The calculated amount of 
     *        this aggregation for public corporations only to set
     */
    public void setAmountForPublicCorporations(
            BigDecimal amountForPublicCorporations) {
        this.amountForPublicCorporations = amountForPublicCorporations;
    }

    /**
     * @return The calculated amount of this
     *         aggregation without public 
     *         corporations only
     */
    public BigDecimal getAmountWithoutPublicCorporations() {
        return amount.subtract(amountForPublicCorporations);
    }

    public enum BPAType {
        LINE, SUM, B2, B1, H2, H1;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "#" + index + " - " + label+((type != BPAType.H1 && type != BPAType.H2)?
                " = " + String.format("%,.2f", amount) + " (thereof A85-89 = " + String.format("%,.2f", amountForPublicCorporations) + ")":"");
    }
}
package at.lombuda.entities;

/**
 * This class represents integrity constraints that can be checked
 * via SPARQL queries and which's results can be stored in instances
 * of the respective IntegrityConstraint.
 */
public class IntegrityConstraint {
    private String label;
    private String description;
    private String sparqlQuery;
    private boolean violated;
    private boolean checked;

    /**
     * @param label       The label of this integrity constraint
     * @param description The description of this integrity constraint
     * @param sparqlQuery The corresponding SPARQL query for this 
     *                    integrity constraint
     */
    public IntegrityConstraint(String label, String description,
            String sparqlQuery) {
        this.label = label;
        this.description = description;
        this.sparqlQuery = sparqlQuery;
        this.checked = false;
    }

    /**
     * @return The label of this integrity constraint
     */
    public String getLabel() {
        return label;
    }

    /**
     * @return The description of this integrity constraint
     */
    public String getDescription() {
        return description;
    }

    /**
     * @return The corresponding SPARQL query for this 
     *         integrity constraint
     */
    public String getSparqlQuery() {
        return sparqlQuery;
    }

    /**
     * @param sparqlQuery The corresponding SPARQL query for 
     *                    this integrity constraint to set
     */
    public void setSparqlQuery(String sparqlQuery) {
        this.sparqlQuery = sparqlQuery;
    }

    /**
     * @return Whether this integrity constraint is reported
     *         as violated or not
     * @throws IllegalStateException If the given integrity 
     *         constraint has not been reported as violated 
     *         (or not) via {@link #setViolated(boolean)} 
     *         - if needed, check via {@link #isChecked()} 
     *         whether the report has been made already
     */
    public boolean isViolated() throws IllegalStateException {
        if(!checked)
            throw new IllegalStateException("Violation has not been checked yet.");
        return violated;
    }

    /**
     * @param violated Whether this integrity constraint 
     *        is reported as violated or not to set
     */
    public void setViolated(boolean violated) {
        this.violated = violated;
        this.checked = true;
    }

    /**
     * @return Whether this integrity constraint has been 
     *         checked already and it's violation (or not) 
     *         can be queried via {@link #isViolated()}
     */
    public boolean isChecked() {
        return checked;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return label +(checked?(" = "+(violated?"NOK":"OK")):" [query = "+sparqlQuery+"]");
    }
}
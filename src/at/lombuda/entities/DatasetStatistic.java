package at.lombuda.entities;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import at.lombuda.services.util.UIUtil;

/**
 * This class contains general statistics of the 
 * underlying dataset of this application.
 */
public class DatasetStatistic {
    private Date creationTime;

    private int numOfObservations;
    private int numOfFiscalYears;
    private int numOfMunicipalities;
    private List<CodeCount> codeCounts;

    /**
     * @param numOfObservations Number of saved observations
     * @param numOfFiscalYears Number of observed fiscal years
     * @param numOfMunicipalities Number of covered municipalities
     * @param codeCounts Number of different codes as sorted list
     */
    public DatasetStatistic(int numOfObservations, int numOfFiscalYears,
            int numOfMunicipalities, List<CodeCount> codeCounts) {
        this.creationTime = new Date();

        this.numOfObservations = numOfObservations;
        this.numOfFiscalYears = numOfFiscalYears;
        this.numOfMunicipalities = numOfMunicipalities;
        this.codeCounts = codeCounts;
    }

    /**
     * @return Number of saved observations
     */
    public int getNumOfObservations() {
        return numOfObservations;
    }

    /**
     * @return Number of observed fiscal years
     */
    public int getNumOfFiscalYears() {
        return numOfFiscalYears;
    }

    /**
     * @return Number of covered municipalities
     */
    public int getNumOfMunicipalities() {
        return numOfMunicipalities;
    }

    /**
     * @return Number of different codes as sorted list
     */
    public List<CodeCount> getCodeCounts() {
        return Collections.unmodifiableList(codeCounts);
    }

    /**
     * @return the time at which this statistic got created
     */
    public Date getCreationTime() {
        return creationTime;
    }

    /**
     * @return a human readable representation of the interval 
     *         since the creation of this statistic
     */
    public String getCreationTimeIntervalStr() {
        return UIUtil.formatMillis(System.currentTimeMillis() - creationTime.getTime());
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "DatasetStatistic ["
                + "numOfObservations=" + numOfObservations + ", "
                + "numOfFiscalYears=" + numOfFiscalYears+ ", "
                + "numOfMunicipalities=" + numOfMunicipalities + ", "
                + "codes = "+codeCounts+"]";
    }
}

package at.lombuda.entities;

import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;

import at.lombuda.services.GeoNameService;

/**
 * This entity contains all information
 * of a municipality necessary for this
 * application
 */
public class MunicipalityInfo {
    private int geonameId;
    private String name;
    private String municipalityCode;

    /**
     * @param geonameId the ID on GeoNames of this municipality
     * @param name the name of a municipality (in English language)
     * @param municipalityCode the five-digit-long code code (German 
     *          'Gemeindekennziffer') according to Statistik Austria
     */
    public MunicipalityInfo(int geonameId, String name,
            String municipalityCode) {
        this.geonameId = geonameId;
        this.name = name;
        this.municipalityCode = municipalityCode;
    }
    /**
     * @return the ID on GeoNames of this municipality
     */
    public int getGeonameId() {
        return geonameId;
    }
    /**
     * @return the GeoNames resource of this municipality
     */
    public Resource getGeonameResource() {
        return ResourceFactory.createResource(GeoNameService.GEONAMES_NS+geonameId);
    }
    /**
     * @return the name of a municipality (in English language)
     */
    public String getName() {
        return name;
    }
    /**
     * @return the five-digit-long code code (German 'Gemeindekennziffer')
     *         according to Statistik Austria
     */
    public String getMunicipalityCode() {
        return municipalityCode;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Municipality '"+name+"' ["
                + "GKZ = " + municipalityCode + ", "
                + "GeoNames ID = " + geonameId + "]";
    }
}

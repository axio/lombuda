package at.lombuda.views.util;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import at.lombuda.entities.enums.MessageType;
import at.lombuda.services.Context;

public class ControllerUtil {
    private static final Logger log = LogManager.getLogger(ControllerUtil.class);

    private static final String COUNT_COOKIE_NAME = "C42";
    private static final int COOKIE_AGE = 3600*24*365;//1 year
    private static int visitorCount = 0;

    public static void feedModel(HttpServletRequest request, ModelMap model) {
        feedModel(request, null, model);
    }

    public static void feedModel(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
        model.addAttribute("context", Context.getInstance());
        model.addAttribute("username", request.getUserPrincipal() != null?request.getUserPrincipal().getName():"N/A");
        model.addAttribute("currentYear", Calendar.getInstance().get(Calendar.YEAR));
        if(response!=null)
            countVisitor(request, response);
    }

    public static boolean hasMessage(ModelMap model) {
        return model.containsAttribute("msg") && model.containsAttribute("msgStatus");
    }

    public static void addMessage(ModelMap model, MessageType msgType, String msg) {
        model.addAttribute("msg", msg);
        model.addAttribute("msgStatus", msgType);
    }

    public static void addMessage(RedirectAttributes model, MessageType msgType, String msg) {
        model.addFlashAttribute("msg", msg);
        model.addFlashAttribute("msgStatus", msgType);
    }

    public static int getVisitorCount() {
        return visitorCount;
    }

    public synchronized static void countVisitor(HttpServletRequest request, HttpServletResponse response) {
        if(request.getHeader(HttpHeaders.USER_AGENT) == null)
            return;

        if(isBot(request))
            return;

        Cookie[] cookies = request.getCookies();
        if(cookies == null)
            return;

        int count = 0;
        for(Cookie cookie:cookies){
            if(cookie.getName().equals(COUNT_COOKIE_NAME)){
                count = Integer.parseInt(cookie.getValue());

                if(count > visitorCount) {
                    log.warn("VISITOR UPDATES COUNTER ("+visitorCount+" -> "+count+"): "+getClientStr(request));
                    visitorCount = count;
                } else {
                    cookie.setValue(visitorCount+"");
                    cookie.setMaxAge(COOKIE_AGE);
                    cookie.setComment("Counts the visitors on lombuda.at");
                    cookie.setHttpOnly(true);
                    response.addCookie(cookie);
                }
            }
        }
        if(count == 0){
            visitorCount++;
            log.info("NEW VISITOR (#"+visitorCount+"): "+getClientStr(request));

            Cookie cookie = new Cookie(COUNT_COOKIE_NAME,Integer.toString(visitorCount));
            cookie.setMaxAge(COOKIE_AGE);
            cookie.setComment("Counts the visitors on lombuda.at");
            cookie.setHttpOnly(true);
            response.addCookie(cookie);
        }
    }

    private static String getClientStr(HttpServletRequest request) {
        String remoteIp = getClientIpFrom(request);
        String remoteHost = request.getRemoteHost();
        String referrer = request.getHeader(HttpHeaders.REFERER);
        String ua =request.getHeader(HttpHeaders.USER_AGENT);
        return "IP="+remoteIp+((remoteHost!=null&&!remoteHost.equals(remoteIp))?"|HOST="+remoteHost:"")+(referrer!=null?"|ORIGIN="+referrer:"")+(ua!=null?"|AGENT="+ua:"");
    }

    private static HashMap<String, Date> cachedBots = new HashMap<String, Date>();
    private static final long BOT_CACHE_MAX_AGE = 24*60*60*1000;
    private static boolean isBot(HttpServletRequest request) {
        String ua = request.getHeader(HttpHeaders.USER_AGENT);
        if(!cachedBots.containsKey(ua)) {
            String bot = getBot(ua);
            if(bot != null) {
                log.info("VISITED BY "+bot+": "+getClientStr(request));
                cachedBots.put(ua, new Date());
                return true;
            } else
                return false;
        } else {
            boolean isCacheOutdated = System.currentTimeMillis() - cachedBots.get(ua).getTime() > BOT_CACHE_MAX_AGE;
            if(isCacheOutdated) {
                String bot = getBot(ua);
                log.info("VISITED AGAIN BY "+bot+": "+getClientStr(request));
                cachedBots.put(ua, new Date());
            }

            return true;
        }
    }

    private static String getBot(String userAgentStr) {
        String bot = null;
        if(userAgentStr != null)
            if(userAgentStr.contains("Googlebot"))
                bot = "GOOGLE's BOT";
            else if(userAgentStr.contains("BingBot"))
                bot = "BING's BOT";
            else if(userAgentStr.contains("Slurp"))
                bot = "YAHOO's BOT";
            else if(userAgentStr.contains("Bot/") || userAgentStr.contains("bot/"))
                bot = "SOME BOT";
        return bot;
    }

    public static String getClientIpFrom(HttpServletRequest request) {
        //is client behind something?
        String ipAddress = request.getHeader("X-FORWARDED-FOR");  
        if (ipAddress == null) {  
            ipAddress = request.getRemoteAddr();  
        }
        return ipAddress;
    }

    public static void setHeader(HttpHeaders headers, String headerName, HttpServletRequest request) {
        if(request.getHeader(headerName) != null)
            headers.set(headerName, request.getHeader(headerName));
    }

    private ControllerUtil() {} // Utility class, hide constructor.
}
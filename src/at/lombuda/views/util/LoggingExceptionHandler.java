package at.lombuda.views.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.catalina.connector.ClientAbortException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.Ordered;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

public class LoggingExceptionHandler implements HandlerExceptionResolver, Ordered {
    private static final Logger logger = LogManager.getLogger(LoggingExceptionHandler.class);
    
    public int getOrder() {
        return Integer.MIN_VALUE; // This handler is triggered as first, if any exception is raised
    }

    public ModelAndView resolveException(HttpServletRequest aReq, 
            HttpServletResponse aRes, Object aHandler, Exception anExc) {
        if(anExc instanceof ClientAbortException)
            logger.warn("Requesting client "+(aReq!=null?"["+aReq.getRemoteAddr()+" for '"+aReq.getRequestURI()+"'] ":"")+"closed incoming connection unexpectedly.");
        else
            logger.error("An unhandled exception occured.", anExc);
        return null; // trigger other HandlerExceptionResolvers
    }
}
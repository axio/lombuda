package at.lombuda.views;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import at.lombuda.daos.DaoManager;
import at.lombuda.daos.SparqlDao;
import at.lombuda.daos.consts.FileNameConsts;
import at.lombuda.entities.CsvMetadata;
import at.lombuda.entities.CsvMetadata.ATTR;
import at.lombuda.entities.CsvMetadata.BCE;
import at.lombuda.entities.DatasetStatistic;
import at.lombuda.entities.enums.CodeListType;
import at.lombuda.entities.enums.FiscalState;
import at.lombuda.entities.enums.MessageType;
import at.lombuda.exceptions.DataAlreadyExistsException;
import at.lombuda.exceptions.MissingCsvColumnException;
import at.lombuda.exceptions.ParsingException;
import at.lombuda.services.Context;
import at.lombuda.services.GeoNameService;
import at.lombuda.services.LinkedDataService;
import at.lombuda.services.ServerLogService;
import at.lombuda.services.util.UIUtil;
import at.lombuda.views.util.ControllerUtil;

@Controller
public class AdminController {
    private static final Logger log = LogManager.getLogger(AdminController.class);

    private static final String DEFAULT_LOG_FILE = "lombuda.log";

    private static double collectedSpace;
    private static boolean dataCacheExpired = false;

    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public String getAdminIndex(HttpServletRequest request, ModelMap model) {
        // if Garbage Collector has been run, set collected space
        if(collectedSpace != 0) {
            model.addAttribute("collectedSpace", collectedSpace);
            collectedSpace = 0;
        }

        // collect and set data & statistics for display
        SparqlDao sparqlDao = DaoManager.getInstance().getSparqlDao();
        model.addAttribute("sparqlEndpointUrl", sparqlDao.getSparqlEndpointUrl());
        model.addAttribute("isSparqlEndpointOffline", sparqlDao.isOffline());
        if(!model.containsAttribute("datasetStatistic"))
            model.addAttribute("datasetStatistic", LinkedDataService.getInstance().getDatasetStatistic());

        Date lastSparqlQueryExecutionDate = sparqlDao.getLastQueryExecution();
        if(lastSparqlQueryExecutionDate != null) {
            model.addAttribute("lastSparqlQueryExecutionDate", lastSparqlQueryExecutionDate);
            model.addAttribute("lastSparqlQueryExecutionDateStr", 
                    UIUtil.formatMillis(System.currentTimeMillis() - lastSparqlQueryExecutionDate.getTime()));
            model.addAttribute("numOfSparqlRequests", sparqlDao.getNumOfRequests());
        }

        Date lastSparqlUpdateExecutionDate = sparqlDao.getLastUpdateExecution();
        if(lastSparqlUpdateExecutionDate != null) {
            model.addAttribute("lastSparqlUpdateExecutionDate", lastSparqlUpdateExecutionDate);
            model.addAttribute("lastSparqlUpdateExecutionDateStr", 
                    UIUtil.formatMillis(System.currentTimeMillis() - lastSparqlUpdateExecutionDate.getTime()));
        }
        model.addAttribute("numOfForwardedSparqlRequests", MainController.getNumOfForwardedSparqlRequests());
        model.addAttribute("isGeoNamesWebServiceReachable", GeoNameService.getInstance().isWebserviceOnline());
        model.addAttribute("visitorCount", ControllerUtil.getVisitorCount());
        if(LinkedDataService.getInstance().getCurrentIntegrityConstraints() != null)
            model.addAttribute("isIcOK", LinkedDataService.getInstance().areCurrentIntegrityConstraintsSuccessful());
        if(!model.containsAttribute("numOfAvailableDataSlices"))
            model.addAttribute("numOfAvailableDataSlices", 
                    countDataSlices(LinkedDataService.getInstance().getStatmentsOfAccountSlices()));

        // get and set current status of service layer
        model.addAttribute("serviceLayerInitialized", Context.getInstance().isInitialized());
        if(Context.getInstance().isDelayedInitialised())
            model.addAttribute("serviceLayerDelayedInitialized", Context.getInstance().isDelayedInitialised());
        else
            model.addAttribute("serviceLayerInitialisationError", Context.getInstance().hasInitialisationError());
        model.addAttribute("serviceLayerShutdown", Context.getInstance().isShutdown());

        ControllerUtil.feedModel(request, model);
        return "admin/index";
    }

    private int countDataSlices(Map<String,List<String>> dataSlices) {
        int result = 0;

        if(dataSlices != null)
            for(String m:dataSlices.keySet()) {
                result += dataSlices.get(m).size();
            }

        return result;
    }

    @RequestMapping(value="/admin/runGc", method = RequestMethod.GET)
    public String runGc(@RequestParam(required = false) String target) {
        collectedSpace = Context.getInstance().runGc();
        if(target == null)
            target = "/admin/";
        return "redirect:"+target;
    }

    @RequestMapping(value="/admin/data/cache/refresh", method = RequestMethod.GET)
    public String refresh(@RequestParam(required = false) String target,
            RedirectAttributes redirectAttributes) {
        long startMillis = System.currentTimeMillis();
        DatasetStatistic dataStatistic = LinkedDataService.getInstance().getDatasetStatistic(true);
        int numOfDataSlices = countDataSlices(LinkedDataService.getInstance().getStatmentsOfAccountSlices(true));
        LinkedDataService.getInstance().doIntegrityCheck();
        boolean pubbySubjectCacheRefreshed = LinkedDataService.getInstance().refreshPubbySubjectCache();
        long duration = System.currentTimeMillis() - startMillis;

        redirectAttributes.addFlashAttribute("datasetStatistic", dataStatistic);
        redirectAttributes.addFlashAttribute("numOfAvailableDataSlices", numOfDataSlices);

        boolean refreshSuccessful = dataStatistic != null && pubbySubjectCacheRefreshed;
        if(refreshSuccessful) {
            String msg = "Successfully refreshed all data caches. This operation took "+UIUtil.formatMillis(duration)+".";
            ControllerUtil.addMessage(redirectAttributes, MessageType.SUCCESS, msg);
            log.info(msg);
            dataCacheExpired = false;
        } else
            ControllerUtil.addMessage(redirectAttributes, MessageType.ALERT, 
                    "Some unexpected error occured during update of one or more data caches. Please consult the logs and try again.");

        if(target == null)
            target = "/admin/data";
        return "redirect:"+target;
    }

    @RequestMapping(value="/admin/refreshStatistics", method = RequestMethod.GET)
    public String refreshStatistics(@RequestParam(required = false) String target,
            RedirectAttributes redirectAttributes) {
        redirectAttributes.addFlashAttribute("datasetStatistic", 
                LinkedDataService.getInstance().getDatasetStatistic(true));
        if(target == null)
            target = "/admin/";
        return "redirect:"+target;
    }

    @RequestMapping(value="/admin/geoNamesTest", method = RequestMethod.GET)
    public String performGeoNamesTest(@RequestParam(required = false) String target,
            RedirectAttributes redirectAttributes) {
        redirectAttributes.addFlashAttribute("isGeoNamesWebServiceTestSuccess", 
                GeoNameService.getInstance().performTestCall());
        if(target == null)
            target = "/admin/";
        return "redirect:"+target;
    }

    @RequestMapping(value="/admin/checkIntegrity", method = RequestMethod.GET)
    public String performIntegrityCheck(@RequestParam(required = false) String target) {
        LinkedDataService.getInstance().doIntegrityCheck();
        if(target == null)
            target = "/admin/";
        return "redirect:"+target;
    }

    @RequestMapping(value="/admin/refreshAvailableDataSlices", method = RequestMethod.GET)
    public String refreshAvailableDataSlices(@RequestParam(required = false) String target,
            RedirectAttributes redirectAttributes) {
        redirectAttributes.addFlashAttribute("numOfAvailableDataSlices", 
                countDataSlices(LinkedDataService.getInstance().getStatmentsOfAccountSlices(true)));
        if(target == null)
            target = "/admin/";
        return "redirect:"+target;
    }

    @RequestMapping(value = "/admin/data", method = RequestMethod.GET)
    public String getDataOps(HttpServletRequest request, ModelMap model) {   
        // get general data for display
        List<String> filenames = DaoManager.getInstance().getFileDao().getAllFilenames();
        Collections.sort(filenames);
        model.addAttribute("storedFilenames", filenames);
        if(!model.containsAttribute("datasetStatistic"))
            model.addAttribute("datasetStatistic", LinkedDataService.getInstance().getDatasetStatistic());
        if(dataCacheExpired)
            model.addAttribute("showRefresh", true); 

        ControllerUtil.feedModel(request, model);
        return "admin/data";
    }

    @RequestMapping(value = "/admin/data/upload", method = RequestMethod.POST)
    public String fileUploadHandler(HttpServletRequest request, ModelMap model, RedirectAttributes redirectAttributes,
            @RequestParam("type") String type, @RequestParam("files") MultipartFile[] files,
            @RequestParam(value = "codeListType", required = false) String codeType,
            @RequestParam(required = false) String budaCnSrcUrl, @RequestParam(required = false) String budaCnSrcDesc,
            @RequestParam(required = false) String budaOpYear, 
            @RequestParam(required = false) String budaYear, @RequestParam(required = false) String budaCnYear,
            @RequestParam(required = false) String budaOpMuni, 
            @RequestParam(required = false) String budaMuniName, @RequestParam(required = false) String budaMuniNr, @RequestParam(required = false) String budaCnMuni,
            @RequestParam(required = false) String budaOpStat, @RequestParam(required = false) String budaCnStat,
            @RequestParam(required = false) String budaOpBce, @RequestParam(required = false) String budaCnBc,
            @RequestParam(required = false) String budaCnBi, @RequestParam(required = false) String budaCnType,
            @RequestParam(required = false) String budaCnFc, @RequestParam(required = false) String budaCnEc,
            @RequestParam(required = false) String budaCnAmount, @RequestParam(required = false) String budaCnDesc) {

        String iseMsg = "An internal server error occured. Please check the server logs and try again.";

        // parse upload type
        UploadType uploadType = null;
        try {
            uploadType = UploadType.valueOf(type);
        } catch(Exception e) {
            log.error("Received invalid upload type: "+type, e);
            ControllerUtil.addMessage(redirectAttributes, MessageType.ALERT, iseMsg);
            return "redirect:/admin/data";
        }

        // check whether files are received anyway
        if(files == null || files.length == 0) {
            ControllerUtil.addMessage(redirectAttributes, MessageType.ALERT, 
                    "No file received. Please select your choice another time and try again.");
            return "redirect:/admin/data";
        }

        // regulate operations according to transmitted upload type
        log.debug("Upload Type = "+uploadType);
        switch (uploadType) {

        /** Upload for predefined Data Structure Definition **/
        case DSD_UPLOAD:

            // parse received files
            String filenames = "";
            long filesize = 0;
            List<MultipartFile> filesToUpload = new ArrayList<MultipartFile>();
            for(MultipartFile file:files) {            
                String filename = file.getOriginalFilename().toLowerCase();
                if(FileNameConsts.ACCEPTED_FILE_NAMES.contains(filename)) {
                    filenames += file.getOriginalFilename()+", ";
                    filesize += file.getSize();

                    filesToUpload.add(file);
                } else {
                    log.info("Ignore received file for upload: "+filename);
                }
            }

            // if all received files are classified as relevant, do upload
            if(filesize > 0) {
                boolean success = false;
                for(MultipartFile file:filesToUpload) {   
                    success = DaoManager.getInstance().getFileDao().store(file, file.getOriginalFilename().toLowerCase());
                    if(!success)
                        break;
                }

                if(success) {
                    String msg = "Successfully uploaded following file"+(filesToUpload.size() == 1?"":"s")+": "+filenames.substring(0, filenames.length()-2)
                    +" ("+Math.round(((double) filesize)/1024)+"KB)";
                    log.debug(msg);
                    ControllerUtil.addMessage(redirectAttributes, MessageType.SUCCESS, msg);
                } else {
                    String msg = "An error occured durring file upload. Please consult the error logs and try again.";
                    log.error(msg);
                    ControllerUtil.addMessage(redirectAttributes, MessageType.ALERT, msg);
                }
            } else {
                ControllerUtil.addMessage(redirectAttributes, MessageType.WARNING, 
                        "No file received with one of the correct names. Please rename your choice and try again.");
            }

            break;

            /** Import for predefined VRV Code Lists **/
        case CODE_LIST_IMPORT:

            // parse uploaded code list type
            CodeListType codeListType = null;
            try {
                codeListType = CodeListType.valueOf(codeType.toUpperCase());
            } catch(Exception e) {
                log.error("Received invalid code list type: "+type, e);
                ControllerUtil.addMessage(redirectAttributes, MessageType.ALERT, iseMsg);
                return "redirect:/admin/data";
            }

            log.debug("Received code list type for import = "+codeListType);

            // get CSV upload
            MultipartFile csvCodeList = files[0];
            if(files.length > 1)
                log.info("Received "+files.length+" for "+codeListType.getSingularName()+" code list import. All except the first one will be ignored.");

            // try to import CSV
            try {
                long startMillis = System.currentTimeMillis();
                int numOfImportedCodes = LinkedDataService.getInstance().importCodeList(codeListType, csvCodeList);
                long duration = System.currentTimeMillis() - startMillis;

                if(numOfImportedCodes == 0) {
                    String msg = "No "+codeListType.getSingularName()+" codes imported. Please check the given CSV file, the server logs and try again.";
                    log.warn(msg);
                    ControllerUtil.addMessage(redirectAttributes, MessageType.WARNING, msg);
                } else {
                    String msg = "Successfully imported "+numOfImportedCodes+" "+codeListType.getSingularName()+" codes. "
                            + "This operation took "+UIUtil.formatMillis(duration)+".";
                    log.info(msg);
                    ControllerUtil.addMessage(redirectAttributes, MessageType.SUCCESS, msg);
                }
            } catch (MissingCsvColumnException e) {
                ControllerUtil.addMessage(redirectAttributes, MessageType.ALERT, 
                        e.getMessage()+" Please correct the given CSV file and try again.");
                return "redirect:/admin/data";
            }

            break;


            /** Import of budgetary data for Austrian municipalities **/
        case DATA_IMPORT:

            /* START | parse given CSV metadata */
            CsvMetadata metadata = new CsvMetadata();

            //  the source
            metadata.setFixedValueFor(ATTR.sourceUrl, budaCnSrcUrl);
            metadata.setFixedValueFor(ATTR.sourceDesc, budaCnSrcDesc);

            //  the fiscal year
            switch (budaOpYear) {
            case "given":
                metadata.setFixedValueFor(ATTR.fiscalYear, budaYear);
                break;

            case "column":
                metadata.setColumnNameFor(ATTR.fiscalYear, budaCnYear);
                break;

            default:
                log.error("Received invalid fiscal year option during budget data import: "+budaOpYear);
                ControllerUtil.addMessage(redirectAttributes, MessageType.ALERT, iseMsg);
                return "redirect:/admin/data";
            }

            //  the municipality
            switch (budaOpMuni) {
            case "given_name":
                metadata.setFixedValueFor(ATTR.municipality, budaMuniName);
                break;

            case "given_nr":
                metadata.setFixedValueFor(ATTR.municipality, budaMuniNr);
                break;

            case "column":
                metadata.setColumnNameFor(ATTR.municipality, budaCnMuni);
                break;

            default:
                log.error("Received invalid municipality option during budget data import: "+budaOpMuni);
                ControllerUtil.addMessage(redirectAttributes, MessageType.ALERT, iseMsg);
                return "redirect:/admin/data";
            }

            //  the fiscal state
            switch (budaOpStat) {
            case "given_va":
                metadata.setFixedValueFor(ATTR.fiscalState, FiscalState.VA.toString());
                break;

            case "given_ra":
                metadata.setFixedValueFor(ATTR.fiscalState, FiscalState.RA.toString());
                break;

            case "column":
                metadata.setColumnNameFor(ATTR.fiscalState, budaCnStat);
                break;

            default:
                log.error("Received invalid fiscal state option during budget data import: "+budaOpStat);
                ControllerUtil.addMessage(redirectAttributes, MessageType.ALERT, iseMsg);
                return "redirect:/admin/data";
            }

            //  budgetary classification encoding
            switch (budaOpBce) {
            case "combo":
                metadata.setBudgetaryClassificationEncoding(BCE.combined);
                metadata.setColumnNameFor(ATTR.budgetaryClass, budaCnBc);
                break;

            case "separ":
                metadata.setBudgetaryClassificationEncoding(BCE.separated);
                metadata.setColumnNameFor(ATTR.budgetaryIndicator, budaCnBi);
                metadata.setColumnNameFor(ATTR.fiscalType, budaCnType);
                metadata.setColumnNameFor(ATTR.functionalClass, budaCnFc);
                metadata.setColumnNameFor(ATTR.economicClass, budaCnEc);
                break;

            default:
                log.error("Received invalid budgetary classification encoding during budget data import: "+budaOpBce);
                ControllerUtil.addMessage(redirectAttributes, MessageType.ALERT, iseMsg);
                return "redirect:/admin/data";
            }

            //  the remainder
            metadata.setColumnNameFor(ATTR.amount, budaCnAmount);
            metadata.setColumnNameFor(ATTR.description, budaCnDesc);
            /* END | parse given CSV metadata */

            // get CSV upload
            MultipartFile csvBugdetData = files[0];
            if(files.length > 1)
                log.info("Received "+files.length+" files for budget data import. All except the first one will be ignored.");

            // try to import CSV
            try {
                if(!metadata.isConsistent()) {
                    String msg = "Given CSV file is not consistent. Please check it according to the instructions on this page and try again.";
                    log.warn(msg);
                    ControllerUtil.addMessage(redirectAttributes, MessageType.WARNING, msg);
                    return "redirect:/admin/data";
                }

                long startMillis = System.currentTimeMillis();
                int numOfImportedDataRecords = LinkedDataService.getInstance().importBudgetData(metadata, csvBugdetData);
                long duration = System.currentTimeMillis() - startMillis;

                if(numOfImportedDataRecords == 0) {
                    String msg = "No data imported. Please check the given CSV file, the server logs and try again.";
                    log.warn(msg);
                    ControllerUtil.addMessage(redirectAttributes, MessageType.WARNING, msg);
                } else {
                    String msg = "Successfully imported and stored "+numOfImportedDataRecords+" budgetary data records. "
                            + "This operation took "+UIUtil.formatMillis(duration)+".";
                    dataCacheExpired = true;
                    ControllerUtil.addMessage(redirectAttributes, MessageType.SUCCESS, msg);
                }
            } catch (IOException e) {
                log.error("The application encountered an unexpected error during import of the given data into the backed dataset. ", e);
                ControllerUtil.addMessage(redirectAttributes, MessageType.ALERT, iseMsg);
                return "redirect:/admin/data";
            } catch (DataAlreadyExistsException e) {
                ControllerUtil.addMessage(redirectAttributes, MessageType.ALERT, e.getMessage());
                return "redirect:/admin/data";
            } catch (ParsingException e) {
                ControllerUtil.addMessage(redirectAttributes, MessageType.WARNING, 
                        e.getMessage()+" Please correct the given CSV file and try again.");
                return "redirect:/admin/data";
            }
            break;

        default:
            throw new AssertionError();
        }

        return "redirect:/admin/data";
    }

    private enum UploadType {
        DSD_UPLOAD, CODE_LIST_IMPORT, DATA_IMPORT;
    }

    private static final String DOWNLOAD_TYPE = "text/turtle";

    @RequestMapping(value = "/admin/data/download/{filename:.+\\.ttl}", method = RequestMethod.GET, produces = DOWNLOAD_TYPE)
    public @ResponseBody Resource fileDownloadHandler(@PathVariable("filename") String filename, 
            HttpServletResponse response) throws FileNotFoundException {
        // get saved file
        File file = DaoManager.getInstance().getFileDao().getFile(filename);

        // if file is not found, return exception
        if(file == null)
            throw new FileNotFoundException("Could not find file on the server: "+filename);
        else
            log.debug("Prepare download for locally saved file: "+file);

        // prepare response and return file
        response.setContentType(DOWNLOAD_TYPE);
        response.setHeader("Content-Disposition", "inline; filename=" + file.getName());
        response.setHeader("Content-Length", String.valueOf(file.length()));
        return new FileSystemResource(file);
    }

    @RequestMapping(value="/admin/data/rebuild", method = RequestMethod.POST)
    public String rebuildDataSet(@RequestParam(required = false) String target, RedirectAttributes redirectAttributes) {
        long startMillis = System.currentTimeMillis();
        boolean rebuildSuccessful = LinkedDataService.getInstance().rebuildDataset();
        long duration = System.currentTimeMillis() - startMillis;
        if(rebuildSuccessful) {
            ControllerUtil.addMessage(redirectAttributes, MessageType.SUCCESS, 
                    "Automatic rebuild of backed dataset successful. This operation took "+UIUtil.formatMillis(duration)+".");
            log.info("Successfully rebuild backed dataset. The operation took "+UIUtil.formatMillis(duration)+".");
        } else
            ControllerUtil.addMessage(redirectAttributes, MessageType.ALERT, 
                    "The application encountered an unexpected error during rebuilding the backed dataset. "
                            + "This operation took "+UIUtil.formatMillis(duration)+" until this error.");

        dataCacheExpired = true;

        if(target == null)
            target = "/admin/data";
        return "redirect:"+target;
    }

    @RequestMapping(value = "/admin/logs", method = RequestMethod.GET)
    public String getLog(HttpServletRequest request, ModelMap model,
            @RequestParam(required = false) String logfile) {
        // get log file names
        List<String> logFilenames = ServerLogService.getInstance().getAllLogFilenames();
        model.addAttribute("logFilenames", logFilenames);

        // if no or an unknown log file was selected, select application log file
        if(logfile == null || !logFilenames.contains(logfile))
            logfile = DEFAULT_LOG_FILE; 

        // get log content
        String log = UIUtil.getFormattedLogStr(ServerLogService.getInstance().getLog(logfile));

        // if log is empty, show message
        if(log == null || log.trim().isEmpty())
            log = "ERROR - NOTHING TO DISPLAY";

        model.addAttribute("logfile", logfile);
        model.addAttribute("log", log);

        ControllerUtil.feedModel(request, model);
        return "admin/log";
    }
}

package at.lombuda.views;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.jena.web.HttpSC;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import at.lombuda.daos.DaoManager;
import at.lombuda.daos.consts.FileNameConsts;
import at.lombuda.entities.IntegrityConstraint;
import at.lombuda.entities.enums.FiscalState;
import at.lombuda.services.LinkedDataService;
import at.lombuda.services.util.ModelUtil;
import at.lombuda.views.util.ControllerUtil;

@Controller
public class MainController {
    private static final Logger log = LogManager.getLogger(MainController.class);
    private static final RestTemplate restTemplate = new RestTemplate();

    private static int numOfForwardedSparqlRequests = 0;

    @RequestMapping(value={"/","index.html","index"}, method = RequestMethod.GET)
    public String getIndex(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
        model.addAttribute("datasetStatistic", LinkedDataService.getInstance().getDatasetStatistic());

        ControllerUtil.feedModel(request, response, model);
        return "index";
    }

    @RequestMapping(value = "/data/ic", method = RequestMethod.GET)
    public String getIntegrityCheckReport(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
        Map<String, IntegrityConstraint> ics = LinkedDataService.getInstance().getCurrentIntegrityConstraints();
        if(ics != null) {
            List<String> icNames = new ArrayList<String>(ics.keySet());
            Collections.sort(icNames);
            model.addAttribute("icNames",icNames);
            model.addAttribute("ics", ics);
        }

        ControllerUtil.feedModel(request, response, model);
        return "ic";
    }

    private static final String IC_DOWNLOAD_TYPE = "application/sparql-query";

    @RequestMapping(value = "/data/ic/download/{icName:IC-[0-9]+.*}", method = RequestMethod.GET, produces = IC_DOWNLOAD_TYPE)
    public @ResponseBody Resource fileDownloadHandler(@PathVariable("icName") String icName, 
            HttpServletResponse response) throws FileNotFoundException {
        // get saved file
        File file = DaoManager.getInstance().getFileDao().getIntegrityConstraint(icName);

        // if file is not found, return exception
        if(file == null)
            throw new FileNotFoundException("Could not find integrity constraint on the server: "+icName);
        else
            log.trace("Prepare download for locally saved integrity constraint: "+icName);

        // prepare response and return file
        response.setContentType(IC_DOWNLOAD_TYPE);
        response.setHeader("Content-Disposition", "inline; filename=" + icName +".sq");
        response.setHeader("Content-Length", String.valueOf(file.length()));
        return new FileSystemResource(file);
    }

    @RequestMapping(value = "/sparql", method = { RequestMethod.GET, RequestMethod.POST })
    public String doSparqlProxy(@RequestBody(required = false) String requestBody, @RequestParam(value = "query", required = false) String requestQuery,
            @RequestHeader Map<String, String> requestHeader, HttpMethod requestMethod, HttpServletRequest request, 
            ModelMap model, HttpServletResponse response) 
                    throws URISyntaxException, IOException {        
        String sparqlMediaType = "application/sparql-query";

        // check incoming request, whether it is a valid SPARQL request or not, and hence, should be forwarded or not
        String contentType = request.getHeader(HttpHeaders.CONTENT_TYPE);        
        boolean isSparqlPostRequest = requestMethod == HttpMethod.POST
                && contentType != null
                && contentType.equalsIgnoreCase(sparqlMediaType);
        boolean isSparqlGetRequest = requestQuery != null;
        boolean isValidSparqlRequest = isSparqlGetRequest || isSparqlPostRequest;

        // in case valid SPARQL query is received, do proxy
        if(isValidSparqlRequest) {
            ControllerUtil.countVisitor(request, response);
            String endpointUrl = DaoManager.getInstance().getSparqlDao().getSparqlEndpointUrl();

            // get correct request URL according to request type
            URI thirdPartyApi;
            if(isSparqlGetRequest)
                thirdPartyApi = new URI(endpointUrl+"/sparql?query="+URLEncoder.encode(requestQuery, "UTF-8"));
            else
                thirdPartyApi = new URI(endpointUrl);

            log.debug("Received valid SPARQL call for '"+thirdPartyApi.getPath()+"' ("+requestMethod+(contentType!=null?"|"+contentType:"")+")");

            // forward headers
            HttpHeaders headers = new HttpHeaders();
            ControllerUtil.setHeader(headers, HttpHeaders.ACCEPT, request);
            ControllerUtil.setHeader(headers, HttpHeaders.ACCEPT_ENCODING, request);
            ControllerUtil.setHeader(headers, HttpHeaders.ACCEPT_CHARSET, request);
            ControllerUtil.setHeader(headers, HttpHeaders.ACCEPT_LANGUAGE, request);
            ControllerUtil.setHeader(headers, HttpHeaders.CONTENT_TYPE, request);

            try {
                addForwardedSparqlRequest();

                // make proxy request
                ResponseEntity<String> resp =
                        restTemplate.exchange(thirdPartyApi, isSparqlGetRequest?HttpMethod.GET:HttpMethod.POST, 
                                new HttpEntity<String>(isSparqlPostRequest?requestBody:null, headers), String.class);

                // parse and forward response
                response.setStatus(resp.getStatusCodeValue());
                String fusekiRequestId = null;
                for(Entry<String, List<String>> headerEntry:resp.getHeaders().entrySet()) {
                    if(headerEntry.getKey().equalsIgnoreCase("Fuseki-Request-ID"))
                        fusekiRequestId = headerEntry.getValue().get(0);
                    else if(!headerEntry.getKey().equalsIgnoreCase(HttpHeaders.TRANSFER_ENCODING))
                        for(String headerValue:headerEntry.getValue()) {
                            log.trace("Forwarding Header: "+headerEntry.getKey()+": "+headerValue);
                            response.setHeader(headerEntry.getKey(), headerValue);
                        }
                }
                PrintWriter responseWriter = response.getWriter();
                responseWriter.print(resp.getBody());
                responseWriter.flush();
                responseWriter.close();

                log.debug("Succesfully forwarded SPARQL request"+(fusekiRequestId!=null?" (Fuseki Request ID #"+fusekiRequestId+")":"")+": "+
                        resp.getStatusCode().value()+" "+resp.getStatusCode().getReasonPhrase());

                return null;
            } catch(HttpClientErrorException e) {
                log.warn("SPARQL endpoint delivered an error during request forwarding: "+e.getMessage());

                PrintWriter responseWriter = response.getWriter();
                response.sendError(e.getRawStatusCode(), e.getStatusText());
                responseWriter.flush();
                responseWriter.close();

                return null;
            } catch (RestClientException e) {
                log.error("An unexpected error occured during SPARQL request forwarding.", e);

                PrintWriter responseWriter = response.getWriter();
                response.sendError(HttpSC.INTERNAL_SERVER_ERROR_500);
                responseWriter.flush();
                responseWriter.close();

                return null;
            }
        }

        // otherwise display human usable query page
        ControllerUtil.feedModel(request, response, model);
        return "sparql";
    }

    @RequestMapping(value = "/budget-profiles", method = { RequestMethod.GET, RequestMethod.POST })
    public String getBudgetProfile(@RequestParam(required = false) String municipality, @RequestParam(required = false) String fiscalYear,
            HttpServletRequest request, HttpMethod requestMethod, HttpServletResponse response, ModelMap model) {
        // get all available slices for budget profile display, if available
        Map<String, List<String>> availableSlices = LinkedDataService.getInstance().getStatmentsOfAccountSlices();
        if(availableSlices != null) {
            ArrayList<String> municipalities = new ArrayList<String>(availableSlices.keySet());
            Collections.sort(municipalities);
            model.addAttribute("municipalities", municipalities);
            String municiaplitiesJs = "{";
            boolean isFirst = true;
            for(Entry<String, List<String>> slice:availableSlices.entrySet()) {
                if(isFirst)
                    isFirst = false;
                else
                    municiaplitiesJs += ", ";
                municiaplitiesJs += "\""+slice.getKey()+"\": "+slice.getValue();
            }
            municiaplitiesJs += "}";
            model.addAttribute("municipalitiesJs", municiaplitiesJs);
        }

        // if budget profile selection is received
        if(requestMethod == HttpMethod.POST && 
                municipality != null && fiscalYear != null) {
            // get and set budget profile list
            model.addAttribute("budgetProfileList", LinkedDataService.getInstance().getBudgetProfile(fiscalYear, municipality, FiscalState.RA));

            // get data slice URI for selected budget profile for hyperlink purposes
            String municipalityCode = municipality.substring(municipality.indexOf('(') + 1, municipality.length() - 1);
            String dataSliceUrl = ModelUtil.getDataSliceUri(fiscalYear, municipalityCode, FiscalState.RA.name());
            model.addAttribute("dataSliceUrl", dataSliceUrl);

            // set selected attributes (again)
            model.addAttribute("municipality", "\""+municipality+"\"");
            model.addAttribute("fiscalYear", "\""+fiscalYear+"\"");
        }

        ControllerUtil.feedModel(request, response, model);
        return "budgetProfile";
    }

    private static final String BP_QUERY_DOWNLOAD_TYPE = "application/sparql-query";

    @RequestMapping(value = "/budget-profile/downloadQuery", method = RequestMethod.GET, produces = BP_QUERY_DOWNLOAD_TYPE)
    public @ResponseBody Resource getBudgetProfileQuery(HttpServletResponse response) throws FileNotFoundException {
        // get saved query template for budget profile aggregation as file
        String queryFilename = FileNameConsts.BP_QUERY;
        File queryFile = DaoManager.getInstance().getFileDao().getQueryFile(queryFilename);

        // if string  is not found, return exception
        if(queryFile == null)
            throw new FileNotFoundException("Could not find query template file on the server: "+queryFilename);
        else
            log.trace("Prepare download for locally saved integrity constraint: "+queryFilename);

        // prepare response and return file
        response.setContentType(BP_QUERY_DOWNLOAD_TYPE);
        response.setHeader("Content-Disposition", "inline; filename=Budget Profile Query.sq");
        response.setHeader("Content-Length", String.valueOf(queryFile.length()));
        return new FileSystemResource(queryFile);
    }

    @RequestMapping(value = "/about", method = RequestMethod.GET)
    public String getAbout(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
        ControllerUtil.feedModel(request, response, model);
        return "about";
    }

    private synchronized static void addForwardedSparqlRequest() {
        numOfForwardedSparqlRequests++;
    }

    /**
     * @return Number of forwarded SPARQL requests by this controller
     */
    public static int getNumOfForwardedSparqlRequests() {
        return numOfForwardedSparqlRequests;
    }
}
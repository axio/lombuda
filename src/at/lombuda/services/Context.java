package at.lombuda.services;

import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletContext;

import at.lombuda.daos.DaoManager;
import at.lombuda.services.util.UIUtil;

/**
 * This class represents the application context, which provides
 * all basic functionality of it and will eventually be called
 * by Spring MVC on startup.
 */
public class Context extends AbstractService {

    private static Context instance;
    private static ServletContext servletContext;

    private static Date startDate;
    private static boolean isInTestMode;
    private static String catalinaBasePath;
    private static Path runningBasePath;
    private static String applicationBasePath;
    private static String host;
    private static String hostAddress;
    private static ScheduledExecutorService executor;
    private static boolean isDelayedInitialised = false;
    private static boolean initialisationError = false;

    private static final double MB = 1024*1024;

    private Context() {}

    @Override
    protected void initService(Object... args) throws Exception {
        //// saving general server info
        Context.startDate = new Date();

        if(args[0] instanceof ServletContext) Context.servletContext = (ServletContext) args[0];

        catalinaBasePath = System.getProperty("catalina.base");
        log.info("System property: CATALINA.BASE = '" + catalinaBasePath + "'");

        runningBasePath = Paths.get("").toAbsolutePath();
        applicationBasePath = servletContext.getRealPath(File.separator);
        log.info("Application property: RUN.PATH = '" + runningBasePath + "'");
        log.info("Application property: APP.PATH = '" + applicationBasePath + "'");

        try {// get Server Host/IP
            Context.host =        InetAddress.getLocalHost().toString();
            Context.hostAddress = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            Context.host = "UNKNOWN HOST";
            log.warn("Host could not be identified.", e);
        }

        // get test mode parameter, if available        
        if(servletContext != null) {
            String isInTestModeStr = servletContext.getInitParameter( "at.lombuda.testMode" );
            if( isInTestModeStr != null && !isInTestModeStr.isEmpty() ) {
                isInTestMode = Boolean.parseBoolean(isInTestModeStr);

                log.info("Context property: at.lombuda.testMode = " + Boolean.toString(isInTestMode).toUpperCase());
            }
        }

        //// initialisation of basic service layer
        log.info("Initialisation of service layer started.");
        PropertiesService.getInstance().init();
        ServerLogService.getInstance().init();
        DaoManager.getInstance().init(isInTestMode);
        LinkedDataService.getInstance().init();
        GeoNameService.getInstance().init();

        // created thread pool for delayed initialisation
        executor = Executors.newScheduledThreadPool(1);
        executor.schedule(new DelayedServiceInitialiser(
                PropertiesService.getInstance(),
                ServerLogService.getInstance(),
                DaoManager.getInstance(),
                LinkedDataService.getInstance(),
                GeoNameService.getInstance()), 5, TimeUnit.SECONDS);
    }

    @Override
    protected void shutdownService(Object... args) throws Exception {
        //// destruction of service layer
        log.info("Shutdown of service layer started.");

        // shutdown executor
        if(executor != null && !executor.isShutdown())
            executor.shutdown();
    }

    public static Context getInstance() {
        if(instance == null)
            instance = new Context();
        return instance;
    }

    public Date getStartDate() {
        return startDate;
    }

    public long getRunTimeMillis() {
        return System.currentTimeMillis() - startDate.getTime();
    }

    public String getRunTimeStr() {
        return UIUtil.formatMillis(getRunTimeMillis());
    }

    public String getServerInfo() {
        return servletContext.getServerInfo();
    }

    public File getCatalinaBaseDir() {
        return new File(this.getCatalinaBasePath());
    }

    public String getCatalinaBasePath() {
        return catalinaBasePath;
    }

    public File getTomcatLogDir() {
        return new File(this.getTomcatLogPath());
    }

    public String getTomcatLogPath() {
        return this.getCatalinaBasePath()+File.separator+"logs"+File.separator;
    }

    public String getAppPath() {
        return applicationBasePath;
    }

    /**
     * @return "hostname / literal IP address" of the host of this application
     */
    public String getHost() {
        return host;
    }

    /**
     * @return the raw IP address of the host in a string format
     */
    public String getHostAddress() {
        return hostAddress;
    }

    public double getUsedMemory() {
        Runtime runtime = Runtime.getRuntime();
        return ((double) (runtime.totalMemory() - runtime.freeMemory())) / MB;
    } 

    public double getFreeMemory() {
        Runtime runtime = Runtime.getRuntime();
        return ((double) (runtime.freeMemory())) / MB;
    } 

    public double getTotalMemory() {
        Runtime runtime = Runtime.getRuntime();
        return ((double) (runtime.totalMemory())) / MB;
    }

    public double getMaxMemory() {
        Runtime runtime = Runtime.getRuntime();
        return ((double) (runtime.maxMemory())) / MB;
    }

    public double runGc() {
        double before = this.getUsedMemory();
        Runtime.getRuntime().gc();
        double after = this.getUsedMemory();
        double collectedSpace = before - after;

        log.info("Invoked VM garbage collector (" + Math.round(collectedSpace) + " MB collected, "
                + Math.round(after) + " MB are currently used).");

        return collectedSpace;
    }

    /**
     * @return Whether all services are initialised completely (also in a delayed manner)
     */
    @Override
    public boolean isDelayedInitialised() {
        return isDelayedInitialised;
    }

    /**
     * @return Whether an error occurred during initialisation of the service layer
     */
    public boolean hasInitialisationError() {
        return initialisationError;
    }

    protected void initialisationError() {
        initialisationError = true;
    }

    /**
     * This class is used to perform delayed initialisation
     * of the web application's service layer in a thread.
     */
    private class DelayedServiceInitialiser implements Runnable {
        private AbstractService[] services;

        public DelayedServiceInitialiser(AbstractService... services) {
            this.services = services;
        }

        @Override
        public void run() {            
            try {
                log.info("Delayed initialisation of service layer started.");

                long startMillis = System.currentTimeMillis();
                for(AbstractService s:services)
                    s.delayedInit();
                long duration = System.currentTimeMillis() - startMillis;

                isDelayedInitialised = true;
                log.info("Successfully initialised service layer in a delayed manner within "+UIUtil.formatMillis(duration)+".");  
            } catch(Exception e) {
                initialisationError = true;
                log.fatal("An unexpected exception occurred during delayed initialisation of service layer.", e);
            }
        }
    }
}
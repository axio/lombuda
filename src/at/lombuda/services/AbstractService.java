package at.lombuda.services;

import java.util.Date;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import at.lombuda.exceptions.ServiceException;
import at.lombuda.services.util.UIUtil;

/**
 * This class encapsulates primary service class functionality 
 * for this application, as:
 * <ul>
 * <li>Initialisation of the respective service class by 
 * calling {@link #init()}.
 * <li>Delayed initialisation of the respective service 
 * class by calling {@link #delayedInitService()} (after startup).
 * <li>Shutdown of the respective service class by 
 * calling {@link #shutdown()}.
 * </ul>
 */
public abstract class AbstractService {
    protected final Logger log = LogManager.getLogger(this.getClass().getName());

    private boolean initialized;
    private boolean delayedInitialized;
    private boolean shutdown;
    private Date initialisationTime;
    private Date delayedInitialisationTime;

    /**
     * Initialises this service with the given arguments.
     * 
     * @param args Arguments for initialisation of this service.
     * @throws ServiceException If this service could not be initialised.
     */
    public void init(Object... args) throws ServiceException {
        checkPossibleInitialization(initialized);
        try {
            initialisationTime = new Date();
            initService(args);
        } catch(Exception e) {
            throw new ServiceException(
                    this.getClass().getSimpleName()+" could not be initialised due to an unexpected error. The stack trace follows.", e);
        }
        initializationComplete(false);
    }

    protected abstract void initService(Object... args) throws Exception;

    /**
     * Initialises this service in a delayed manner after startup.
     * 
     * @throws ServiceException If this service could not be initialised.
     */
    public void delayedInit() throws ServiceException {
        checkPossibleInitialization(delayedInitialized);
        try {
            delayedInitialisationTime = new Date();
            if(needDelayedInit())
                delayedInitService();
            else {
                initializationComplete(true, Level.DEBUG);
                return;
            }
        } catch(Exception e) {
            throw new ServiceException(
                    this.getClass().getSimpleName()+" could not be initialised due to an unexpected error. The stack trace follows.", e);
        }
        initializationComplete(true);
    }

    /**
     * This method is called after the web application has been
     * started up. In this way jobs that has to be performed
     * right after startup can be scheduled, if implementing
     * sub-classes overwrite this method.
     * In order to work correctly, the method 
     * {@link #needDelayedInit()} needs to be overwritten and
     * must return <code>true</code>
     * 
     * @throws ServiceException If this service could not be initialised.
     */
    protected void delayedInitService() throws Exception {}

    /**
     * @return Indicates whether this service needs to be initialised
     *         in a delayed manner or not.
     */
    protected boolean needDelayedInit() {
        return false;
    }

    /**
     * Shuts down this service with the given arguments.
     * 
     * @param args Arguments for shutdown of this service.
     * @throws ServiceException If this service could not be shutdown.
     */
    public void shutdown(Object... args) throws ServiceException {
        checkPossibleShutdown();
        try {
            shutdownService(args);
        } catch(Exception e) {
            throw new ServiceException(
                    this.getClass().getSimpleName()+" could not be shut down due to an unexpected error. The stack trace follows.", e);
        }
        shutdownComplete();
    }

    protected abstract void shutdownService(Object... args) throws Exception;

    /**
     * @return Whether this application-wide service has been initialised or not.
     */
    public boolean isInitialized() {
        return initialized;
    }

    /**
     * @return Whether this application-wide service has been initialised in a delayed manner or not.
     */
    public boolean isDelayedInitialised() {
        return delayedInitialized;
    }

    /**
     * @return Whether this application-wide service has been shutdown or not.
     */
    public boolean isShutdown() {
        return shutdown;
    }

    private void initializationComplete(boolean delayed) {
        initializationComplete(delayed, Level.INFO);
    }

    private void initializationComplete(boolean delayed, Level lvl) {
        String initDuration = UIUtil.formatMillis(System.currentTimeMillis() - (delayed?delayedInitialisationTime.getTime():initialisationTime.getTime()));
        log.log(lvl, this.getClass().getSimpleName()+" initialised in "+initDuration+".");
        if(delayed)
            delayedInitialized = true;
        else
            initialized = true;
    }

    private void shutdownComplete() {
        log.info(this.getClass().getSimpleName()+" has been shut down.");
        shutdown = true;
        initialized = false;
        delayedInitialized = false;
    }

    private void checkPossibleInitialization(boolean initialized) {
        if(initialized) {
            String errorMsg = this.getClass().getSimpleName()+" is already initialised!";
            throw new IllegalStateException(errorMsg);
        }
    }

    private void checkPossibleShutdown() {
        if(!initialized || shutdown) {
            String errorMsg = this.getClass().getSimpleName()+" is "+(shutdown?"already shutdown!":"not (yet?) initialised!");
            throw new IllegalStateException(errorMsg);
        }
    }
}

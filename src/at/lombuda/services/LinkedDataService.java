package at.lombuda.services;

import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.vocabulary.OWL;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.SKOS;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import at.lombuda.daos.DaoManager;
import at.lombuda.daos.FileDao;
import at.lombuda.daos.SparqlDao;
import at.lombuda.daos.consts.FileNameConsts;
import at.lombuda.daos.consts.voc.LombudaVoc;
import at.lombuda.entities.BudgetProfileAggregation;
import at.lombuda.entities.BudgetProfileAggregation.BPAType;
import at.lombuda.entities.CsvMetadata;
import at.lombuda.entities.DatasetStatistic;
import at.lombuda.entities.IntegrityConstraint;
import at.lombuda.entities.enums.CodeListType;
import at.lombuda.entities.enums.FiscalState;
import at.lombuda.exceptions.DaoException;
import at.lombuda.exceptions.DataAlreadyExistsException;
import at.lombuda.exceptions.MissingCsvColumnException;
import at.lombuda.exceptions.ParsingException;
import at.lombuda.exceptions.PropertyNotFoundException;
import at.lombuda.services.util.BudgetDataModelGenerator;
import at.lombuda.services.util.BudgetProfileGenerator;
import at.lombuda.services.util.ModelUtil;
import at.lombuda.services.util.UIUtil;

/**
 * Main service handling all operations regarding saved 
 * and to-be-saved LinkedData
 */
public class LinkedDataService extends AbstractService {
    private static LinkedDataService instance;

    private static int datasetRequestBoundary = 15*60*1000;
    private static int dataSliceUpdateBoundary = 15*60*1000;
    private static String defaultCsvEncodingName = "Windows-1252";
    private static boolean saveImportsAsFile = false;
    private static Charset csvEncoding = Charset.forName(defaultCsvEncodingName);
    private static CSVFormat csvFormat = CSVFormat.DEFAULT.withDelimiter(';').withHeader();
    private static String pubbyRefreshSubjectCacheUrl = null;

    private SparqlDao dao;
    private FileDao fileDao;

    private DatasetStatistic datasetStatistic;
    private Map<String, List<String>> soas;
    private long lastSliceUpdate;
    private Map<String,IntegrityConstraint> integrityConstraints;

    @Override
    protected void initService(Object... args) throws Exception {
        // load properties
        this.loadProperties();

        // get DAO references for SPARQL endpoint and file access
        dao     = DaoManager.getInstance().getSparqlDao();
        fileDao = DaoManager.getInstance().getFileDao();
    }

    @Override
    protected void delayedInitService() {
        // get initial statistic
        log.info("Get initial dataset statistics ...");
        datasetStatistic = dao.getDatasetStatistic();

        if(datasetStatistic != null)
            log.info("Current saved dataset at '"+dao.getSparqlEndpointUrl()+"' contains "+
                    datasetStatistic.getNumOfObservations()+" observation"+(datasetStatistic.getNumOfObservations()>1?"s":"")+".");

        // get initial data slices
        log.info("Get all municipalities and fiscal years with available statements of account ...");
        soas = dao.getStatmentsOfAccountSlices();
        lastSliceUpdate = System.currentTimeMillis();
        if(soas.isEmpty())
            log.warn("No municipalities and fiscal years found as valid data slices from statements of account.");
        else
            for(String m:soas.keySet())
                log.info("> '"+m+"' for fiscal years: "+soas.get(m));

        // check initially integrity constraints
        this.doIntegrityCheck();
    }

    private void loadProperties() throws PropertyNotFoundException {
        PropertiesService ps = PropertiesService.getInstance();

        // refresh boundaries
        Integer datasetRequestBoundary = ps.getProperty("service.data.statisticRefreshBoundary", false, Integer.class);
        if(datasetRequestBoundary != null)
            LinkedDataService.datasetRequestBoundary = datasetRequestBoundary*60*1000;
        Integer dataSliceRequestBoundary = ps.getProperty("service.data.dataSliceRefreshBoundary", false, Integer.class);
        if(dataSliceRequestBoundary != null)
            LinkedDataService.dataSliceUpdateBoundary = dataSliceRequestBoundary*60*1000;

        // default CSV encoding
        String csvEncodingName = ps.getProperty("service.data.csvEncoding", String.class, defaultCsvEncodingName);
        if(Charset.isSupported(csvEncodingName))
            csvEncoding = Charset.forName(csvEncodingName);
        else
            log.warn("Unknown encoding '"+csvEncodingName+"' for CSV parsing; use default encoding '"+defaultCsvEncodingName+"' for future processing.");

        // file saving during import
        saveImportsAsFile = ps.getProperty("service.data.import.saveAsFile", Boolean.class, saveImportsAsFile);

        // get pubby refresh URL if available
        pubbyRefreshSubjectCacheUrl = ps.getProperty("service.pubby.subjectRefreshUrl", false, String.class);
    }

    /**
     * @see #getDatasetStatistic(boolean)
     */
    public DatasetStatistic getDatasetStatistic() {
        return this.getDatasetStatistic(false);
    }

    /**
     * This method will return current statistics
     * for the backed dataset, after <b>initial</b>
     * successful initialisation. Before the latter
     * (or in case of any initialisation error)
     * the result will be null.
     * 
     * @param forceRefresh This will force a refresh
     *           of the cached statistic, if true
     * 
     * @return The current dataset statistics or null
     */
    public DatasetStatistic getDatasetStatistic(boolean forceRefresh) {
        if(datasetStatistic == null)
            return null;

        // refresh the statistic if forced by caller
        if(forceRefresh) {
            log.info("Triggered forced refresh of last dataset statistic made at '"+datasetStatistic.getCreationTime()+"'.");
            datasetStatistic = dao.getDatasetStatistic();

            return datasetStatistic;
        }

        // otherwise refresh statistic if it is outdated by time boundary
        long interval = System.currentTimeMillis() - datasetStatistic.getCreationTime().getTime();

        if(interval > datasetRequestBoundary) {
            log.info("Last dataset statistic was made at '"+datasetStatistic.getCreationTime()+"'. Boundary exceeded - create new one.");
            datasetStatistic = dao.getDatasetStatistic();
        }

        return datasetStatistic;
    }

    /**
     * Refreshes the external subject cache in 
     * the sister application "Pubby" (if available).
     * 
     * @return Whether the operation was successful
     *         or not
     */
    public boolean refreshPubbySubjectCache() {
        if(pubbyRefreshSubjectCacheUrl == null) {
            log.warn("Pubby subject cache refresh not possible. Set refresh URL is null.");
            return false;
        }

        try {
            long startMillis = System.currentTimeMillis();
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<String> response = restTemplate.getForEntity(pubbyRefreshSubjectCacheUrl, String.class);
            long duration = System.currentTimeMillis() - startMillis;
            boolean success = response.getStatusCode() == HttpStatus.OK;
            if(success)
                log.info("Successfully refreshed Pubby's subject cache in "+UIUtil.formatMillis(duration)+".");
            else
                log.error("Could not refresh Pubby's subject cache - server responded with error: "
                        +response.getStatusCode().value()+" "+response.getStatusCode().getReasonPhrase());
            return success;
        } catch (Exception e) {
            log.error("Could not refresh Pubby's subject cache due to an unexpected exception.", e);
            return false;
        }
    }

    /**
     * @see #getStatmentsOfAccountSlices(boolean)
     */
    public Map<String, List<String>> getStatmentsOfAccountSlices() {
        return this.getStatmentsOfAccountSlices(false);
    }

    /**
     * This method return all municipalities and respective 
     * fiscal years with available data slices for statements 
     * of account from the backed dataset.
     * The data is normally cached for a predefined amount of
     * time and a direct call to retrieve all currently saved
     * (possibly new) data slices is made automatically during
     * invocation in case the cache is outdated.
     * 
     * @param forceRefresh This will force a refresh
     *           of the cached statistic, if true
     * @return A map with a String describing the municipality in 
     *         a human-readable way for which data slices with 
     *         statements of account are available. The values 
     *         represent fiscal years for which such data slices 
     *         are available for the given municipality as key.
     *         The result is null in case no information could be
     *         retrieved since the application startup due to some
     *         error.
     */
    public Map<String, List<String>> getStatmentsOfAccountSlices(boolean forceRefresh) {
        if(soas == null)
            return null;

        // refresh available data slices, if forced by caller
        if(forceRefresh) {
            log.info("Triggered forced query for available data slices.");
            soas = dao.getStatmentsOfAccountSlices();
            lastSliceUpdate = System.currentTimeMillis();

            return soas;
        }

        // refresh available data slices, if information is outdated by time boundary
        long interval = System.currentTimeMillis() - lastSliceUpdate;

        if(interval > dataSliceUpdateBoundary) {
            log.info("Last query for available data slices was made at '"+datasetStatistic.getCreationTime()+"'. Boundary exceeded - make new one.");
            soas = dao.getStatmentsOfAccountSlices();
            lastSliceUpdate = System.currentTimeMillis();
        }

        return soas;
    }

    /**
     * Queries the backed dataset for creating a full budget 
     * profile of the given fiscal document including all
     * sums, balances as well as headlines according to the
     * VRV 1995.
     * 
     * @param fiscalYear The fiscal year (in the format 'YYYY') 
     *                   of the budget profile to be created
     * @param municipality The municipality for which the budget 
     *                   profile aggregations should be created as 
     *                   given by {@link #getStatmentsOfAccountSlices()} 
     *                   as result map key
     * @param fiscalState The fiscal state of the numbers for the 
     *                   budget profile to be created
     * @return All budget profile aggregations (with sums, etc.) 
     *         as a list ordered according to the VRV (for direct
     *         display)
     */
    public List<BudgetProfileAggregation> getBudgetProfile(String fiscalYear, String municipality, FiscalState fiscalState) {
        // check given parameters
        if(!fiscalYear.matches("^\\d{4}$"))
            throw new IllegalArgumentException("Given fiscal year has not the correct format: "+fiscalYear);
        else if(municipality == null)
            throw new IllegalArgumentException("Given municipality null.");
        else if(soas != null && !soas.containsKey(municipality))// TODO check also for available VA when available in this service
            throw new IllegalArgumentException("Given municipality is not available.");
        else if(fiscalState == null)
            throw new IllegalArgumentException("Given fiscal state is null.");

        // get budget profile aggregations from backed dataset
        log.debug("Create VRV Budget Profile for FY"+fiscalYear+" & "+municipality+" ("+fiscalState+") ...");
        long startMillis = System.currentTimeMillis();
        List<BudgetProfileAggregation> bpa = dao.getBudgetProfileAggregations(fiscalYear, municipality, fiscalState.name());

        // generate full budget profile according to VRV
        BudgetProfileGenerator bpg = new BudgetProfileGenerator(bpa, getAllBudgetProfileHeadlinesAndSumsEntities());
        bpg.generate();
        List<BudgetProfileAggregation> budgetProfile = null;
        if(bpg.isGenerationSuccessful())
            budgetProfile = bpg.getBudgetProfile(); 
        long duration = System.currentTimeMillis() - startMillis;

        if(budgetProfile != null)
            log.debug("Successfully created VRV Budget Profile for FY"+fiscalYear+" & "+municipality+" ("+fiscalState+") in "+UIUtil.formatMillis(duration)+".");
        else
            log.warn("Some error seem to be occured during VRV Budget Profile generation ("+UIUtil.formatMillis(duration)+").");

        // return full budget profile 
        return budgetProfile;
    }

    private Map<String, BudgetProfileAggregation> getAllBudgetProfileHeadlinesAndSumsEntities() {
        Map<String, BudgetProfileAggregation> aggMap = new HashMap<String, BudgetProfileAggregation>();

        aggMap.put("I", new BudgetProfileAggregation(BPAType.H1,   "I",  "Profile"));

        aggMap.put("1",  new BudgetProfileAggregation(BPAType.H2,  "1",  "Operating Revenues"));
        aggMap.put("19", new BudgetProfileAggregation(BPAType.SUM, "19", "Sum #1: Operating Revenues"));
        aggMap.put("2",  new BudgetProfileAggregation(BPAType.H2,  "2",  "Operating Expenditures"));
        aggMap.put("29", new BudgetProfileAggregation(BPAType.SUM, "29", "Sum #2: Operating Expenditures"));
        aggMap.put("91", new BudgetProfileAggregation(BPAType.B2,  "91", "Balance #1: Operating Result"));

        aggMap.put("3",  new BudgetProfileAggregation(BPAType.H2,  "3",  "Revenues from Asset Accounting"));
        aggMap.put("39", new BudgetProfileAggregation(BPAType.SUM, "39", "Sum #3: Revenues from Asset Accounting"));
        aggMap.put("4",  new BudgetProfileAggregation(BPAType.H2,  "4",  "Expenditures from Asset Accounting"));
        aggMap.put("49", new BudgetProfileAggregation(BPAType.SUM, "49", "Sum #4: Expenditures from Asset Accounting"));
        aggMap.put("92", new BudgetProfileAggregation(BPAType.B2,  "92", "Balance #2: Asset Account Result w/o Financial Transactions"));

        aggMap.put("5",  new BudgetProfileAggregation(BPAType.H2,  "5",  "Revenues from Financial Transactions"));
        aggMap.put("59", new BudgetProfileAggregation(BPAType.SUM, "59", "Sum #5: Revenues from Financial Transactions"));
        aggMap.put("6",  new BudgetProfileAggregation(BPAType.H2,  "6",  "Expenditures from Financial Transactions"));
        aggMap.put("69", new BudgetProfileAggregation(BPAType.SUM, "69", "Sum #6: Expenditures from Financial Transactions"));
        aggMap.put("93", new BudgetProfileAggregation(BPAType.B2,  "93", "Balance #3: Financial Account Result"));

        aggMap.put("94", new BudgetProfileAggregation(BPAType.B1,  "94", "Balance #4: Annual Result w/o final settlement"));

        aggMap.put("II", new BudgetProfileAggregation(BPAType.H1,  "II", "Deduction of the Fiscal Balance"));
        aggMap.put("70", new BudgetProfileAggregation(BPAType.SUM, "70", "Annual Result w/o A85-89 & Financial Transactions"));
        aggMap.put("71", new BudgetProfileAggregation(BPAType.SUM, "71", "Clearing of Annual Result of A85-89"));
        aggMap.put("95", new BudgetProfileAggregation(BPAType.B1,  "95", "Fiscal Balance (\"Maastricht Result\")"));

        aggMap.put("III",new BudgetProfileAggregation(BPAType.H1,  "III","Total Budget Overview"));
        aggMap.put("80", new BudgetProfileAggregation(BPAType.LINE,"80", "Operating Revenues incl. those from Asset Accounting"));
        aggMap.put("79", new BudgetProfileAggregation(BPAType.SUM, "79", "Sum #7: Total Revenue"));
        aggMap.put("84", new BudgetProfileAggregation(BPAType.LINE,"84", "Operating Expenditures incl. those from Asset Accounting"));
        aggMap.put("89", new BudgetProfileAggregation(BPAType.SUM, "89", "Sum #8: Total Expenditure"));
        aggMap.put("99", new BudgetProfileAggregation(BPAType.B1,  "99", "Administrative Annual Result"));

        return aggMap;
    }

    /**
     * Rebuilds the backed dataset through models containing
     * the original data structure definition from the local 
     * storage.<br>
     * <b>The current backed dataset is <i>reset</i> during 
     * this operation and all saved data is lost!</b>
     * 
     * @return Whether the automatic rebuild was successful 
     *         or not
     */
    public boolean rebuildDataset() {
        // get models containing the original data structure definition from the local storage
        Model basicCodes    = fileDao.getFileAsModel(FileNameConsts.BASIC_CODES);
        Model dsd           = fileDao.getFileAsModel(FileNameConsts.DSD);
        Model approachCodes = fileDao.getFileAsModel(FileNameConsts.APPROACH_CODES);
        Model accountCodes  = fileDao.getFileAsModel(FileNameConsts.ACCOUNT_CODES);
        Model bpIndices     = fileDao.getFileAsModel(FileNameConsts.BPI);
        Model dataset       = fileDao.getFileAsModel(FileNameConsts.DATASET);

        // check the availability of all necessary models
        if(basicCodes == null || dsd == null) {
            log.error("One of the mandatory models or the files containing the "
                    + "respective data structure definition, respectively, "
                    + "necessary for rebuilding the dataset could not be loaded: "
                    + "[basicCodes = "+basicCodes+", dsd = "+dsd+"]");
            return false;
        }

        // reset backed dataset & check result
        boolean deletionSuccessful = dao.deleteEverything();
        if(!deletionSuccessful) {
            log.error("Resetting data store was not successful. Stop rebuild of dataset.");
            return false;
        }

        // insert (re-)models containing the original data structure definition & return result
        boolean insertionSuccessful = dao.insertModel(basicCodes);
        if(insertionSuccessful)
            insertionSuccessful = dao.insertModel(dsd);
        if(insertionSuccessful && approachCodes != null)
            insertionSuccessful = dao.insertModel(approachCodes);
        if(insertionSuccessful && accountCodes != null)
            insertionSuccessful = dao.insertModel(accountCodes);
        if(insertionSuccessful && bpIndices != null)
            insertionSuccessful = dao.insertModel(bpIndices);
        if(insertionSuccessful && dataset != null)
            insertionSuccessful = dao.insertModel(dataset);

        // close all generated models
        ModelUtil.closeModel(basicCodes);
        ModelUtil.closeModel(dsd);
        ModelUtil.closeModel(approachCodes);
        ModelUtil.closeModel(accountCodes);
        ModelUtil.closeModel(bpIndices);
        ModelUtil.closeModel(dataset);

        if(!insertionSuccessful) {
            log.error("Insertion of models containing the data structure definition was not successful. "
                    + "Incomplete rebuilding of the backed dataset.");
            return false;
        }

        return true;
    }

    /**
     * Parses the given CSV files and imports or transforms, respectively, 
     * the contained code list to a RDF model that will be saved in the 
     * local file storage.
     * 
     * @param codeListType the type of the given code list as CSV
     * @param csvFile the uploaded temporary CSV file
     * @return the number of records/codes that have been parsed 
     *         successfully
     * @throws MissingCsvColumnException If any mandatory column in the
     *         given CSV is missing
     */
    public int importCodeList(CodeListType codeListType, MultipartFile csvFile) 
            throws MissingCsvColumnException {
        // get variables according to given code list type
        String codeNs, codeFileName;
        Resource list, codeClass;
        switch (codeListType) {
        case APPROACHES:
            codeNs = LombudaVoc.NS_FC_CODES;
            list = LombudaVoc.LIST_APPROACHES;
            codeClass = LombudaVoc.CLASS_FC;
            codeFileName = FileNameConsts.APPROACH_CODES;
            break;
        case ACCOUNTS:
            codeNs = LombudaVoc.NS_EC_CODES;
            list = LombudaVoc.LIST_ACCOUNTS;
            codeClass = LombudaVoc.CLASS_EC;
            codeFileName = FileNameConsts.ACCOUNT_CODES;
            break;
        case BPI:
            codeNs = LombudaVoc.NS_BPI;
            list = LombudaVoc.LIST_BPI;
            codeClass = LombudaVoc.CLASS_BPI;
            codeFileName = FileNameConsts.BPI;
            break;

        default:
            throw new IllegalArgumentException("Unsupported code list type: "+codeListType);
        }

        try {
            // create CSV parser
            CSVParser csv = this.getCsvParser(csvFile);

            // check for mandatory headers
            Map<String, Integer> headers = csv.getHeaderMap();
            boolean hasCode = false;
            boolean hasLabel = false;
            boolean hasFiscalType = false;
            boolean hasEconomicClass = false;
            for(String header:headers.keySet()) {
                if(header.equalsIgnoreCase("code"))
                    hasCode = true;
                else if(header.matches("(?i)(label_[a-z]{2,3})"))
                    hasLabel = true;
                else if(codeListType == CodeListType.BPI && header.equalsIgnoreCase("ft"))
                    hasFiscalType = true;
                else if(codeListType == CodeListType.BPI && header.equalsIgnoreCase("ec"))
                    hasEconomicClass = true;
            }

            if(!hasCode || !hasLabel || 
                    (codeListType == CodeListType.BPI && (!hasFiscalType || !hasEconomicClass)))
                throw new MissingCsvColumnException("Missing mandatory column in given CSV file.");

            // create an empty model and add prefixes
            Model model = ModelFactory.createDefaultModel();
            ModelUtil.setGeneralNamespacePrefixes(model);

            // crate code caches i.a. for downward links
            List<Resource> lvl1Cache = new ArrayList<Resource>();
            List<Resource> lvl2Cache = new ArrayList<Resource>();
            Map<String, List<Resource>> lvl2ClassCache = new HashMap<String, List<Resource>>();
            Map<String, List<Resource>> lvl3ClassCache = new HashMap<String, List<Resource>>();
            Map<String, Resource> bpiCache = new HashMap<String, Resource>();

            // parse records
            int numOfParsedRecords = 0;
            for(CSVRecord record:csv) {
                // get code & all labels - and for budget profile indices: fiscal type & economic class
                Map<String, String> recordMap = record.toMap();
                String codeStr = null;
                String fiscalTypeStr = null;
                String economicClassStr = null;
                Map<String, String> labelMap = new HashMap<String, String>();
                for(Entry<String, String> recordEntry:recordMap.entrySet()) {
                    if(recordEntry.getKey().equalsIgnoreCase("code"))
                        codeStr = recordEntry.getValue().trim();
                    else if(recordEntry.getKey().matches("(?i)(label_[a-z]{2,3})"))
                        labelMap.put(recordEntry.getKey().toLowerCase(), recordEntry.getValue().trim());
                    else if(codeListType == CodeListType.BPI && recordEntry.getKey().equalsIgnoreCase("ft"))
                        fiscalTypeStr = recordEntry.getValue().trim();
                    else if(codeListType == CodeListType.BPI && recordEntry.getKey().equalsIgnoreCase("ec"))
                        economicClassStr = recordEntry.getValue().trim();
                }

                // if all necessary attributes are available, generate RDF resource for this code
                if(codeStr != null && !labelMap.isEmpty() && (codeListType != CodeListType.BPI || 
                        (fiscalTypeStr != null && economicClassStr != null))) {
                    // for budget profile indices: check, if index has already been generated
                    if(codeListType == CodeListType.BPI) {
                        Resource bpiCode = bpiCache.get(codeStr);
                        if(bpiCode != null) {
                            bpiCode.addProperty(SKOS.narrower, getEconomicClass(economicClassStr));
                            continue;
                        }
                    }

                    // create resource & add type
                    Resource code = model.createResource(codeNs+codeStr);
                    code.addProperty(RDF.type, SKOS.Concept);
                    code.addProperty(RDF.type, codeClass);

                    // add all found labels
                    for(Entry<String, String> recordEntry:labelMap.entrySet()) {
                        String[] splittedLabel = recordEntry.getKey().split("_");
                        if(splittedLabel.length != 2)
                            throw new IllegalArgumentException("Illegal label found: "+recordEntry.getKey());
                        String lang = recordEntry.getKey().split("_")[1];
                        code.addProperty(SKOS.prefLabel, model.createLiteral(recordEntry.getValue(), lang));
                    }

                    // add SKOS properties
                    code.addProperty(SKOS.inScheme, list);
                    code.addProperty(SKOS.notation, codeStr);

                    // add SKOS hierarchy properties (upwards) and cache for downwards links according to code level
                    int codeLevel = codeListType == CodeListType.BPI?1:codeStr.length();

                    if(codeLevel == 1) {
                        code.addProperty(SKOS.topConceptOf, list);

                        lvl1Cache.add(code);
                    } else if(codeLevel == 2) {
                        String parentCode = codeStr.substring(0, 1);
                        code.addProperty(SKOS.broader, 
                                ResourceFactory.createResource(codeNs+parentCode));

                        if(!lvl2ClassCache.containsKey(parentCode))
                            lvl2ClassCache.put(parentCode, new ArrayList<Resource>());

                        lvl2ClassCache.get(parentCode).add(code);

                        lvl2Cache.add(code);
                    } else if(codeLevel == 3) {
                        String parentCode = codeStr.substring(0, 2);
                        code.addProperty(SKOS.broader, 
                                ResourceFactory.createResource(codeNs+parentCode));

                        if(!lvl3ClassCache.containsKey(parentCode))
                            lvl3ClassCache.put(parentCode, new ArrayList<Resource>());

                        lvl3ClassCache.get(parentCode).add(code);
                    }

                    // for budget profile indices: add fiscal type, (first) economic class association & add new index to cache
                    if(codeListType == CodeListType.BPI) {
                        code.addProperty(LombudaVoc.PROP_FISCAL_TYPE, getFiscalType(fiscalTypeStr));
                        code.addProperty(SKOS.narrower, getEconomicClass(economicClassStr));
                        bpiCache.put(codeStr, code);
                    }

                    numOfParsedRecords++;
                }
            }
            csv.close();
            log.debug("Successfully received and parsed "+numOfParsedRecords+" "+codeListType.getSingularName()+" code records.");

            // for functional & economic classifications: add downwards links to complete SKOS hierarchy or code list graph, respectively
            if(codeListType != CodeListType.BPI) {
                this.addDownwardLinks(lvl1Cache, lvl2ClassCache);
                this.addDownwardLinks(lvl2Cache, lvl3ClassCache);
            }

            // add references to all level 1 codes to list declaration (as top concepts)
            Resource refList = model.createResource(list.getURI());
            for(Resource lvl1Code:lvl1Cache) {
                refList.addProperty(SKOS.hasTopConcept, lvl1Code);
            }

            // try to store generated model to local file store
            boolean isStored = fileDao.store(model, codeFileName);
            if(!isStored)
                numOfParsedRecords = 0;
            else
                log.debug("Successfully parsed "+codeListType.getSingularName()+" code records to local storage as '"+FileNameConsts.APPROACH_CODES+"'.");

            // close model
            model.close();

            // return number of parsed records
            return numOfParsedRecords;
        } catch (IOException e) {
            log.error("An unexpected error occurred during parsing of given CSV file.", e);
            return 0;
        }
    }

    private CSVParser getCsvParser(MultipartFile csvFile) throws IOException {
        return csvFormat.parse(new InputStreamReader(csvFile.getInputStream(), csvEncoding));
    }

    private void addDownwardLinks(List<Resource> resourceList, Map<String, List<Resource>> classCache) {
        for(Resource r:resourceList) {
            String approachCodeStr = r.getProperty(SKOS.notation).getString();
            if(classCache.containsKey(approachCodeStr))
                for(Resource childCode:classCache.get(approachCodeStr)) {
                    r.addProperty(SKOS.narrower, childCode);
                }
            else {
                // if no individual of child class is available generate pseudo child
                r.getModel().createResource(r.getURI()+"0").addProperty(OWL.sameAs, r);
            }
        }
    }

    private Resource getEconomicClass(String ecStr) {
        if(ecStr.matches("^\\d{3}$"))
            return ResourceFactory.createResource(LombudaVoc.NS_EC_CODES+ecStr);
        else
            throw new IllegalArgumentException("Given level 3-economic class does not exist: "+ecStr);
    }

    private Resource getFiscalType(String ftStr) {
        if(ftStr.matches("^(EXP|REV)$"))
            return ResourceFactory.createResource(LombudaVoc.NS_FT_CODES+ftStr);
        else
            throw new IllegalArgumentException("Given fiscal type does not exist: "+ftStr);
    }

    /**
     * Parses the given CSV file and imports and transforms, respectively, 
     * the contained budgetary to an RDF model that will be saved directly
     * to the backed dataset.
     * 
     * @param metadata the meta data for the data in the given CSV file
     * @param csvFile the uploaded temporary data given as CSV file
     * @return the number of data records that have been parsed and 
     *         imported successfully
     * @throws IOException If something goes wrong during loading of the 
     *         given CSV file
     * @throws ParsingException If something goes wrong during parsing of
     *         the given data.
     * @throws DataAlreadyExistsException  If the given data already exists 
     *         in the backed data set.
     */
    public int importBudgetData(CsvMetadata metadata, MultipartFile csvFile) 
            throws IOException, ParsingException, DataAlreadyExistsException {
        log.debug("Received metadata for CSV data transformation: "+metadata);

        // parse data and start transformation (TODO do it unsynchronised)
        BudgetDataModelGenerator bdmg = new BudgetDataModelGenerator(metadata, csvFile, csvEncoding, csvFormat);
        bdmg.transform();

        if(!bdmg.isTransformationSuccessful())// impossible, but check nevertheless
            throw new IllegalStateException("Something went terribily wrong... :(");

        // get results of successful transformation
        int numOfParsedRecords = bdmg.getNumOfParsedRecords();
        int numOfAggregatedAmounts = bdmg.getNumOfAggregatedAmounts();
        if(numOfAggregatedAmounts > 0)
            log.info("Aggregated "+numOfAggregatedAmounts+" given data records to fit 3-digit-long classifications without budgetary indicator.");
        Model budgetDataModel = bdmg.getModel();

        boolean isStorageSuccessful = false;
        // save transformed data into local file storage, if configured
        if(saveImportsAsFile)
            isStorageSuccessful = fileDao.store(budgetDataModel, FileNameConsts.LAST_DATA_IMPORT);

        // save transformed data into backed dataset
        if(!saveImportsAsFile || (saveImportsAsFile && isStorageSuccessful))
            isStorageSuccessful = dao.insertModel(budgetDataModel);

        // close generator class
        bdmg.close();

        // if storage is successful, return result; otherwise throw exception
        if(isStorageSuccessful)
            return numOfParsedRecords;
        else
            throw new IOException("Could not stor given budget data succesfully.");
    }

    /**
     * Performs a new check for all given integrity constraints 
     * saved in the application's file storage. The results are 
     * available after successful invocation of this method via 
     * {@link #getCurrentIntegrityConstraints()}. If anything 
     * goes wrong during retrieval of the respective integrity 
     * check or its check, it will not be in the result or will 
     * counted as violated, respectively.
     */
    public synchronized void doIntegrityCheck() {        
        // get all integrity constraints from file storage
        Map<String, IntegrityConstraint> ics = fileDao.getIntegrityConstraints();

        // perform respective query for each of them
        log.info("Start integrity check of backed dataset ...");
        boolean success = true;
        for(IntegrityConstraint ic:ics.values()) {
            try {
                dao.checkIntegrityConstraint(ic);
            } catch (DaoException e) {
                log.error("Could not perform integrity check for '"+ic.getLabel()+"' due to an unexpected DAO error. "
                        + "Assume that integrity constraint is violated.", e);
                ic.setViolated(true);
            }
            if(success && ic.isViolated())
                success = false;
        }

        // replace existing integrity constraints
        integrityConstraints = ics;

        // log result
        if(success)
            log.info("Succesfully performed "+ics.size()+" integrity checks of backed data set.");
        else
            log.warn("One or multiple integrity checks of the backed data set failed: "+this.getCurrentIntegrityConstraints().values().toString());
    }

    /**
     * @return A map with all integrity constraints (with the integrity 
     *         constraint's label as key (e.g. 'IC-19c')) and their status 
     *         from the last invocation of {@link #doIntegrityCheck()}.
     */
    public Map<String, IntegrityConstraint> getCurrentIntegrityConstraints() {
        return integrityConstraints;
    }

    /**
     * @return Whether all existing integrity checks have been performed
     *         successfully.
     * @throws IllegalStateException In case the integrity check has not
     *         been performed yet
     */
    public boolean areCurrentIntegrityConstraintsSuccessful() throws IllegalStateException {
        if(integrityConstraints != null) {
            for(IntegrityConstraint ic:integrityConstraints.values()) {
                if(ic.isViolated())
                    return false;
            }
            return true;
        }
        throw new IllegalStateException("Integrity checks have not been performed yet.");
    }

    @Override protected boolean needDelayedInit() { return true; }

    @Override protected void shutdownService(Object... args) throws Exception {}

    public static LinkedDataService getInstance() {
        if(instance == null)
            instance = new LinkedDataService();
        return instance;
    }
}

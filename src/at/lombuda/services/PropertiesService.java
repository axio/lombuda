package at.lombuda.services;

import java.io.IOException;

import at.lombuda.exceptions.PropertyNotFoundException;
import at.lombuda.exceptions.ServiceException;
import at.lombuda.services.util.PropertiesLoader;

public class PropertiesService extends AbstractService {
    private static final String GENERAL_APPLICATION_PROPERTIES_FILENAME = "app.properties";
    
    private static PropertiesService instance;

    private static PropertiesLoader serviceProperties;
    
    @Override
    protected void initService(Object... args) throws Exception {
        // get references to DAO-Layer for property retrieval
        serviceProperties = this.getPropertiesLoader(GENERAL_APPLICATION_PROPERTIES_FILENAME);
    }
    
    private PropertiesLoader getPropertiesLoader(String filename) throws ServiceException {
        return this.getPropertiesLoader(filename, null);
    }
    
    private PropertiesLoader getPropertiesLoader(String filename, String specificKey) throws ServiceException {
        try {
            return new PropertiesLoader(filename, specificKey);
        } catch (IOException e) {
            throw new ServiceException("Properties file '"+filename+"' was not found in classpath.", e);
        }
    }

    /**
     * Returns the value associated with the given key with
     * the option to indicate whether the property is mandatory or not.
     * 
     * @param key The key to be associated with a property's value.
     * @param mandatory Sets whether the returned property value should not be null nor empty.
     * @param clazz The class of the property.
     * @return The property value associated with the given key.
     * @throws PropertyNotFoundException If the returned property value is null or empty while
     *                                   it is mandatory.
     */
    @SuppressWarnings("unchecked")
    public <T> T getProperty(String key, boolean mandatory, Class<T> clazz) throws PropertyNotFoundException {
        // try to retrieve it from property file
        String property = serviceProperties.getProperty(key, mandatory);

        // perform type conversion for given property
        if(property == null)
            return null;
        else if(clazz == String.class)
            return (T) property;
        else if(clazz == Integer.class)
            return (T) new Integer(property);
        else if(clazz == Long.class)
            return (T) new Long(property);
        else if(clazz == Boolean.class)
            return (T) new Boolean(property);
        else 
            throw new IllegalArgumentException(clazz.getName()+" is not supported as property data type.");
    }
    
    /**
     * Returns the value associated with the given key with
     * the option to indicate whether the property is mandatory or not.
     * 
     * @param key The key to be associated with a property's value.
     * @param mandatory Sets whether the returned property value should not be null nor empty.
     * @return The property value associated with the given key as string.
     * @throws PropertyNotFoundException If the returned property value is null or empty while
     *                                   it is mandatory.
     */
    public String getProperty(String key, boolean mandatory) throws PropertyNotFoundException {        
        return this.getProperty(key, mandatory, String.class);
    }
    
    /**
     * Returns the properties instance specific property value associated with the given key.
     * 
     * @param key The key to be associated with a properties instance specific value as string.
     * @param clazz The class of the property.
     * @param defaultValue The default value, if the property is not found
     * @return The properties instance specific property value associated with the given key 
     *         or the defaultValue if the property was not found.
     */
    public <T> T getProperty(String key, Class<T> clazz, T defaultValue) {
        try {
            T property = this.getProperty(key, false, clazz);
            if(property == null)
                return defaultValue;
            else
                return this.getProperty(key, false, clazz);
        } catch (PropertyNotFoundException e) {
            log.warn("Property '"+key+"' not found in database. Change to default value.");
            return defaultValue;
        }
    }
    
    /**
     * Returns the properties instance specific property value associated with the given key.
     * 
     * @param key The key to be associated with a properties instance specific value as string.
     * @return The properties instance specific property value associated with the given key 
     *         or null if the property was not found.
     */
    public String getProperty(String key) {
        try {
            return this.getProperty(key, false, String.class);
        } catch (PropertyNotFoundException e) {
            log.warn("Property '"+key+"' not found in database.");
            return null;
        }
    }
    
    /**
     * Tests if the specified key is a key of some property.
     *  
     * @param key The key associated with a property's value.
     * @return if and only if the specified object is a key of some
     *         property value, as determined by its <code>name</code>; 
     *         false otherwise
     */
    public boolean hasProperty(String key) {
        try {
            return this.getProperty(key, false, String.class) != null;
        } catch (PropertyNotFoundException e) {
            return false;
        }
    }

    @Override
    protected void shutdownService(Object... args) throws Exception {}

    public static PropertiesService getInstance() {
        if(instance == null)
            instance = new PropertiesService();
        return instance;
    }
}

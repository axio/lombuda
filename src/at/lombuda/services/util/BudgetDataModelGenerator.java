package at.lombuda.services.util;

import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Set;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.jena.datatypes.RDFDatatype;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.vocabulary.DCTerms;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.multipart.MultipartFile;

import at.lombuda.daos.DaoManager;
import at.lombuda.daos.consts.voc.DataCubeVoc;
import at.lombuda.daos.consts.voc.LombudaVoc;
import at.lombuda.entities.CsvMetadata;
import at.lombuda.entities.CsvMetadata.ATTR;
import at.lombuda.entities.CsvMetadata.BCE;
import at.lombuda.entities.MunicipalityInfo;
import at.lombuda.entities.enums.FiscalState;
import at.lombuda.exceptions.DataAlreadyExistsException;
import at.lombuda.exceptions.MissingCsvColumnException;
import at.lombuda.exceptions.MunicipalityNotFoundException;
import at.lombuda.exceptions.ParsingException;
import at.lombuda.exceptions.PropertyMalformedException;
import at.lombuda.services.GeoNameService;

/**
 * This class serves as a generation or transformation
 * helper object, respectively, for given budget data 
 * (in a given non-RDF format - currently only CSV data 
 * is supported). Therefore, a meta data object for the
 * given data (format) needs to be given.<br>
 * The normal procedure to use this class is as follows:
 * <ul>
 * <li>Construct a helper object with the given data and
 *     some metadata (there are normally no preliminary 
 *     <i>general</i> syntactic or semantic checks made 
 *     on the given data, e.g. whether the given metadata
 *     is consistent or not)
 * <li>The model is generated via calling 
 *     {@link #transform()} in a synchronised way in the 
 *     <i>same</i> thread as the caller
 * <li>In case of any problem or for getting statistical
 *     data on the generated model, use the different 
 *     methods offered by this class
 * <li>In case everything went just fine, get the 
 *     generated model via {@link #getModel()}
 * </ul>
 */
public class BudgetDataModelGenerator {
    private static final Logger log = LogManager.getLogger(BudgetDataModelGenerator.class);
    private static final HashMap<String, MunicipalityInfo> municipalityCache = new HashMap<String, MunicipalityInfo>();

    private CsvMetadata metadata;
    private CSVParser csvParser;
    private Model model;
    private int numOfParsedRecords;
    private int aggregatedAmounts = 0;
    private boolean done = false;
    private boolean error = false;
    private boolean closed = false;

    private NumberFormat numberParser;
    private NumberFormat numberFormatter;
    private final HashMap<String, Resource> dataSliceCache = new HashMap<String, Resource>();
    private final HashMap<String, Resource> dataRecordCache = new HashMap<String, Resource>();

    /**
     * Constructs a generator helper for a given CSV file.
     * The CSV file is loaded immediately (in contrast to
     * the model generation)!
     * 
     * @param metadata the meta data for the data in the
     *        given CSV file
     * @param csvFile the data given as CSV file
     * @param csvEncoding the charset used for encoding the
     *        data in the given CSV file
     * @param csvFormat the general format of the given CSV
     *        file
     * @throws IOException If something goes wrong during
     *        loading of the given CSV file
     */
    public BudgetDataModelGenerator(CsvMetadata metadata, MultipartFile csvFile, 
            Charset csvEncoding, CSVFormat csvFormat) throws IOException {
        if(metadata == null || csvFile == null || csvEncoding == null || csvFormat == null)
            throw new IllegalArgumentException("One of the given parameters is null.");

        this.metadata = metadata;

        // load given CSV file
        csvParser = csvFormat.parse(new InputStreamReader(csvFile.getInputStream(), csvEncoding));

        // create an empty model and add prefixes
        model = ModelFactory.createDefaultModel();
        ModelUtil.setGeneralNamespacePrefixes(model);

        // create formatters
        numberParser = NumberFormat.getNumberInstance(Locale.GERMAN);
        numberParser.setParseIntegerOnly(false);
        numberFormatter = NumberFormat.getNumberInstance(Locale.US);
        numberFormatter.setGroupingUsed(false);
        numberFormatter.setMaximumFractionDigits(2);
        numberFormatter.setMinimumFractionDigits(2);
    }

    /**
     * Starts the transformation of the given data into
     * a RDF model.
     * 
     * @throws ParsingException If something goes wrong
     *         during parsing of the given data.
     * @throws DataAlreadyExistsException If the given
     *         data already exists in the backed data set.
     */
    public synchronized void transform() throws ParsingException, DataAlreadyExistsException {
        // check whether one-shot has been fired
        if(error)
            throw new IllegalStateException("I could not do that... Please, leave me alone (and try again with someone else)!");
        else if(done)
            throw new IllegalStateException("Generation was already done... Leave me alone!");

        try {
            // check CSV header consistency with metadata
            this.isCsvHeaderConsistent();

            // get source information and generate (model-wide consistent) objects
            if(!metadata.hasFixedValueFor(ATTR.sourceUrl) || !metadata.hasFixedValueFor(ATTR.sourceDesc))
                throw new ParsingException("Metadata for source of given CSV data is missing.");
            Resource sourceUrl = this.getFixedResource( ATTR.sourceUrl );
            Literal sourceDesc = this.getFixedLiteral( ATTR.sourceDesc, "en" );

            // get used budgetary classification encoding
            BCE budgetaryClassificationEncoding = metadata.getBudgetaryClassificationEncoding();

            // get other fixed attributes, if available
            String fixedFiscalYearStr   = metadata.getFixedValueFor(ATTR.fiscalYear);
            String fixedMunicipalityStr = metadata.getFixedValueFor(ATTR.municipality);
            String fixedFiscalStateStr  = metadata.getFixedValueFor(ATTR.fiscalState);

            // determine whether data record descriptions are available
            boolean hasDescriptions = metadata.hasColumnNameFor(ATTR.description);

            // parse records
            numOfParsedRecords = 0;
            for(CSVRecord record:csvParser) {
                Resource dataRecord = null;

                // get (possibly fixed) data
                String fiscalYearStr   = fixedFiscalYearStr!=null?fixedFiscalYearStr:record.get(getHeaderName(ATTR.fiscalYear));
                String municipalityStr = fixedMunicipalityStr!=null?fixedMunicipalityStr:record.get(getHeaderName(ATTR.municipality));
                String fiscalStateStr  = fixedFiscalStateStr!=null?fixedFiscalStateStr:record.get(getHeaderName(ATTR.fiscalState));

                // get measurement and (optional) description
                String amountStr       = record.get(getHeaderName(ATTR.amount));
                String descriptionStr  = hasDescriptions?record.get(getHeaderName(ATTR.description)):null;

                // generate data record according to given budgetary classification encoding
                if(budgetaryClassificationEncoding == BCE.combined) {
                    String budgetaryClassStr     = record.get(getHeaderName(ATTR.budgetaryClass));
                    dataRecord = this.getDataRecord(fiscalYearStr, municipalityStr, fiscalStateStr, budgetaryClassStr, amountStr, descriptionStr, sourceUrl, sourceDesc);
                } else if(budgetaryClassificationEncoding == BCE.separated) {
                    String budgetaryIndicatorStr = metadata.hasColumnNameFor(ATTR.budgetaryIndicator)?record.get(getHeaderName(ATTR.budgetaryIndicator)):null;
                    String functionalClassStr    = record.get(getHeaderName(ATTR.functionalClass));
                    String economicClassStr      = record.get(getHeaderName(ATTR.economicClass));
                    if(budgetaryIndicatorStr != null && !metadata.hasColumnNameFor(ATTR.fiscalType))
                        dataRecord = this.getDataRecord(fiscalYearStr, municipalityStr, fiscalStateStr, budgetaryIndicatorStr, functionalClassStr, economicClassStr, 
                                amountStr, descriptionStr, sourceUrl, sourceDesc);
                    else {
                        String fiscalTypeStr     = record.get(getHeaderName(ATTR.fiscalType));
                        dataRecord = this.getDataRecord(fiscalYearStr, municipalityStr, fiscalStateStr, fiscalTypeStr, functionalClassStr, economicClassStr, 
                                amountStr, budgetaryIndicatorStr, descriptionStr, sourceUrl, sourceDesc);
                    }
                } else
                    throw new ParsingException("Unsupport budgetary classification encoding: "+budgetaryClassificationEncoding);

                if(dataRecord != null)
                    numOfParsedRecords++;
            }
        } catch(ParsingException e) {
            error = true;
            log.error("Budget data model generation failed.", e);
            throw e;
        } finally {
            done = true;
            try {
                csvParser.close();
            } catch (IOException e) {
                log.warn("Could not close CSV parsing resources.", e);
            }
        } 
    }

    private void isCsvHeaderConsistent() throws MissingCsvColumnException {
        Set<String> headerNames = csvParser.getHeaderMap().keySet();
        for(ATTR a:ATTR.values()) {
            if(metadata.hasColumnNameFor(a)) {
                String definedColumnName = metadata.getColumnNameFor(a);
                if(!headerNames.contains(definedColumnName))
                    throw new MissingCsvColumnException("Cannot find definied column '"
                            +definedColumnName+"' in given CSV file.");
            }
        }
    }

    private String getHeaderName(ATTR attribute) {
        return metadata.getColumnNameFor(attribute);
    }

    private Resource getDataRecord(String fiscalYearStr, String municipalityStr, String fiscalStateStr,
            String budgetaryClass, String amountStr, String descriptionStr,
            Resource sourceUrl, Literal sourceDesc) throws ParsingException, DataAlreadyExistsException {
        // if a combined budgetary classification is given, separate it
        String budgetaryIndicatorStr, functionalClassStr, economicClassStr;
        if(budgetaryClass.matches("^\\d\\.\\d{3,6}\\.\\d{3,6}$")) {
            String[] budgetaryClassArray = budgetaryClass.split("\\.");
            budgetaryIndicatorStr = budgetaryClassArray[0];
            functionalClassStr = budgetaryClassArray[1];
            economicClassStr = budgetaryClassArray[2];
        } else
            throw new PropertyMalformedException("Given budgetary classficiation cannot be parsed: "+budgetaryClass);

        return this.getDataRecord(fiscalYearStr, municipalityStr, fiscalStateStr, budgetaryIndicatorStr, 
                functionalClassStr, economicClassStr, amountStr, descriptionStr, sourceUrl, sourceDesc);
    }

    private Resource getDataRecord(String fiscalYearStr, String municipalityStr, String fiscalStateStr,
            String budgetaryIndicatorStr, String functionalClassStr, String economicClassStr, 
            String amountStr, String descriptionStr,
            Resource sourceUrl, Literal sourceDesc) throws ParsingException, DataAlreadyExistsException {
        // if no fiscal type is given, try to get it from the budgetary indicator
        String fiscalTypeStr = null;
        switch (budgetaryIndicatorStr) {
        case "1":
        case "5":
        case "9":
            fiscalTypeStr = "EXP";
            break;
        case "2":
        case "6":
        case "0":
            fiscalTypeStr = "REV";
            break;
        }

        if(fiscalTypeStr == null)
            throw new PropertyMalformedException("No fiscal type could be determined via given budgetary indicator: "+budgetaryIndicatorStr);

        return this.getDataRecord(fiscalYearStr, municipalityStr, fiscalStateStr, fiscalTypeStr, 
                functionalClassStr, economicClassStr, amountStr, budgetaryIndicatorStr, descriptionStr, sourceUrl, sourceDesc);
    }

    private static final String fcName = "functional classification";
    private static final String ecName = "economic classification";
    private Resource getDataRecord(String fiscalYearStr, String municipalityStr, String fiscalStateStr,
            String fiscalTypeStr, String functionalClassStr, String economicClassStr, String amountStr, 
            String budgetaryIndicatorStr, String descriptionStr,
            Resource sourceUrl, Literal sourceDesc) throws ParsingException, DataAlreadyExistsException {
        // parse and check (yet unparsed) input parameters for data slice and, subsequently, this record
        MunicipalityInfo m  = getMunicipalityInfo(municipalityStr);
        FiscalState      fs = getFiscalState(fiscalStateStr);
        if(!fiscalYearStr.matches("^\\d{4}$"))
            throw new PropertyMalformedException("Given fiscal year has not the correct format: "+fiscalYearStr);

        // get data record, if it is already existing
        String dataRecordUri = ModelUtil.getDataRecordUri(fiscalYearStr, m.getMunicipalityCode(), fs.name(), 
                fiscalTypeStr, functionalClassStr, economicClassStr);
        if(dataRecordCache.containsKey(dataRecordUri)) {
            Resource dataRecord = dataRecordCache.get(dataRecordUri);

            // get new aggregated amount to model
            amountStr = this.getAggregatedAmount(dataRecord, amountStr);
            Literal newAmount = ResourceFactory.createTypedLiteral(amountStr, XSDDatatype.XSDdecimal);
            aggregatedAmounts++;

            // prepare additional description, if present
            Literal newDescription = descriptionStr!= null?ResourceFactory.createLangLiteral(descriptionStr, "de"):null;

            // add new literals to existing data record
            dataRecord.addLiteral(LombudaVoc.PROP_AMOUNT, newAmount);
            if(newDescription != null) dataRecord.addLiteral(LombudaVoc.PROP_DESCRIPTION, newDescription);

            // add additional budgetary indicator, if not present in existing data record
            if(budgetaryIndicatorStr != null) {
                Resource budgetaryIndicator = ResourceFactory.createResource(LombudaVoc.NS_BI_CODES+budgetaryIndicatorStr);
                if(!dataRecord.hasProperty(LombudaVoc.PROP_BUDGETARY_INDICATOR, budgetaryIndicator))
                    dataRecord.addProperty(LombudaVoc.PROP_BUDGETARY_INDICATOR, budgetaryIndicator);
            }

            // add comment about automated aggregation, if not present already
            if(!dataRecord.hasProperty(RDFS.comment)) {
                Literal comment = ResourceFactory.createLangLiteral("This data record is the result of the automated aggregation of "
                        + "multiple, more detailed data records of the original dataset within the given budgetary classification.", "en");
                dataRecord.addLiteral(RDFS.comment, comment);
            }

            return dataRecord;
        }

        // get data slice for this data record (incl. respective parsing)
        Resource dataSlice = this.getDataSlice(fiscalYearStr, m, fs, sourceUrl, sourceDesc);

        // parse and check (yet unparsed) input parameters for data record)
        if(budgetaryIndicatorStr!= null && !budgetaryIndicatorStr.matches("^(1|2|5|6|9|0)$"))
            throw new PropertyMalformedException("Given budgetary indicator does not exist: "+fiscalTypeStr);
        if(!fiscalTypeStr.matches("^(EXP|REV)$"))
            throw new PropertyMalformedException("Given fiscal type does not exist: "+fiscalTypeStr);
        functionalClassStr  = this.checkAndShortenClassification(fcName, functionalClassStr);
        economicClassStr    = this.checkAndShortenClassification(ecName, economicClassStr);
        amountStr           = this.transformAmountStr(amountStr);

        // prepare given data in RDF format
        String commonLabelStr = fiscalYearStr+" / "+m.getName()+" / "+fs.name()+": " +fiscalTypeStr+" - "
                +(budgetaryIndicatorStr!=null?budgetaryIndicatorStr+".":"")+functionalClassStr+"."+economicClassStr+")";
        Literal  labelEn            = ResourceFactory.createLangLiteral("Budget Data Record (FY"+commonLabelStr, "en");
        Literal  labelDe            = ResourceFactory.createLangLiteral("Budgetdatensatz (FJ"+commonLabelStr, "de");
        Resource fiscalType         = ResourceFactory.createResource(LombudaVoc.NS_FT_CODES+fiscalTypeStr);
        Resource functionalClass    = ResourceFactory.createResource(LombudaVoc.NS_FC_CODES+functionalClassStr);
        Resource economicClass      = ResourceFactory.createResource(LombudaVoc.NS_EC_CODES+economicClassStr);
        Literal  amount             = ResourceFactory.createTypedLiteral(amountStr, XSDDatatype.XSDdecimal);
        Resource budgetaryIndicator = budgetaryIndicatorStr!=null?ResourceFactory.createResource(LombudaVoc.NS_BI_CODES+budgetaryIndicatorStr):null;
        Literal  description        = descriptionStr!= null?ResourceFactory.createLangLiteral(descriptionStr, "de"):null;

        // create new resource for the given data record
        Resource dataRecord = model.createResource(dataRecordUri);
        dataRecord.addProperty(RDF.type, DataCubeVoc.CLASS_OBSERVATION);
        dataRecord.addLiteral(RDFS.label, labelEn);
        dataRecord.addLiteral(RDFS.label, labelDe);
        dataRecord.addProperty(DataCubeVoc.PROPERTY_DATA_SET, LombudaVoc.DATASET);
        dataRecord.addProperty(LombudaVoc.PROP_FISCAL_TYPE, fiscalType);
        dataRecord.addProperty(LombudaVoc.PROP_FUNCTIONAL_CLASS, functionalClass);
        dataRecord.addProperty(LombudaVoc.PROP_ECONOMIC_CLASS, economicClass);
        dataRecord.addLiteral(LombudaVoc.PROP_AMOUNT, amount);
        if(budgetaryIndicator != null) dataRecord.addProperty(LombudaVoc.PROP_BUDGETARY_INDICATOR, budgetaryIndicator);
        if(description != null) dataRecord.addLiteral(LombudaVoc.PROP_DESCRIPTION, description);

        // add data record reference to respective data slice
        dataSlice.addProperty(DataCubeVoc.PROPERTY_OBSERVATION, dataRecord);

        // add newly created resource for the given data record to cache and return it
        dataRecordCache.put(dataRecordUri, dataRecord);
        return dataRecord;
    }

    private String checkAndShortenClassification(String className, String classification) throws PropertyMalformedException {
        if(classification.matches("^\\d{3}$"))
            return classification;
        if(classification.matches("^\\d{4,6}$"))
            return classification.substring(0, 3);
        else
            throw new PropertyMalformedException("Given "+className+" does not exist: "+classification);
    }

    private String transformAmountStr(String amountStr) throws PropertyMalformedException {
        try {
            double doubleValue = numberParser.parse(amountStr).doubleValue();
            return numberFormatter.format(doubleValue);
        } catch (ParseException e) {
            throw new PropertyMalformedException("Given amount of cash flow cannot be parsed: "+amountStr, e);
        }
    }

    private String getAggregatedAmount(Resource r, String amountStr) throws PropertyMalformedException {
        try {
            // get currently saved amount from data record
            Statement savedAmountStmt = r.getProperty(LombudaVoc.PROP_AMOUNT);
            double savedAmount = savedAmountStmt.getDouble();

            // parse new, additional amount
            double doubleValue = numberParser.parse(amountStr).doubleValue();

            // remove current saved amount
            r.removeAll(LombudaVoc.PROP_AMOUNT);

            // calculate, format and return new aggregated amount
            double newDoubleValue = doubleValue + savedAmount;
            String newAmount = numberFormatter.format(newDoubleValue);

            log.trace("Duplicate data record '"+r.getURI()+"' will be aggregated: "
                    +savedAmountStmt.getString()+" + "+amountStr+" = "+newAmount);
            return newAmount;
        } catch (ParseException e) {
            throw new PropertyMalformedException("Given amount of cash flow cannot be parsed: "+amountStr, e);
        }

    }

    private Resource getDataSlice(String fiscalYearStr, MunicipalityInfo m, FiscalState fs, 
            Resource sourceUrl, Literal sourceDesc) throws ParsingException, DataAlreadyExistsException {
        // get existing resource for the given data slice, if possible
        String dataSliceUri = ModelUtil.getDataSliceUri(fiscalYearStr, m.getMunicipalityCode(), fs.name());
        if(dataSliceCache.containsKey(dataSliceUri))
            return dataSliceCache.get(dataSliceUri);

        // check whether given data slice is already present in backed dataset
        boolean isSaved = DaoManager.getInstance().getSparqlDao().isDataSliceExisting(ResourceFactory.createResource(dataSliceUri));
        if(isSaved)
            throw new DataAlreadyExistsException("Given data already exists in backed data store: "+dataSliceUri);

        // prepare given data in RDF format
        Literal labelEn = ResourceFactory.createLangLiteral(fs.getHumanReadableNameEn()+" for "+m.getName()+" from FY"+fiscalYearStr, "en");
        Literal labelDe = ResourceFactory.createLangLiteral(fs.getHumanReadableNameDe()+" für "+m.getName()+" vom FJ"+fiscalYearStr, "de");
        Literal comment = ResourceFactory.createLangLiteral("A data slice representing all municipal budget data from "
                +m.getName()+"'s "+fs.getHumanReadableNameEn().toLowerCase()+" for the fiscal year "+fiscalYearStr+".", "en");
        Literal fiscalYear = ResourceFactory.createTypedLiteral(fiscalYearStr, XSDDatatype.XSDgYear);

        // create new resource for the given data slice
        Resource dataSlice = model.createResource(dataSliceUri);
        dataSlice.addProperty(RDF.type, DataCubeVoc.CLASS_SLICE);
        dataSlice.addLiteral(RDFS.label, labelEn);
        dataSlice.addLiteral(RDFS.label, labelDe);
        dataSlice.addLiteral(RDFS.comment, comment);
        dataSlice.addProperty(DCTerms.source, sourceUrl);
        dataSlice.addLiteral(DCTerms.source, sourceDesc);
        dataSlice.addProperty(DataCubeVoc.PROPERTY_SLICE_STRUCTURE, LombudaVoc.SLICE_FISCAL_DOC);
        dataSlice.addLiteral(LombudaVoc.PROP_FISCAL_YEAR, fiscalYear);
        dataSlice.addProperty(LombudaVoc.PROP_MUNICIPALITY, m.getName()+" ("+m.getMunicipalityCode()+")");
        dataSlice.addProperty(LombudaVoc.PROP_MUNICIPALITY, m.getGeonameResource());
        dataSlice.addProperty(LombudaVoc.PROP_FISCAL_STATE, fs.getResource());

        // add data slice reference to dataset
        model.add(LombudaVoc.DATASET, DataCubeVoc.PROPERTY_SLICE, dataSlice);

        // add newly created resource for the given data slice to cache and return it
        dataSliceCache.put(dataSliceUri, dataSlice);
        return dataSlice;
    }

    private static synchronized MunicipalityInfo getMunicipalityInfo(String municipalityStr) 
            throws MunicipalityNotFoundException, PropertyMalformedException {
        // check whether information about the given municipality has already been queried
        MunicipalityInfo m = municipalityCache.get(municipalityStr);
        if(m != null)// if so return immediately
            return m;

        // otherwise guess input format (municipality code vs. municipality name)
        String municipalityName = null;
        String municipalityCode = null;
        if(municipalityStr.matches("^\\d{5}$"))
            municipalityCode = municipalityStr;
        else
            municipalityName = municipalityStr;

        // get municipality information from GeoNames web service
        m = GeoNameService.getInstance().getMunicipalityInfo(municipalityName, municipalityCode);

        // if no information can be found, throw exception and cancel transformation
        if(m == null)
            throw new MunicipalityNotFoundException("Could not find any information for given municipality: "+municipalityStr);

        // check whether received information is sufficient
        if(m.getGeonameId() <= 0 || m.getName() == null || m.getMunicipalityCode() == null || 
                m.getName().isEmpty() || m.getMunicipalityCode().length() != 5)
            throw new PropertyMalformedException("Received no sufficent information for given municipality: "+municipalityStr+" => "+m);

        // otherwise add information to cache and return it
        municipalityCache.put(municipalityStr, m);
        return m;
    }

    private static FiscalState getFiscalState(String fiscalStateStr) throws PropertyMalformedException {
        try {
            return FiscalState.valueOf(fiscalStateStr.toUpperCase());
        } catch(Exception e) {
            throw new PropertyMalformedException("Could not parse given fiscal state: "+fiscalStateStr, e);
        }
    }

    private Resource getFixedResource(ATTR attribute) {
        String resourceStr = metadata.getFixedValueFor(attribute);
        if(resourceStr == null)
            throw new IllegalArgumentException("Fixed value for resource not found in given metadata: "+attribute);

        return ResourceFactory.createResource(resourceStr);
    }

    private Literal getFixedLiteral(ATTR attribute, String lang) {
        String literalStr = metadata.getFixedValueFor(attribute);
        if(literalStr == null)
            throw new IllegalArgumentException("Fixed value for literal not found in given metadata: "+attribute);

        return ResourceFactory.createLangLiteral(literalStr, lang);
    }

    @SuppressWarnings("unused")
    private Literal getFixedLiteral(ATTR attribute, RDFDatatype datatype) {
        String literalStr = metadata.getFixedValueFor(attribute);
        if(literalStr == null)
            throw new IllegalArgumentException("Fixed value for literal not found in given metadata: "+attribute);

        return ResourceFactory.createTypedLiteral(literalStr, datatype);
    }

    /**
     * @return The number of parsed or transformed data records
     * @throws IllegalStateException If the method {@link #transform()} 
     *         has not been called
     */
    public int getNumOfParsedRecords() {
        if(!done)
            throw new IllegalStateException("Invalid method invocation: Given data was not (yet) transformed.");
        return numOfParsedRecords;
    }

    /**
     * @return the number of amount that have been aggregated in the 
     *         course of a performed transformation
     * @throws IllegalStateException If the method {@link #transform()} 
     *         has not been called
     */
    public int getNumOfAggregatedAmounts() {
        if(!done)
            throw new IllegalStateException("Invalid method invocation: Given data was not (yet) transformed.");
        return aggregatedAmounts;
    }

    /**
     * @return Whether the transformation has been called via 
     *         {@link #transform()} and finished already. The
     *         result does not make any indication whether the
     *         operation was successful (see also
     *         {@link #isTransformationSuccessful()})
     */
    public boolean isTransformationFinshed() {
        return done;
    }

    /**
     * @return Whether the transformation has been called via 
     *         {@link #transform()} and finished already <i>
     *         successfully</i>.
     */
    public boolean isTransformationSuccessful() {
        return done && !error;
    }

    /**
     * @return The transformed model after invocation of {@link #transform()}
     * @throws IllegalStateException If the method {@link #transform()} 
     *         has not been called successfully
     */
    public Model getModel() {
        if(!done || error)
            throw new IllegalStateException("Invalid method invocation: Given data was not (yet) transformed successfully.");

        return model;
    }

    /**
     * Close this helper class and free up resources held. 
     * @throws IllegalStateException If this method has
     *         already been invoked before
     */
    public void close() {
        if(closed)
            throw new IllegalStateException("This object has already been closed.");

        model.close();
        if(!csvParser.isClosed())
            try { csvParser.close(); } catch (IOException e) {}
        dataRecordCache.clear();
        dataSliceCache.clear();
    }
}
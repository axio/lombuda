package at.lombuda.services.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import at.lombuda.entities.BudgetProfileAggregation;

/**
 * This class serves as a generation helper object for 
 * given budget profile aggregations retrieved from the 
 * backed dataset. Therefore, these aggregations (as 
 * line items) as well as all empty aggregator items 
 * need to be given.<br>
 * The normal procedure to use this class is as follows:
 * <ul>
 * <li>Construct a helper object with the given data 
 *     (there are normally no preliminary 
 *     <i>general</i> syntactic or semantic checks made 
 *     on the given data, e.g. whether all indices of a
 *     normal budget profile are present)
 * <li>The whole budget profile is generated via calling 
 *     {@link #generate()} in a synchronised way in the 
 *     <i>same</i> thread as the caller
 * <li>In case of any problem or for getting statistical
 *     data on the generated model, use the different 
 *     methods offered by this class
 * <li>In case everything went just fine, get the 
 *     generated complete budget profile via
 *     {@link #getBudgetProfile()}
 * </ul>
 */
public class BudgetProfileGenerator {
    private static final Logger log = LogManager.getLogger(BudgetProfileGenerator.class);

    private List<BudgetProfileAggregation> lines;
    private Map<String, BudgetProfileAggregation> aggs;
    private List<BudgetProfileAggregation> result;

    private boolean done = false;
    private boolean error = false;

    /**
     * Constructs a generator helper for the given data.
     * 
     * @param lineItems all aggregations retrieved from 
     *                the backed data set as line items
     * @param emptyAggregators all empty aggregator items
     *                of a budget profile
     */
    public BudgetProfileGenerator(List<BudgetProfileAggregation> lineItems, Map<String, BudgetProfileAggregation> emptyAggregators) {
        if(lineItems == null || emptyAggregators == null)
            throw new IllegalArgumentException("One of the given parameters is null.");

        this.lines = lineItems;
        this.aggs = emptyAggregators;
    }

    /**
     * Starts the generation of a full budget profile
     * with the given data.
     * To retrieve the result of this operation call
     * {@link #getBudgetProfile()} after this method
     * finished.
     * This method can be only invoked once.
     */
    public synchronized void generate() {
        // check whether one-shot has been fired
        if(error)
            throw new IllegalStateException("I could not do that... Please, leave me alone (and try again with someone else)!");
        else if(done)
            throw new IllegalStateException("Generation was already done... Leave me alone!");

        // prepare result list & sum caches
        result = new ArrayList<BudgetProfileAggregation>();
        BigDecimal sum, sumRev, sumExp;
        BigDecimal sumA8589, sumRevA8589, sumExpA8589;

        // try to generate complete budget profile
        try {
            // I. PROFILE
            addAgg("I");
            // 1. Operating Revenues
            addAgg("1");
            sumRev = addAndSum("10","11","12","13","14","15","16","17","18");
            sumRevA8589 = sum(true,"10","11","12","13","14","15","16","17","18");
            addAgg("19", sumRev, sumRevA8589);
            // 2. Operating Expenditures
            addAgg("2");
            sumExp = addAndSum("20","21","22","23","24","25","26","27","28");
            sumExpA8589 = sum(true,"20","21","22","23","24","25","26","27","28");
            addAgg("29", sumExp, sumExpA8589);
            // Balance #1: Operating Result
            addAgg("91", sumRev.subtract(sumExp), sumRevA8589.subtract(sumExpA8589));

            // 3. Revenues from Asset Accounting
            addAgg("3");
            sumRev = addAndSum("30","31","32","33","34");
            sumRevA8589 = sum(true,"30","31","32","33","34");
            addAgg("39", sumRev, sumRevA8589);
            // 4. Expenditures from Asset Accounting
            addAgg("4");
            sumExp = addAndSum("40","41","42","43","44");
            sumExpA8589 = sum(true,"40","41","42","43","44");
            addAgg("49", sumExp, sumExpA8589);
            // Balance #2: Asset Account Result w/o Financial Trans.
            addAgg("92", sumRev.subtract(sumExp), sumRevA8589.subtract(sumExpA8589));

            // 5. Revenues from Financial Transactions
            addAgg("5");
            sumRev = addAndSum("50","51","52","53","54","55","56");
            sumRevA8589 = sum(true,"50","51","52","53","54","55","56");
            addAgg("59", sumRev, sumRevA8589);
            // 6. Expenditures from Financial Transactions
            addAgg("6");
            sumExp = addAndSum("60","61","62","63","64","65","66");
            sumExpA8589 = sum(true,"60","61","62","63","64","65","66");
            addAgg("69", sumExp, sumExpA8589);
            // Balance #3: Financial Account Result
            addAgg("93", sumRev.subtract(sumExp), sumRevA8589.subtract(sumExpA8589));

            // Balance #4: Annual Result w/o final settlement
            sum = sumAgg(false, "91", "92", "93");
            sumA8589 = sumAgg(true, "91", "92", "93");
            addAgg("94", sum, sumA8589);

            // II. DEDUCTION OF THE FISCAL BALANCE
            addAgg("II");
            // Annual Result w/o A85-89 & Financial Transactions
            sum = sumAggWithoutPublicCorporations("91", "92");
            addAgg("70", sum, new BigDecimal("0"));
            // Clearing of Annual Result of A85-89
            sumA8589 = sumAgg(true, "94");
            addAgg("71", sumA8589, new BigDecimal("0"));

            // Fiscal Balance ("Maastricht Result")
            addAgg("95", sum.add(sumA8589), new BigDecimal("0"));

            // III. TOTAL BUDGET OVERVIEW
            addAgg("III");
            // Total Revenue
            sum = sumAgg(false, "19", "39", "59");
            addAgg("80", sum, new BigDecimal("0"));
            sum = sum.add(addAndSum("81","82","83"));
            addAgg("79", sum, new BigDecimal("0"));
            BigDecimal totalRev = sum;
            // Total Expenditure
            sum = sumAgg(false, "29", "49", "69");
            addAgg("84", sum, new BigDecimal("0"));
            sum = sum.add(addAndSum("85","86","87"));
            addAgg("89", sum, new BigDecimal("0"));
            BigDecimal totalExp = sum;

            // Administrative Annual Result
            sum = totalRev.subtract(totalExp);
            addAgg("99", sum, new BigDecimal("0"));
        } catch (Exception e) {
            error = true;
            log.error("Budget data model generation failed.", e);
            throw e;
        } finally {
            done = true;
        }
    }

    private BigDecimal addAndSum(String... index) {
        BigDecimal sum = new BigDecimal("0");
        Set<String> indices = new HashSet<String>(Arrays.asList(index));
        for(BudgetProfileAggregation line:lines) {
            if(indices.contains(line.getIndex())) {
                result.add(line);
                sum = sum.add(line.getAmount());

                indices.remove(line.getIndex());
                if(indices.isEmpty())
                    return sum;
            }
        }

        return sum;
    }

    private void addAgg(String index) {
        BudgetProfileAggregation agg = aggs.get(index);
        if(agg == null)
            throw new IllegalArgumentException("Budget Profile Index unknown: "+index);

        result.add(agg);
    }

    private void addAgg(String index, BigDecimal amount, BigDecimal amountForPublicCorporations) {
        BudgetProfileAggregation agg = aggs.get(index);
        if(agg == null)
            throw new IllegalArgumentException("Budget Profile Index unknown: "+index);

        agg.setAmount(amount);
        agg.setAmountForPublicCorporations(amountForPublicCorporations);

        result.add(agg);
    }

    private BigDecimal sum(boolean onlyPublicCorporations, String... index) {
        BigDecimal sum = new BigDecimal("0");
        Set<String> indices = new HashSet<String>(Arrays.asList(index));
        for(BudgetProfileAggregation line:lines) {
            if(indices.contains(line.getIndex())) {
                sum = sum.add(getAmount(line, onlyPublicCorporations));

                indices.remove(line.getIndex());
                if(indices.isEmpty())
                    return sum;
            }
        }

        if(!indices.isEmpty())
            for(String i:indices) {
                BudgetProfileAggregation agg = aggs.get(i);
                if(agg != null)
                    sum = sum.add(getAmount(agg, onlyPublicCorporations));
            }

        return sum;
    }

    private BigDecimal sumAgg(boolean onlyPublicCorporations, String... index) {
        BigDecimal sum = new BigDecimal("0");
        Set<String> indices = new HashSet<String>(Arrays.asList(index));
        for(String i:indices) {
            BudgetProfileAggregation agg = aggs.get(i);
            if(agg != null)
                sum = sum.add(getAmount(agg, onlyPublicCorporations));
        }

        return sum;
    }

    private BigDecimal sumAggWithoutPublicCorporations(String... index) {
        BigDecimal sum = new BigDecimal("0");
        Set<String> indices = new HashSet<String>(Arrays.asList(index));
        for(String i:indices) {
            BudgetProfileAggregation agg = aggs.get(i);
            if(agg != null)
                sum = sum.add(agg.getAmountWithoutPublicCorporations());
        }

        return sum;
    }
    private BigDecimal getAmount(BudgetProfileAggregation agg, boolean onlyPublicCorporations) {
        return onlyPublicCorporations?agg.getAmountForPublicCorporations():agg.getAmount();
    }

    /**
     * @return Whether the generation has been triggered via 
     *         {@link #generate()} and finished already. The
     *         result does not make any indication whether the
     *         operation was successful (see also
     *         {@link #isGenerationSuccessful()} )
     */
    public boolean isGenerationFinshed() {
        return done;
    }

    /**
     * @return Whether the generation has been triggered via 
     *         {@link #generate()} and finished already <i>
     *         successfully</i>.
     */
    public boolean isGenerationSuccessful() {
        return done && !error;
    }

    /**
     * @return The generated Budget Profile model after invocation 
     *         of {@link #generate()} as an ordered list according
     *         to the VRV
     * @throws IllegalStateException If the method {@link #generate()}
     *         has not been called successfully
     */
    public  List<BudgetProfileAggregation> getBudgetProfile() {
        if(!done || error)
            throw new IllegalStateException("Invalid method invocation: Given data was not (yet) transformed successfully.");
        return result;
    }
}

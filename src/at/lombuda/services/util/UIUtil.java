package at.lombuda.services.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import at.lombuda.entities.enums.MessageType;

/**
 * Static service class for common utility methods for UserInterface
 */
public final class UIUtil {
    private static final Logger log = LogManager.getLogger(UIUtil.class);

    private static SimpleDateFormat secondsFormatter,
    millisDayFormatter, millisHoursFormatetr, millisMinutesFormatter, 
    millisSecondsFormatter, millisFormatter;

    /**
     * Formats the given duration in milliseconds to a readable presentation,
     * like "Xd HH:mm:ss.SSS".
     * 
     * @param duration Duration to format in milliseconds
     * @return Formated duration
     */
    public static String formatMillis(long duration) {
        if(duration < 0)
            throw new IllegalArgumentException("Duration must be greater than zero!");

        long durationInMillis = duration;

        long days = TimeUnit.MILLISECONDS.toDays(duration);
        duration -= TimeUnit.DAYS.toMillis(days);
        long hours = TimeUnit.MILLISECONDS.toHours(duration);
        duration -= TimeUnit.HOURS.toMillis(hours);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(duration);
        duration -= TimeUnit.MINUTES.toMillis(minutes);
        long seconds = TimeUnit.MILLISECONDS.toSeconds(duration);

        if(days > 0) {
            return millisDayFormatter.format(new Date(durationInMillis-86400000L)); 
        } else if(hours > 0) {
            return millisHoursFormatetr.format(new Date(durationInMillis)); 
        } else if(minutes > 0) {
            return millisMinutesFormatter.format(new Date(durationInMillis)); 
        } else if(seconds > 0) {
            return millisSecondsFormatter.format(new Date(durationInMillis)); 
        } else if(durationInMillis == 0) {
            return "<1ms"; 
        } else {
            return millisFormatter.format(new Date(durationInMillis)); 
        }
    }

    /**
     * Formats the given duration in seconds to a readable presentation,
     * as "HH:mm:ss" (fixed).
     * 
     * @param duration Duration to format in seconds
     * @return Formated duration
     */
    public static String formatSeconds(long duration) {
        if(secondsFormatter == null) {
            TimeZone tz = TimeZone.getTimeZone("UTC");
            secondsFormatter = new SimpleDateFormat("HH:mm:ss");
            secondsFormatter.setTimeZone(tz);
        }
        return secondsFormatter.format(new Date(duration*1000L));
    }

    /**
     * Calculates the amount of minutes that passed since the given date.
     * The amount of time is rounded down (so, 59s equal 0 minutes).
     * 
     * @param date Date to which the amount of minutes should be calculated
     * @return The number of minutes since the given date
     */
    public static int getMinutesSince(Date date) {
        double minutes = ((double) (System.currentTimeMillis() - date.getTime()))/((double) 60000);
        return (int) Math.floor(minutes);
    }

    /**
     * Generates a HTML-formatted label out of the given message and its type
     * 
     * @param msgType type or priority of the message
     * @param label message
     * @return HTML-formatted label
     */
    public static String getHtmlFormattedLabel(MessageType msgType, String label) {
        return "<span class=\""+msgType.getLabelStyleClassName()+" label\">"+label.toUpperCase()+"</span>";
    }

    /**
     * This method formats the given string which contains a log file
     * with HTML-span-tags for error and warning messages.
     * 
     * @param log An unformatted content of a log file.
     * @return The formatted log.
     */
    public static String getFormattedLogStr(String log) {
        try {
            StringBuilder formattedLogBuilder = new StringBuilder();

            BufferedReader logReader = new BufferedReader(new StringReader(log));
            String logLine;

            while ((logLine = logReader.readLine()) != null) {
                if(logLine.contains("[ERROR]") || logLine.contains("[FATAL]") || logLine.contains("[SCHWERWIEGEND]"))
                    logLine = "<span class=\"error\">"+logLine+"</span>";
                else if(logLine.contains("[WARN]") || logLine.contains("[WARNING]") || logLine.contains("[WARNUNG]"))
                    logLine = "<span class=\"warn\">"+logLine+"</span>";
                formattedLogBuilder.append(logLine+"\n");
            }

            return formattedLogBuilder.toString();
        } catch(IOException ex) {
            UIUtil.log.warn("Unexpected error during log file formatting. Unformatted logfile will be displayed", ex);
            return log;
        }
    }

    /**
     * Gets the stack trace from a Throwable as a String.
     * 
     * The result of this method vary by JDK version as this method uses 
     * {@link Throwable.printStackTrace(java.io.PrintWriter)}. <br>
     * <br>
     * On JDK1.3 and earlier, the cause exception will not be shown unless 
     * the specified throwable alters printStackTrace.
     * 
     * @param throwable the <code>Throwable</code> to be examined
     * @return the stack trace as generated by the exception's 
     *         <code>printStackTrace(PrintWriter)</code> method
     */
    public static String getStackTraceAsString(Throwable throwable) {
        StringWriter errors = new StringWriter();
        throwable.printStackTrace(new PrintWriter(errors));
        return errors.toString();
    }

    /**
     * Capitalise the first letter of the given string
     * @param str A string
     * @return A string with its first letter capitalised
     */
    public static String setFirstLetterToUpperCase(String str) {
        return str.substring(0,1).toUpperCase() + str.substring(1);
    }

    private static SimpleDateFormat getFormatter(String pattern) {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        sdf.setTimeZone(TimeZone.getTimeZone("GMT+0"));
        return sdf;
    }

    static {
        millisDayFormatter     = getFormatter("D'd 'HH:mm");
        millisHoursFormatetr   = getFormatter("HH:mm:ss");
        millisMinutesFormatter = getFormatter("mm:ss.SSS");
        millisSecondsFormatter = getFormatter("s.SSS'sec'");
        millisFormatter        = getFormatter("S'ms'");
    }

    private UIUtil() {} // Utility class, hide constructor.
}

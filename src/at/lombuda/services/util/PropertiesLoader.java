package at.lombuda.services.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import at.lombuda.exceptions.PropertyNotFoundException;

/**
 * This class loads the properties file specified in the constructor.
 */
public class PropertiesLoader {
	private final String PROPERTIES_FILENAME;
    private final Properties PROPERTIES = new Properties();
    private final String SPECIFIC_KEY;

    /**
     * Construct a properties loader instance for the given properties file and specific key which
     * is to be used as property key prefix of the properties file.
     * 
     * @param filename The name of the properties file in the classpath
     * @param specificKey The specific key which is to be used as property key prefix. If it is null,
     *                    no specific key prefix will be used for retrieving property values.
     * @throws IOException During class initialisation if the properties file is
     * missing in the classpath or cannot be loaded.
     */
    public PropertiesLoader(String filename, String specificKey) throws IOException {
        this.PROPERTIES_FILENAME = filename;
        this.SPECIFIC_KEY = specificKey;
        
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        InputStream propertiesFile = classLoader.getResourceAsStream(PROPERTIES_FILENAME);

        if (propertiesFile == null) 
            throw new FileNotFoundException(
                "Properties file '" + PROPERTIES_FILENAME + "' is missing in classpath.");

        PROPERTIES.load(propertiesFile);
    }
    
    /**
     * Construct a properties loader instance for the given properties file.
     * 
     * @param filename The name of the properties file in the classpath
     * @throws IOException During class initialisation if the properties file is
     * missing in the classpath or cannot be loaded.
     */
    public PropertiesLoader(String filename) throws IOException {
        this(filename, null);
    }

    /**
     * Returns the properties instance specific property value associated with the given key with
     * the option to indicate whether the property is mandatory or not.
     * 
     * @param key The key to be associated with a properties instance specific value.
     * @param mandatory Sets whether the returned property value should not be null nor empty.
     * @return The properties instance specific property value associated with the given key.
     * @throws PropertyNotFoundException If the returned property value is null or empty while
     * 									 it is mandatory.
     */
    public String getProperty(String key, boolean mandatory) throws PropertyNotFoundException {
    	String fullKey = this.getFullKey(key);
    	
        String property = PROPERTIES.getProperty(fullKey);

        if (property == null || property.trim().isEmpty()) {
            if (mandatory) {
                throw new PropertyNotFoundException("Required property '" + fullKey + "'"
                    + " is missing in properties file '" + PROPERTIES_FILENAME + "'.");
            } else {
                // Make empty value null. Empty Strings are evil.
                property = null;
            }
        }

        return property;
    }
    
    /**
     * Returns the properties instance specific property value associated with the given key..
     * 
     * @param key The key to be associated with a properties instance specific value.
     * @return The properties instance specific property value associated with the given key 
     *         or null if the property was not found.
     */
    public String getProperty(String key) {
    	try {
			return this.getProperty(key, false);
		} catch (PropertyNotFoundException e) {
			return null;
		}
    }
    
    /**
     * Tests if the specified key is a key of this properties instance.
     *  
     * @param key The key associated with a properties instance specific value.
     * @return if and only if the specified object is a key in this properties 
     *         instance, as determined by the <code>equals</code> method; 
     *         false otherwise
     */
    public boolean hasProperty(String key) {
        String fullKey = this.getFullKey(key);
        
        return PROPERTIES.containsKey(fullKey) && 
                PROPERTIES.getProperty(fullKey) != null && !PROPERTIES.getProperty(fullKey).trim().isEmpty();
    }
    
    /**
     * @return The filename mapped by this properties instance
     */
    public String getPropertiesFilename() {
    	return PROPERTIES_FILENAME;
    }
    
    /**
     * @return The specific key which is used as property key prefix.
     */
    public String getSpecificKey() {
    	return SPECIFIC_KEY;
    }
    
    /**
     * @return Whether this properties instance has a specific key,
     *  	   which is used as property key prefix.
     */
    public boolean hasSpecificKey() {
    	return SPECIFIC_KEY != null;
    }
    
    private String getFullKey(String key) {
        if(this.hasSpecificKey())
            return SPECIFIC_KEY + "." + key;
        else
            return key;
    }
}

package at.lombuda.services.util;

import org.apache.jena.query.Query;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.vocabulary.DCTerms;
import org.apache.jena.vocabulary.OWL;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;
import org.apache.jena.vocabulary.SKOS;
import org.apache.jena.vocabulary.XSD;

import at.lombuda.daos.consts.voc.DataCubeVoc;
import at.lombuda.daos.consts.voc.LombudaVoc;

/**
 * Static service class for common utility methods for RDF Models
 */
public class ModelUtil {

    /**
     * This method sets default prefixes for a given RDF model.
     * 
     * @param model A RDF model
     * @return The given model to allow cascading calls.
     */
    public static Model setGeneralNamespacePrefixes(Model model) {
        model.setNsPrefix("rdf", RDF.uri);
        model.setNsPrefix("rdfs", RDFS.uri);
        model.setNsPrefix("owl", OWL.getURI());
        model.setNsPrefix("xsd", XSD.getURI());
        model.setNsPrefix("dct", DCTerms.NS);
        model.setNsPrefix("skos", SKOS.uri);
        model.setNsPrefix("qb", DataCubeVoc.NS);

        model.setNsPrefix("lombuda",          LombudaVoc.NS_ONTOLOGY);
        model.setNsPrefix("lombuda-slice",    LombudaVoc.NS_SLICE);
        model.setNsPrefix("lombuda-code",     LombudaVoc.NS_CODES);
        model.setNsPrefix("lombuda-fs-code",  LombudaVoc.NS_FS_CODES);
        model.setNsPrefix("lombuda-ft-code",  LombudaVoc.NS_FT_CODES);
        model.setNsPrefix("lombuda-vrv-code", LombudaVoc.NS_VRV_CODES);
        model.setNsPrefix("lombuda-bi-code",  LombudaVoc.NS_BI_CODES);
        model.setNsPrefix("lombuda-fc-code",  LombudaVoc.NS_FC_CODES);
        model.setNsPrefix("lombuda-ec-code",  LombudaVoc.NS_EC_CODES);
        model.setNsPrefix("lombuda-bp-index", LombudaVoc.NS_BPI);
        model.setNsPrefix("lombudata",        LombudaVoc.NS);

        return model;
    }

    /**
     * This method sets default prefixes for a given SPARQL query.
     * 
     * @param query A SPARQL query
     * @return The given model to allow cascading calls.
     */
    public static Query setGeneralNamespacePrefixes(Query query) {
        query.setPrefix("rdf", RDF.uri);
        query.setPrefix("rdfs", RDFS.uri);
        query.setPrefix("owl", OWL.getURI());
        query.setPrefix("xsd", XSD.getURI());
        query.setPrefix("dct", DCTerms.NS);
        query.setPrefix("skos", SKOS.uri);
        query.setPrefix("qb", DataCubeVoc.NS);

        return query;
    }
    
    private static final Character URL_SEPARATOR = '/';
    
    /**
     * Returns the URI for the given data slice
     * 
     * @param fiscalYear a fiscal year (in 4-digit representation)
     * @param municipalityCode a municipal code (5-digits)
     * @param fiscalState the fiscal state (2-character code)
     * @return the URI for the given data slice as String
     */
    public static String getDataSliceUri(String fiscalYear, String municipalityCode, String fiscalState) {
        return LombudaVoc.NS_DATA+fiscalYear+URL_SEPARATOR+municipalityCode+URL_SEPARATOR+fiscalState;
    }

    /**
     * Returns the URI for the given data record
     * 
     * @param fiscalYear a fiscal year (in 4-digit representation)
     * @param municipalityCode a municipal code (5-digits)
     * @param fiscalState the fiscal state (2-character code)
     * @param fiscalType the fiscal type (3-character code)
     * @param functionalClass the functional classification (3-digit code)
     * @param economicClass the economic classification (3-digit code)
     * @return the URI for the given data record as String
     */
    public static String getDataRecordUri(String fiscalYear, String municipalityCode, String fiscalState,
            String fiscalType, String functionalClass, String economicClass) {
        return getDataSliceUri(fiscalYear, municipalityCode, fiscalState)+URL_SEPARATOR
                +fiscalType+URL_SEPARATOR+functionalClass+URL_SEPARATOR+economicClass;
    }

    /**
     * Closes a given model and checks before whether it is not
     * null in beforehand
     * 
     * @param model A RDF model
     */
    public static void closeModel(Model model) {
        if(model != null)
            model.close();
    }
    
    private ModelUtil() {} // Utility class, hide constructor.
}

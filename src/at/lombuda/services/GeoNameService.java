package at.lombuda.services;

import java.util.ArrayList;

import org.geonames.InsufficientStyleException;
import org.geonames.Style;
import org.geonames.Toponym;
import org.geonames.ToponymSearchCriteria;
import org.geonames.ToponymSearchResult;
import org.geonames.WebService;

import at.lombuda.entities.MunicipalityInfo;
import at.lombuda.exceptions.PropertyNotFoundException;
import at.lombuda.services.util.UIUtil;

/**
 * Service for all information retrievals from
 * GeoNames. DAO layer is handled by GeoNames'
 * own Java Client (included as non-maven library).
 */
public class GeoNameService extends AbstractService {
    public static final String GEONAMES_NS = "http://sws.geonames.org/";
    private static GeoNameService instance;

    private static String userName = "lombuda";
    private static Style defaultStyle = Style.FULL;
    private static boolean online = false;

    @Override
    protected void initService(Object... args) throws Exception {
        // load properties
        this.loadProperties();

        // initialise web service client
        WebService.setUserName(userName);
        log.info("GeoNames web service client initialised: "
                + "username = '"+WebService.getUserName()+"', "
                + "server = '"+WebService.getGeoNamesServer()+"', "
                + "requestStyle = '"+defaultStyle+"', "
                + "timeouts = "+WebService.getConnectTimeOut()/1000+"sec > "+WebService.getReadTimeOut()/1000+"sec");
    }

    @Override
    protected void delayedInitService() {
        // make test call
        long startMillis = System.currentTimeMillis();
        boolean online = this.performTestCall();
        long duration = System.currentTimeMillis() - startMillis;

        if(online)
            log.info("GeoNames web service test call successful (in "+UIUtil.formatMillis(duration)+").");      
        else
            log.warn("GeoNames web service test call was not successful (in "+UIUtil.formatMillis(duration)+"). "
                    + "This does not necessarily mean that a call is not possible, but is should be subject of major concern.");    
    }

    private void loadProperties() throws PropertyNotFoundException {
        PropertiesService ps = PropertiesService.getInstance();

        // default API user name
        userName = ps.getProperty("service.gn.api.userName", String.class, userName);
    }

    /**
     * Performs a test call in order to determine whether the 
     * GeoNames web service is online or reachable, respectively, 
     * or not
     * 
     * @return whether the GeoNames web service is online or 
     *         reachable, respectively, according to the test call
     *         or not
     */
    public boolean performTestCall() {
        // make test call & save/return result
        MunicipalityInfo m = this.getMunicipalityInfo("Gutau", "40603", false);
        log.debug("Retrieved GeoNames test response: "+m);

        online = m != null;
        return online;
    }

    /**
     * @return whether the GeoNames web service is online or 
     *         reachable, respectively, or not (this information 
     *         is gathered via a test call)
     */
    public boolean isWebserviceOnline() {
        return online;
    }

    /**
     * Tries to get information about a municipalities with
     * the given parameters via the GeoNames web service.
     * At least one parameter must be given (!= null).
     * 
     * @param name the name of the searched municipality
     * @param code the five-digit-long code code (German 
     *        'Gemeindekennziffer') of the searched
     *        municipality according to Statistik Austria
     * @return informations about the found municipality
     */
    public MunicipalityInfo getMunicipalityInfo(String name, String code) {
        return this.getMunicipalityInfo(name, code, true);
    }

    /**
     * @param logCall Whether the call of this method should be logged or not
     * @see #getMunicialityInfo(String, String)
     */
    protected MunicipalityInfo getMunicipalityInfo(String name, String code, boolean logCall) {
        // NOTE: this special wrapper for Vienna is necessary, because it 
        //       would NOT be found as ADM3 feature on GeoNames and NOTHING 
        //       exists there with code '90001'! So, to keep it consistent,
        //       plain and simple, this wrapper... it's ugly, I know.
        if((name != null && (name.equalsIgnoreCase("Wien") || name.equalsIgnoreCase("Vienna"))) || 
                (code != null && code.equals("90001")))
            return new MunicipalityInfo(2761333, "Vienna", "90001");


        try {
            boolean hasSearchCriteria = false;
            ToponymSearchCriteria searchCriteria = new ToponymSearchCriteria();
            searchCriteria.setStyle(defaultStyle);
            searchCriteria.setCountryCode("AT");
            searchCriteria.setFeatureCode("ADM3");

            if(name != null && !name.isEmpty()) {
                searchCriteria.setNameStartsWith(name);
                hasSearchCriteria = true;
            }
            if(code != null && !code.isEmpty()) {
                searchCriteria.setAdminCode3(code);
                hasSearchCriteria = true;
            }
            if(!hasSearchCriteria)
                throw new IllegalArgumentException("Missing parameter(s) for GeoNames query.");

            long startMillis = System.currentTimeMillis();
            ToponymSearchResult result = WebService.search(searchCriteria);
            ArrayList<MunicipalityInfo> municipalityResults = new ArrayList<MunicipalityInfo>();
            for(Toponym t:result.getToponyms()) {
                municipalityResults.add(transform(t));
            }
            if(logCall) {
                long duration = System.currentTimeMillis() - startMillis;
                log.info("Retrieved municipality information from GeoNames web service (in "+UIUtil.formatMillis(duration)+"): "+municipalityResults);    
            }

            if(municipalityResults.size() == 0)
                return null;
            if(municipalityResults.size() > 1)
                log.warn("Found multiple municipalities for given parameters; will choose the first one: "+municipalityResults);

            return municipalityResults.get(0);
        } catch (Exception e) {
            log.error("Unexpected error during municipality search on GeoNames.", e);
            return null;
        }
    }

    private MunicipalityInfo transform(Toponym toponym) {
        try {
            return new MunicipalityInfo(toponym.getGeoNameId(), toponym.getAdminName3(), toponym.getAdminCode3());
        } catch (InsufficientStyleException e) {
            log.error("Unexpected error during municipality search on GeoNames - skip unparsed attributes.", e);
            return new MunicipalityInfo(toponym.getGeoNameId(), toponym.getAdminName3(), null);
        }
    }

    @Override protected boolean needDelayedInit() { return true; }

    @Override
    protected void shutdownService(Object... args) throws Exception {}

    public static GeoNameService getInstance() {
        if(instance == null)
            instance = new GeoNameService();
        return instance;
    }
}

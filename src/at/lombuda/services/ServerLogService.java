package at.lombuda.services;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * This service class provides all means
 * to retrieve standard log files and their
 * contents from Tomcat's log directory.
 */
public class ServerLogService extends AbstractService {
    private static ServerLogService instance;

    @Override
    protected void initService(Object... args) throws Exception {
        List<String> logFilenames = this.getAllLogFilenames();
        log.debug("Found " + logFilenames.size() + " log files:");

        for(String logFilename:logFilenames)
            log.debug(" - '"+logFilename+"'.length() = "+this.getLog(logFilename).length());
    }

    /**
     * Searches Tomcat's log directory for all log files,
     * ignoring possible rolling files through date pattern.
     * 
     * @return A list of all log filenames in Tomcat's log 
     *         directory
     */
    public List<String> getAllLogFilenames() {
        File dir = Context.getInstance().getTomcatLogDir();
        File [] files = dir.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith(".log") || name.endsWith(".txt") ;
            }
        });

        ArrayList<String> filenames = new ArrayList<String>();
        for (File file : files) {
            String filename = file.getName().replaceFirst("\\.[0-9]{4}-[0-9]{2}-[0-9]{2}", "");
            if(!filenames.contains(filename))
                filenames.add(filename);
        }

        return filenames;
    }

    /**
     * Returns the contents of a given log file including
     * the latest three ones from rolled files through 
     * date pattern.
     * 
     * @see ServerLogService#getLog(String, int)
     * 
     * @param logFilename The name of the log file
     * @return The contents of the given log file in 
     *         String representation
     */
    public String getLog(String logFilename) {
        return this.getLog(logFilename, 3);
    }

    /**
     * Returns the contents of a given log file including
     * the ones from rolled files through date pattern.
     * 
     * @param logFilename The name of the log file
     * @param numberOfDays The number of days rolling log 
     *                     files should be considered
     * @return The contents of the given log file in 
     *         String representation
     */
    public String getLog(String logFilename, int numberOfDays) {
        Calendar cal = new GregorianCalendar();
        String logStr = "";
        String currentLogStr = null;
        boolean logMissingFile = false;
        for(int i = 0; i < numberOfDays;i++) {
            if(i == 0) {
                currentLogStr = this.getLog(logFilename, null, logMissingFile).trim();
                if(currentLogStr.isEmpty()) {
                    currentLogStr = this.getLog(logFilename, cal.getTime(), logMissingFile).trim();
                }
            } else {
                cal.add(Calendar.DATE, -1);
                currentLogStr = this.getLog(logFilename, cal.getTime(), logMissingFile).trim();
            }

            if(currentLogStr != null && !currentLogStr.isEmpty())
                logStr = currentLogStr + (!currentLogStr.endsWith("\n")?"\n":"") + logStr;
        }

        return logStr;
    }

    private static SimpleDateFormat LOG_DATE_FORMATTER = new SimpleDateFormat("yyyy-MM-dd");
    private String getLog(String logFilename, Date date, boolean logMissingFile) {
        File logFile =  new File(Context.getInstance().getTomcatLogPath() +
                (date!=null?logFilename.replaceFirst("\\.", "."+LOG_DATE_FORMATTER.format(date)+"."):logFilename));

        String logStr = "";
        if(!logFile.exists() && logMissingFile)
            log.error("'"+logFile.getAbsolutePath()+"' not found.");
        else if(logFile.exists()) {
            try {
                byte[] encoded = Files.readAllBytes(logFile.toPath());
                logStr = new String(encoded, Charset.defaultCharset());
            } catch (IOException e) {
                log.error("Unexpected error while reading '"+logFile.getName()+"'.", e);
            }
        }

        return logStr;
    }

    public static ServerLogService getInstance() {
        if(instance == null)
            instance = new ServerLogService();
        return instance;
    }

    @Override protected void shutdownService(Object... args) throws Exception {}
}
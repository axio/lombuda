package at.lombuda.services;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import at.lombuda.exceptions.ServiceException;

public class ServletContextListenerImpl implements ServletContextListener {
    private static final Logger log = LogManager.getLogger(ServletContextListenerImpl.class);

    @Override
    public void contextInitialized(ServletContextEvent context) {
        try {
            Context.getInstance().init(context.getServletContext());
            log.info("ServletContext started: "+context.getServletContext().getServerInfo());	
        } catch(ServiceException e) {
            Context.getInstance().initialisationError();
            log.fatal("An unexpected error occurred while initialising service layer.", e);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent context) {
        try {
            Context.getInstance().shutdown(context);
            log.info("ServletContext destroyed after "+Context.getInstance().getRunTimeStr()+".");
        } catch(ServiceException e) {
            log.fatal("An unexpected error occurred while shutting down service layer.", e);
        }
    }
}

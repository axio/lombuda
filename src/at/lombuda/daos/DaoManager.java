package at.lombuda.daos;

import at.lombuda.services.AbstractService;

/**
 * This class manages all Data Access Object-related operations. 
 * Primarily this covers the instantiation and initialisation of 
 * all these DAOs and the provision of these DAOs, in case they 
 * are needed by the service layer of this web application.
 */
public class DaoManager extends AbstractService {
    private static boolean isTestModeActive = false;

    private static DaoManager instance;
    private static SparqlDao sparqlDao;
    private static FileDao fileDao;

    @Override
    protected void initService(Object... args) throws Exception {
        if(args.length > 0 && args[0] instanceof Boolean) DaoManager.isTestModeActive = (Boolean) args[0];

        // initialising DAOs
        sparqlDao = new SparqlDao(isTestModeActive);
        fileDao = new FileDao(isTestModeActive);
    }

    /**
     * @return DAO for access to the backed SPARQL endpoint
     */
    public SparqlDao getSparqlDao() {
        return sparqlDao;
    }

    /**
     * @return DAO for application-based file access and storage
     */
    public FileDao getFileDao() {
        return fileDao;
    }

    @Override
    protected void shutdownService(Object... args) throws Exception {}

    public static DaoManager getInstance() {
        if(instance == null)
            instance = new DaoManager();
        return instance;
    }
}
package at.lombuda.daos.consts.voc;

import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;

/**
 * Collected constants for W3C's RDF Data Cube Vocabulary.
 * 
 * <P>All members of this class are immutable. 
 */
public class DataCubeVoc {
    // GENERAL NAMESPACES //
    public static final String NS = "http://purl.org/linked-data/cube#";

    // ONTOLOGY //

    // classes
    public static final Resource CLASS_OBSERVATION = resource(NS, "Observation");
    public static final Resource CLASS_SLICE       = resource(NS, "Slice");

    // properties
    public static final Property PROPERTY_OBSERVATION      = property(NS, "observation");
    public static final Property PROPERTY_SLICE            = property(NS, "slice");
    public static final Property PROPERTY_SLICE_STRUCTURE  = property(NS, "sliceStructure");
    public static final Property PROPERTY_DATA_SET         = property(NS, "dataSet");
    

    // PUBLIC METHODS //

    /** 
     * @return the base URI for this schema
     */
    public static String getURI() { return NS; }

    // PRIVATE //

    private static final Resource resource( String ns, String local )
    { return ResourceFactory.createResource( ns + local ); }

    private static final Property property( String ns, String local )
    { return ResourceFactory.createProperty( ns, local ); }

    /**
        The caller references the constants using <tt>Consts.EMPTY_STRING</tt>, 
        and so on. Thus, the caller should be prevented from constructing objects of 
        this class, by declaring this private constructor. 
     */
    private DataCubeVoc() {
        // this prevents even the native class from 
        // calling this constructor as well :
        throw new AssertionError();
    }
}

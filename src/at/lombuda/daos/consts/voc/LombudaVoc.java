package at.lombuda.daos.consts.voc;

import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;

/**
 * Collected constants for the LOMBuDa vocabulary.
 * 
 * <P>All members of this class are immutable. 
 */
public class LombudaVoc {
    // GENERAL NAMESPACES //

    public static final String NS            = "http://data.lombuda.at/";
    public static final String NS_ONTOLOGY   = NS+"ontology/";
    public static final String NS_SLICE      = NS_ONTOLOGY+"slice/";
    public static final String NS_CODES      = NS_ONTOLOGY+"code/";
    public static final String NS_FS_CODES   = NS_CODES+"fiscal-states/";
    public static final String NS_FT_CODES   = NS_CODES+"fiscal-types/";
    public static final String NS_VRV_CODES  = NS_CODES+"VRV/";
    public static final String NS_BI_CODES   = NS_VRV_CODES+"budgetary-indicators/";
    public static final String NS_FC_CODES   = NS_VRV_CODES+"approaches/";
    public static final String NS_EC_CODES   = NS_VRV_CODES+"accounts/";
    public static final String NS_BPI        = NS_VRV_CODES+"budget-profile-indices/";
    public static final String NS_DATA       = NS+"OGHD/";

    // ONTOLOGY //

    // classes
    public static final Resource CLASS_FC          = resource(NS_CODES, "FunctionalClass");
    public static final Resource CLASS_EC          = resource(NS_CODES, "EconomicClass");
    public static final Resource CLASS_BPI         = resource(NS_CODES, "BudgetProfileIndex");
    
    // slices
    public static final Resource SLICE_FISCAL_DOC  = resource(NS_SLICE, "fiscalDoc");

    // lists
    public static final Resource LIST_APPROACHES   = resource(NS_VRV_CODES, "approaches");
    public static final Resource LIST_ACCOUNTS     = resource(NS_VRV_CODES, "accounts");
    public static final Resource LIST_BPI          = resource(NS_VRV_CODES, "budget-profile-indices");

    // properties
    public static final Property PROP_FISCAL_YEAR         = property(NS_ONTOLOGY, "fiscalYear");
    public static final Property PROP_MUNICIPALITY        = property(NS_ONTOLOGY, "municipality");
    public static final Property PROP_FISCAL_STATE        = property(NS_ONTOLOGY, "fiscalState");
    public static final Property PROP_FISCAL_TYPE         = property(NS_ONTOLOGY, "fiscalType");
    public static final Property PROP_FUNCTIONAL_CLASS    = property(NS_ONTOLOGY, "functionalClassification");
    public static final Property PROP_ECONOMIC_CLASS      = property(NS_ONTOLOGY, "economicClassification");
    public static final Property PROP_AMOUNT              = property(NS_ONTOLOGY, "amount");
    public static final Property PROP_BUDGETARY_INDICATOR = property(NS_ONTOLOGY, "budgetaryIndicator");
    public static final Property PROP_DESCRIPTION         = property(NS_ONTOLOGY, "description");

    // DATA //
    public static final Resource DATASET           = resource(NS, "OGHD");

    // PUBLIC METHODS //

    /** 
     * @return the base URI for this schema
     */
    public static String getURI() { return NS; }

    // PRIVATE //

    private static final Resource resource( String ns, String local )
    { return ResourceFactory.createResource( ns + local ); }

    private static final Property property( String ns, String local )
    { return ResourceFactory.createProperty( ns, local ); }

    /**
        The caller references the constants using <tt>Consts.EMPTY_STRING</tt>, 
        and so on. Thus, the caller should be prevented from constructing objects of 
        this class, by declaring this private constructor. 
     */
    private LombudaVoc() {
        // this prevents even the native class from 
        // calling this constructor as well :
        throw new AssertionError();
    }
}

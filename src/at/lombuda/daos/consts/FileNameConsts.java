package at.lombuda.daos.consts;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class FileNameConsts {
    // RDF MODELS AS SERIALIZED TURTLE FILES //
    public static final String BASIC_CODES      = "code-lists.ttl";
    public static final String APPROACH_CODES   = "code-list-approaches.ttl";
    public static final String ACCOUNT_CODES    = "code-list-accounts.ttl";
    public static final String BPI              = "budget-profile-indices.ttl";
    public static final String DSD              = "dsd.ttl";
    public static final String DATASET          = "dataset.ttl";
    public static final String LAST_DATA_IMPORT = "last-budget-data-import.ttl";

    // QUERY TEMPLATES //
    public static final String BP_QUERY         = "bp agg.sq";
    public static final String BP_A85_89_QUERY  = "bp agg (only A85-89).sq";
    public static final String BPI_QUERY        = "bp indices.sq";
    public static final String M_QUERY          = "get municipalities.sq";

    /** Accepted filenames for upload */
    public static final Set<String> ACCEPTED_FILE_NAMES;

    // PRIVATE //

    static {
        HashSet<String> fileNames = new HashSet<String>();
        fileNames.add(BASIC_CODES);
        fileNames.add(APPROACH_CODES);
        fileNames.add(ACCOUNT_CODES);
        fileNames.add(BPI);
        fileNames.add(DSD);
        fileNames.add(DATASET);

        ACCEPTED_FILE_NAMES = Collections.unmodifiableSet(fileNames);
    }

    /**
        The caller references the constants using <tt>Consts.EMPTY_STRING</tt>, 
        and so on. Thus, the caller should be prevented from constructing objects of 
        this class, by declaring this private constructor. 
     */
    private FileNameConsts() {
        // this prevents even the native class from 
        // calling this constructor as well :
        throw new AssertionError();
    }
}

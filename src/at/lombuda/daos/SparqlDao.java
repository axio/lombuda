package at.lombuda.daos;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.jena.query.DatasetAccessor;
import org.apache.jena.query.DatasetAccessorFactory;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.ResultSetFormatter;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.update.UpdateExecutionFactory;
import org.apache.jena.update.UpdateFactory;
import org.apache.jena.update.UpdateProcessor;
import org.apache.jena.update.UpdateRequest;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.SKOS;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import at.lombuda.daos.consts.FileNameConsts;
import at.lombuda.daos.consts.voc.DataCubeVoc;
import at.lombuda.daos.consts.voc.LombudaVoc;
import at.lombuda.entities.BudgetProfileAggregation;
import at.lombuda.entities.BudgetProfileAggregation.BPAType;
import at.lombuda.entities.CodeCount;
import at.lombuda.entities.DatasetStatistic;
import at.lombuda.entities.IntegrityConstraint;
import at.lombuda.exceptions.DaoException;
import at.lombuda.services.PropertiesService;
import at.lombuda.services.util.UIUtil;

/**
 * This Data Access Object provides the means to
 * query and connect the configured SPARQL endpoint
 * for arbitrary data access without the need to 
 * handle the accessing technologies or connection
 * classes and their implementations, respectively.
 */
public class SparqlDao {
    protected static final Logger log = LogManager.getLogger(SparqlDao.class);

    private boolean isInTestMode;
    private String sparqlEndpointUrl = "http://lombuda.at/fuseki/data";
    private static final String QUERY_URL  = "/query";
    private static final String UPDATE_URL = "/update";
    private long queryTimeout = 10000;// default timeout for queries to SPARQL endpoint in milliseconds

    private Date lastQueryExecution;
    private long numOfQueries;
    private Date lastUpdateExecution;
    private long numOfUpdates;
    private boolean offline;

    protected SparqlDao(boolean isInTestMode) {
        this.isInTestMode = isInTestMode;
        this.offline = true;

        // try to load configured properties
        this.loadProperties();

        // prepare queries
        this.prepareStatisticQueries();
    }

    /**
     * @return The configured and currently used URL from 
     *         the SPARQL endpoint used for queries. 
     */
    public String getSparqlEndpointUrl() {
        return sparqlEndpointUrl;
    }

    /**
     * @return The configured timeout used for queries to 
     *         the remote SPARQL endpoint. 
     */
    public long getQueryTimeout() {
        return queryTimeout;
    }

    /**
     * @return the time of the last execution of a query 
     *         made by this DAO.
     */
    public Date getLastQueryExecution() {
        return lastQueryExecution;
    }

    /**
     * @return the time of the last execution of an update 
     *         made by this DAO.
     */
    public Date getLastUpdateExecution() {
        return lastUpdateExecution;
    }

    /**
     * @return the number of all SPARQL requests performed 
     *         by this application so far
     */
    public long getNumOfRequests() {
        return numOfQueries + numOfUpdates;
    }

    /**
     * @return whether the SPARQL can be considered as 
     *         offline with respect to this application 
     *         or not
     */
    public boolean isOffline() {
        return offline;
    }

    private void loadProperties() {
        PropertiesService ps = PropertiesService.getInstance();
        sparqlEndpointUrl    = ps.getProperty(isInTestMode?"dao.sparql.remoteEndpointUrl":"dao.sparql.endpointUrl", String.class, sparqlEndpointUrl);
        queryTimeout         = ps.getProperty("dao.sparql.queryTimeout ", Long.class, queryTimeout);
    }

    private void prepareStatisticQueries() {
        // count observations
        String queryString = "SELECT (COUNT(DISTINCT ?s) as ?count) WHERE { ?s a <"+DataCubeVoc.CLASS_OBSERVATION+"> . }";
        observationQuery = this.prepareQueryExecution(queryString);

        // count fiscal years
        queryString = "SELECT (COUNT(DISTINCT ?o) as ?count) WHERE { ?s <"+LombudaVoc.PROP_FISCAL_YEAR+"> ?o . FILTER (!isBlank(?o)) . }";
        fiscalYearQuery = this.prepareQueryExecution(queryString);

        // count municipalities
        queryString = "SELECT (COUNT(DISTINCT ?o) as ?count) WHERE { ?s <"+LombudaVoc.PROP_MUNICIPALITY+"> ?o . FILTER (!isBlank(?o) && isURI(?o)) . }";
        municipalQuery = this.prepareQueryExecution(queryString);

        // count number of codes
        queryString = "SELECT ?notation (COUNT(DISTINCT ?s) as ?count) WHERE { "
                + "?s a <"+SKOS.Concept+"> ; <"+SKOS.inScheme+"> ?b . "
                + "?b a <"+SKOS.ConceptScheme+"> . "
                + "?b <"+SKOS.notation+"> ?notation . } "
                + "GROUP BY ?notation ORDER BY ASC(?count)";
        codeCountQuery = this.prepareQueryExecution(queryString);
    }

    private QueryExecution observationQuery, fiscalYearQuery, municipalQuery, codeCountQuery;

    /**
     * @return Statistics on the backed dataset or null, 
     *         in case no statistic could be retrieved
     */
    public DatasetStatistic getDatasetStatistic() {
        try {
            // count observations
            List<QuerySolution> resultList = this.execSelect(observationQuery, false);
            int numOfObservations =  resultList.get(0).getLiteral("count").getInt();

            // count fiscal years
            resultList = this.execSelect(fiscalYearQuery, false);
            int numOfFiscalYears =  resultList.get(0).getLiteral("count").getInt();

            // count municipalities
            resultList = this.execSelect(municipalQuery, false);
            int numOfMunicipalities =  resultList.get(0).getLiteral("count").getInt();

            // count number of codes
            resultList = this.execSelect(codeCountQuery, false);
            List<CodeCount> codeCounts = new ArrayList<CodeCount>();
            for(QuerySolution r:resultList) {
                codeCounts.add(
                        new CodeCount(
                                r.getLiteral("notation").getString(), 
                                r.getLiteral("count").getInt()
                                ));
            }

            return new DatasetStatistic(numOfObservations, numOfFiscalYears, numOfMunicipalities, codeCounts);
        } catch (DaoException e) {
            log.error("Could not retrieve information on current dataset for statistic generation.", e);
            return null;
        }
    }

    /**
     * Checks whether the given data slice already 
     * exists in the backed dataset.
     * 
     * @param dataslice A data slice
     * @return Whether the given data slice already
     *         exists or not
     */
    public boolean isDataSliceExisting(Resource dataslice) {
        // prepare query
        String queryString = "ASK { <"+dataslice+"> <"+RDF.type+"> <"+DataCubeVoc.CLASS_SLICE+"> . }";
        QueryExecution askQuery = this.prepareQueryExecution(queryString);

        // execute and return immediately
        try {
            return this.execAsk(askQuery, true);
        } catch (DaoException e) {
            log.error("Could not retrieve information for given given data slice.", e);
            return true;
        }
    }

    /**
     * Retrieves all municipalities and respective fiscal years 
     * with available data slices for statements of account from 
     * the backed dataset
     * 
     * @return A map with a String describing the municipality in 
     *         a human-readable way for which data slices with 
     *         statements of account are available. The values 
     *         represent fiscal years for which such data slices 
     *         are available for the given municipality as key.
     */
    public Map<String, List<String>> getStatmentsOfAccountSlices() {
        // get query template
        String mQueryString = DaoManager.getInstance().getFileDao().getQueryString(FileNameConsts.M_QUERY);

        if(mQueryString  == null)
            throw new IllegalStateException("The necessary query template file to execute this query does not exist.");

        // replace placeholder in template with fixed parameter
        mQueryString = this.replacePlaceholder(mQueryString, "", "", "RA");

        try {
            // prepare result map
            Map<String, List<String>> municipalityMap = new HashMap<String, List<String>>();

            // execute query for municipalities and parse result
            List<QuerySolution> rawMs = this.execSelect(mQueryString);
            for(QuerySolution rawM:rawMs) {
                String municipalityStr = rawM.getLiteral("municipalityStr").getString();

                List<String> years = municipalityMap.get(municipalityStr);
                if(years == null) {
                    years = new ArrayList<String>();
                    municipalityMap.put(municipalityStr, years);
                }
                years.add(rawM.getLiteral("fiscalYear").getString());
            }

            return municipalityMap;
        } catch(Exception e) {
            log.error("Could not retrieve municipalities with available statements of account.", e);
            return null;
        }
    }

    /**
     * Queries the backed dataset for creating all budget profile 
     * aggregations of the given fiscal document.
     * 
     * @param fiscalYear The fiscal year of the budget profile to 
     *                   be created
     * @param municipality The municipality for which the budget 
     *                   profile aggregations should be created
     * @param fiscalState The fiscal state of the numbers for the 
     *                   budget profile to be created
     * @return All budget profile aggregations (without any sums) 
     *         as a list ordered by their indices
     */
    public List<BudgetProfileAggregation> getBudgetProfileAggregations(String fiscalYear, String municipality, String fiscalState) {
        // get query templates
        String bpiQueryString = DaoManager.getInstance().getFileDao().getQueryString(FileNameConsts.BPI_QUERY);
        String bpQueryString = DaoManager.getInstance().getFileDao().getQueryString(FileNameConsts.BP_QUERY);
        String bpA8589QueryString = DaoManager.getInstance().getFileDao().getQueryString(FileNameConsts.BP_A85_89_QUERY);

        if(bpiQueryString == null || bpQueryString  == null || bpA8589QueryString  == null)
            throw new IllegalStateException("One of the necessary query files to execute this query does not exist.");

        // replace placeholders in templates with parameters
        bpQueryString = this.replacePlaceholder(bpQueryString, fiscalYear, municipality, fiscalState);
        bpA8589QueryString = this.replacePlaceholder(bpA8589QueryString, fiscalYear, municipality, fiscalState);

        try {
            // prepare result list and cache map
            List<BudgetProfileAggregation> aggs = new ArrayList<BudgetProfileAggregation>();
            Map<String, BudgetProfileAggregation> aggMap = new HashMap<String, BudgetProfileAggregation>();

            // execute query for indices and parse result
            List<QuerySolution> rawBpis = this.execSelect(bpiQueryString);
            for(QuerySolution rawBpi:rawBpis) {
                String index = rawBpi.getLiteral("bpiNo").getString();
                BudgetProfileAggregation agg = new BudgetProfileAggregation(BPAType.LINE, 
                        index, 
                        rawBpi.getLiteral("bpiLabel").getString());

                aggs.add(agg);
                aggMap.put(index, agg);
            }

            // execute query for overall aggregation and parse result
            List<QuerySolution> rawBpAggs = this.execSelect(bpQueryString);
            for(QuerySolution rawAgg:rawBpAggs) {
                String index = rawAgg.getLiteral("bpiNo").getString();
                BudgetProfileAggregation agg = aggMap.get(index);

                if(index == null)
                    throw new IllegalStateException("No prepared aggregation found for given aggregation for BP #"+index+".");

                agg.setAmount(rawAgg.getLiteral("sum").getDouble());
            }

            // execute query for aggregation of amount for public corporations and parse result
            List<QuerySolution> rawA8589Aggs = this.execSelect(bpA8589QueryString);
            for(QuerySolution rawA8589Agg:rawA8589Aggs) {
                String index = rawA8589Agg.getLiteral("bpiNo").getString();
                BudgetProfileAggregation agg = aggMap.get(index);

                if(index == null)
                    throw new IllegalStateException("No prepared aggregation found for given aggregation for BP #"+index+".");

                agg.setAmountForPublicCorporations(rawA8589Agg.getLiteral("sum").getDouble());
            }

            return aggs;
        } catch(Exception e) {
            log.error("Could not retrieve budget profile aggregations for given given data slice ("+fiscalYear+"/"+municipality+"/"+fiscalState+").", e);
            return null;
        }
    }

    private String replacePlaceholder(String str, String fiscalYear, String municipality, String fiscalState) {
        str = str.replaceAll("\\$fiscalYear\\$",   "\""+fiscalYear+"\"")
                .replaceAll("\\$municipality\\$", "\""+municipality+"\"")
                .replaceAll("\\$fiscalState\\$",  fiscalState);
        return str;
    }

    /**
     * Checks the given integrity constraint for 
     * violation in the backed data set and 
     * reports/writes the result directly into 
     * the given object
     * 
     * @param ic An integrity constraint
     * @throws DaoException If the integrity could
     *         not be checked due to some error on 
     *         data set level
     */
    public void checkIntegrityConstraint(IntegrityConstraint ic) throws DaoException {
        // prepare query
        Query query = QueryFactory.create(ic.getSparqlQuery());
        ic.setSparqlQuery(query.toString());
        QueryExecution askQuery = this.prepareQueryExecution(query);

        // execute query and set result
        boolean violated = this.execAsk(askQuery, true);
        ic.setViolated(violated);
    }

    private List<QuerySolution> execSelect(String queryString) throws DaoException {
        // prepare query
        QueryExecution qexec = this.prepareQueryExecution(queryString);

        return this.execSelect(qexec, true);
    }

    private List<QuerySolution> execSelect(QueryExecution qexec, boolean closeQueryExecution) throws DaoException {        
        if(qexec == null)
            throw new IllegalArgumentException("QueryExecution cannot be null.");

        Query query = qexec.getQuery();
        if(query == null)
            throw new IllegalArgumentException("Missing query in given QueryExecution.");
        String queryString = query.toString();
        queryString = linearizeRequestString(queryString);
        try {
            long startMillis = System.currentTimeMillis();

            // (re-)prepare query, if necessary
            if(qexec.isClosed())
                qexec = this.prepareQueryExecution(queryString);

            // execute query & convert result
            ResultSet results = qexec.execSelect();
            List<QuerySolution> resultList = ResultSetFormatter.toList(results);

            // close execution, if necessary
            if(closeQueryExecution)
                qexec.close();

            // make statistical and status logging
            offline = false;
            numOfQueries++;
            lastQueryExecution = new Date();
            long duration = lastQueryExecution.getTime() - startMillis;
            int resultSize = resultList.size();
            log.debug("QUERY EXECUTION #"+numOfQueries+" ("+UIUtil.formatMillis(duration)+"): "+queryString+" - "+resultList.size()+" RESULT"+(resultSize>1?"S":""));

            // return result
            return resultList;
        } catch(Exception e) {
            offline = true;
            throw new DaoException("SPARQL query could not be performed due to error on data access level: "+queryString, e);
        }
    }

    private boolean execAsk(QueryExecution qexec, boolean closeQueryExecution) throws DaoException {        
        if(qexec == null)
            throw new IllegalArgumentException("QueryExecution cannot be null.");

        Query query = qexec.getQuery();
        if(query == null)
            throw new IllegalArgumentException("Missing query in given QueryExecution.");
        String queryString = query.toString();
        queryString = linearizeRequestString(queryString);
        try {
            long startMillis = System.currentTimeMillis();

            // (re-)prepare query, if necessary
            if(qexec.isClosed())
                qexec = this.prepareQueryExecution(queryString);

            // execute query & convert result
            boolean result = qexec.execAsk();

            // close execution, if necessary
            if(closeQueryExecution)
                qexec.close();

            // make statistical and status logging
            offline = false;
            numOfQueries++;
            lastQueryExecution = new Date();
            long duration = lastQueryExecution.getTime() - startMillis;
            log.debug("QUERY EXECUTION #"+numOfQueries+" ("+UIUtil.formatMillis(duration)+"): "+queryString+" - RESULT = "+result);

            // return result
            return result;
        } catch(Exception e) {
            offline = true;
            throw new DaoException("SPARQL query could not be performed due to error on data access level: "+queryString, e);
        }
    }

    private QueryExecution prepareQueryExecution(String queryString) {
        Query query = QueryFactory.create(queryString);
        return this.prepareQueryExecution(query);
    }

    private QueryExecution prepareQueryExecution(Query query) {
        QueryExecution qexec = QueryExecutionFactory.sparqlService(sparqlEndpointUrl+QUERY_URL, query);
        qexec.setTimeout(queryTimeout);

        return qexec;
    }

    /**
     * Adds all statements of the given model 
     * to the backed dataset.
     * 
     * @param model The model containing all 
     *              statements to be added
     * @return Whether the operation was 
     *         successful or not
     */
    public boolean insertModel(Model model) {
        try {
            long startMillis = System.currentTimeMillis();
            DatasetAccessor dataAccess = DatasetAccessorFactory.createHTTP(sparqlEndpointUrl);
            dataAccess.add(model);
            long duration = System.currentTimeMillis() - startMillis;
            log.debug("INSERT MODEL with "+model.size()+" statements ("+UIUtil.formatMillis(duration)+").");
            return true;
        } catch(Exception e) {
            log.error("Could not insert given model to backend dataset due to an unexpected error: "+model, e);
            return false;
        }
    }

    /**
     * Removes <b>all</b> statements from the 
     * backed dataset, resetting the respective 
     * data store.
     *   
     * @return Whether the operation was
     *         successful or not
     */
    public boolean deleteEverything() {
        try {
            this.execUpdate("DROP ALL");
            return true;
        } catch(DaoException e) {
            log.error("Could not delete all statements from backend dataset due to an unexpected error.", e);
            return false;
        }
    }

    private void execUpdate(String updateString) throws DaoException {
        updateString = linearizeRequestString(updateString);
        try {
            long startMillis = System.currentTimeMillis();

            // prepare update on SPARQL endpoint
            UpdateRequest request = UpdateFactory.create(updateString);
            UpdateProcessor uproc = UpdateExecutionFactory.createRemote(request, sparqlEndpointUrl+UPDATE_URL);

            // execute update
            uproc.execute();

            // make statistical and status logging
            offline = false;
            numOfUpdates++;
            lastUpdateExecution = new Date();
            long duration = lastUpdateExecution.getTime() - startMillis;
            log.debug("UPDATE EXECUTION #"+numOfUpdates+" ("+UIUtil.formatMillis(duration)+"): "+updateString);
        } catch(Exception e) {
            throw new DaoException("SPARQL update could not be performed due to error on data access level: "+updateString, e);
        }
    }

    private String linearizeRequestString(String requestString) {
        return requestString.trim().replaceAll("\n+", " ").replaceAll(" {2,}", " ");
    }
}
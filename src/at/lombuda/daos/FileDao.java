package at.lombuda.daos;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.util.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.multipart.MultipartFile;

import at.lombuda.entities.IntegrityConstraint;
import at.lombuda.exceptions.DaoException;
import at.lombuda.services.Context;
import at.lombuda.services.PropertiesService;

/**
 * This Data Access Object provides the means to
 * access and store data file-based for permanent
 * separate storage of data on the server without
 * the use of the backed SPARQL endpoint
 */
public class FileDao {
    private static final Logger log = LogManager.getLogger(FileDao.class);
    private static final String RELATIVE_SQ_PATH = "WEB-INF"+File.separator+"sq";
    private static final String RELATIVE_IC_PATH = RELATIVE_SQ_PATH+File.separator+"ic";
    private static final String SQ_ENCODING = "UTF-8";
    private static final String IC_ENCODING = SQ_ENCODING;

    private boolean isInTestMode;
    private String basePath;
    private String sqPath;

    private File baseDir;
    private final File integrityConstraintDir;

    /**
     * Creates a new File DAO for application-wide
     * file access and storage services.
     * 
     * @param isInTestMode Whether the application
     *        is in test mode or not
     * @throws DaoException In case an error occurs
     *        during initialisation
     */
    protected FileDao(boolean isInTestMode) throws DaoException {
        this.isInTestMode = isInTestMode;

        // try to load configured properties
        this.loadProperties();

        // check base directory for file storage
        baseDir = new File(basePath);

        boolean dirCreated = false;
        if(!baseDir.exists()) {// if it does not exist, try to create it
            dirCreated = baseDir.mkdir();

            if(!dirCreated)
                throw new DaoException("Unsuccessfully tried to create non-existent file directory: "+basePath);
            else
                log.info("Created non-existent file directory: "+basePath);
        } else if(!baseDir.isDirectory()) // check whether it is really a directory
            throw new DaoException("Existing file directory is not reported as such: "+basePath);

        // check contents of the file storage base directory 
        File[] files = baseDir.listFiles();

        String filenames = null;
        if(files.length > 0) {
            filenames = "";
            for(File file:files) {
                filenames += file.getName()+", ";
            }
            filenames = filenames.substring(0, filenames.length()-2);
        }

        if(!dirCreated) {
            if(files.length > 0)
                log.info("File directory '"+basePath+"' found with "+files.length+" file"+(files.length==1?"":"s")+": "+filenames);
            else
                log.info("Empty file directory '"+basePath+"' found.");
        }

        // set directory for integrity constraints
        String integrityConstraintPath = Context.getInstance().getAppPath()+File.separator+RELATIVE_IC_PATH;
        integrityConstraintDir = new File(integrityConstraintPath);

        // check integrity constraint directory
        if(!integrityConstraintDir.exists())
            throw new IllegalStateException("Integrity constraint directory could not be found: "+integrityConstraintPath);
        File[] integrityConstraintFiles = integrityConstraintDir.listFiles();
        if(integrityConstraintFiles.length > 0)
            log.info("File directory '"+integrityConstraintPath+"' found with "+integrityConstraintFiles.length+" files "
                    + "containing (probably) integrity constraints for further use.");
        else
            throw new IllegalStateException("No integrity constraint files could not be found in respective directory: "+integrityConstraintPath);

        // set directory for other SPARQL queries
        sqPath = Context.getInstance().getAppPath()+File.separator+RELATIVE_SQ_PATH;
    }

    private void loadProperties() {
        PropertiesService ps = PropertiesService.getInstance();
        String defaultBasePath = Context.getInstance().getCatalinaBasePath()+File.separator+"lombuda"+File.separator+"files";
        if(!isInTestMode)
            basePath = ps.getProperty("service.file.path", String.class, defaultBasePath);
        else
            basePath = defaultBasePath;
    }

    /**
     * @return A list of all names of those files currently
     *         stored locally on the server. The returned
     *         list is never <code>null</code>. If no files
     *         are found, the list is simply empty.
     */
    public List<String> getAllFilenames() {
        List<String> result = new ArrayList<String>();

        File[] files = baseDir.listFiles();

        for(File file:files) {
            result.add(file.getName());
        }

        return result;
    }

    /**
     * Gets a file with the given name from 
     * the local storage.
     * 
     * @param filename the name of the file
     * @return A file object or null if
     *         it could not be found
     */
    public File getFile(String filename) {
        // find file
        File file = this.createFileObj(filename);

        // check whether this file exists
        if(file == null || !file.exists() || file.isDirectory())
            return null;

        // if so, return it
        return file;
    }

    /**
     * Parses the model within a file from
     * the local storage. 
     * 
     * @param filename the name of the file
     * @return the RDF model from the file 
     *         or null if the file does not
     *         exist
     */
    public Model getFileAsModel(String filename) {
        try {
            // find file and open input stream
            File file = this.createFileObj(filename);
            FileInputStream fis = new FileInputStream(file);

            // create an empty model
            Model model = ModelFactory.createDefaultModel();

            // read model from file
            model.read(fis, null, FileUtils.guessLang(filename));
            fis.close();

            // return model
            return model;
        } catch (FileNotFoundException e) {
            return null;
        } catch (IOException e) {
            log.error("An unexpected error occured during storing the given file: "+filename, e);
            return null;
        }
    }

    /**
     * Stores the given model to a file in the 
     * local storage.
     * 
     * @param model the model that should be 
     *              saved
     * @param filename the name of the file
     * @return Whether the operation was
     *         successful or not
     */
    public boolean store(Model model, String filename) {
        try {
            // create file and open output stream
            File file = this.createFileObj(filename);
            FileOutputStream fos = new FileOutputStream(file);

            // write model to file
            model.write(fos, "Turtle");
            fos.flush();
            fos.close();

            // return result
            return true;
        } catch (IOException e) {
            log.error("An unexpected error occured during storing the given file: "+filename, e);
            return false;
        }
    }

    /**
     * Stores or transfers, respectively, the 
     * given file to the local storage.
     * 
     * @param file the file that should be 
     *             saved
     * @param filename the name of the file
     * @return Whether the operation was
     *         successful or not
     */
    public boolean store(MultipartFile file, String filename) {
        try {
            // create file
            File storageFile = this.createFileObj(filename);

            // transfer file to storage
            file.transferTo(storageFile);

            // return result
            return true;
        } catch (IllegalStateException e) {
            log.error("The given file could not be stored, because it is no longer available for transfer: "+filename, e);
        } catch (IOException e) {
            log.error("An unexpected error occured during storing the given file: "+filename, e);
        }
        return false;
    }

    // TODO delete (all)

    private File createFileObj(String filename) {
        return this.createFileObj(baseDir.toString(), filename);
    }

    private File createFileObj(String baseDir, String filename) {
        return new File(baseDir+File.separator+filename);
    }

    /**
     * Parses all existing files containing integrity constraints and
     * returns them as map with the integrity constraint's label
     * as key (e.g. 'IC-19c').
     * The files are parsed from the application's own integrity 
     * constraint directory.
     * 
     * @return A map containing all existing integrity constraints
     */
    public Map<String, IntegrityConstraint> getIntegrityConstraints() {
        // get all files
        File[] integrityConstraintFiles = integrityConstraintDir.listFiles();

        // generate result map
        HashMap<String, IntegrityConstraint> ics = new HashMap<String, IntegrityConstraint>();

        // try to parse all of the existing integrity constraint files
        for(File icf:integrityConstraintFiles) {
            if(icf.getName().matches("^IC-[0-9]+.*#.*\\.sq$")) {
                try {
                    String[] filename = icf.getName().split("#", 2);
                    String icLabel = filename[0];
                    String icDescription = filename[1].split("\\.", 2)[0];

                    // open input stream and read in file
                    FileInputStream fis = new FileInputStream(icf);
                    String sparqlQuery = IOUtils.toString(fis, IC_ENCODING);

                    // generate internal object holding all data for integrity constraint and add it to result map
                    ics.put(icLabel, new IntegrityConstraint(icLabel, icDescription, sparqlQuery));
                } catch(Exception e) {
                    log.error("An unexpected error occured during parsing of integrity constraint file '"+icf.getName()+"'.", e);
                }
            }
        }

        return ics;
    }

    /**
     * Gets a file with the given label of 
     * an integrity constraint from the 
     * local storage.
     * 
     * @param filename the label of an 
     *                 integrity constraint
     * @return A file object or null if
     *         it could not be found
     */  
    public File getIntegrityConstraint(String icName) {
        // find file
        File file = null;
        File[] integrityConstraintFiles = integrityConstraintDir.listFiles();
        for(File ic:integrityConstraintFiles) {
            if(ic.getName().startsWith(icName)) {
                file = ic;
                break;
            }
        }

        // check whether this file exists
        if(file == null || !file.exists() || file.isDirectory())
            return null;

        // if so, return it
        return file;
    }

    /**
     * Gets the contents of a file with the given name 
     * from the local storage.
     * The file is parsed from the application's own
     * SPARQL query directory.
     * 
     * @param filename the name of the file
     * @return The contents of the file with the given 
     *         name as String or null, if it does not 
     *         exist or any other problem arise
     */
    protected String getQueryString(String filename) {
        // find file
        File file = this.createFileObj(sqPath, filename);

        // check whether this file exists; if not, return null
        if(file == null || !file.exists() || file.isDirectory())
            return null;

        // otherwise, try to open input stream, read in file and return its contents as string
        try {
            FileInputStream fis = new FileInputStream(file);
            String sparqlQuery = IOUtils.toString(fis, SQ_ENCODING);

            return sparqlQuery;
        } catch(Exception e) {
            log.error("An unexpected error occured during parsing of SPARQL query file '"+file.getName()+"'.", e);
            return null;
        }
    }

    /**
     * Gets a file with a general SPARQL 
     * query and the given name from the 
     * local storage.
     * 
     * @param filename the filename of a 
     *                 file with a SPARQL
     *                 query
     * @return A file object or null if
     *         it could not be found
     */
    public File getQueryFile(String filename) {
        // find file
        File file = this.createFileObj(sqPath, filename);

        // check whether this file exists; if not, return null
        if(file == null || !file.exists() || file.isDirectory())
            return null;

        // if so, return it
        return file;
    }
}
package at.lombuda.exceptions;

/**
 * This class represents an exception, which is thrown, 
 * if given property could not be parsed.
 */
public class PropertyMalformedException extends ParsingException {
    private static final long serialVersionUID = -3622848483987983172L;

    /**
     * Constructs a PropertyMalformedException with the given detail message.
     * @param message The detail message of the PropertyMalformedException.
     */
    public PropertyMalformedException(String message) {
        super(message);
    }

    /**
     * Constructs a PropertyMalformedException with the given root cause.
     * @param cause The root cause of the PropertyMalformedException.
     */
    public PropertyMalformedException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructs a PropertyMalformedException with the given detail message and root cause.
     * @param message The detail message of the PropertyMalformedException.
     * @param cause The root cause of the PropertyMalformedException.
     */
    public PropertyMalformedException(String message, Throwable cause) {
        super(message, cause);
    }
}

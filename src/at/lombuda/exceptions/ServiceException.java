package at.lombuda.exceptions;

/**
 * This class represents an exception, which is thrown, 
 * if a general error occurred in a service class.
 */
public class ServiceException extends Exception {
	private static final long serialVersionUID = 4298310189577502568L;

	/**
	 * Constructs a ServiceException with the given detail message.
	 * @param message The detail message of the ServiceException.
	 */
	public ServiceException(String message) {
		super(message);
	}

	/**
	 * Constructs a ServiceException with the given root cause.
	 * @param cause The root cause of the ServiceException.
	 */
	public ServiceException(Throwable cause) {
		super(cause);
	}

	/**
	 * Constructs a ServiceException with the given detail message and root cause.
	 * @param message The detail message of the ServiceException.
	 * @param cause The root cause of the ServiceException.
	 */
	public ServiceException(String message, Throwable cause) {
		super(message, cause);
	}
}

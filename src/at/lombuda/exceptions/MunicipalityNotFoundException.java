package at.lombuda.exceptions;

/**
 * This class represents an exception, which is thrown, 
 * if no information about a given municipality could be
 * found.
 */
public class MunicipalityNotFoundException extends ParsingException {
    private static final long serialVersionUID = 7190322534114001532L;

    /**
     * Constructs a MunicipalityNotFoundException with the given detail message.
     * @param message The detail message of the MunicipalityNotFoundException.
     */
    public MunicipalityNotFoundException(String message) {
        super(message);
    }

    /**
     * Constructs a MunicipalityNotFoundException with the given root cause.
     * @param cause The root cause of the MunicipalityNotFoundException.
     */
    public MunicipalityNotFoundException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructs a MunicipalityNotFoundException with the given detail message and root cause.
     * @param message The detail message of the MunicipalityNotFoundException.
     * @param cause The root cause of the MunicipalityNotFoundException.
     */
    public MunicipalityNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}

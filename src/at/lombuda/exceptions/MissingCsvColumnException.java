package at.lombuda.exceptions;

/**
 * This class represents an exception, which is thrown, 
 * if a column in a CSV file is missing that is parsed.
 */
public class MissingCsvColumnException extends ParsingException {
    private static final long serialVersionUID = -7129839455557623055L;

    /**
     * Constructs a MissingCsvColumnException with the given detail message.
     * @param message The detail message of the MissingCsvColumnException.
     */
    public MissingCsvColumnException(String message) {
        super(message);
    }

    /**
     * Constructs a MissingCsvColumnException with the given root cause.
     * @param cause The root cause of the MissingCsvColumnException.
     */
    public MissingCsvColumnException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructs a MissingCsvColumnException with the given detail message and root cause.
     * @param message The detail message of the MissingCsvColumnException.
     * @param cause The root cause of the MissingCsvColumnException.
     */
    public MissingCsvColumnException(String message, Throwable cause) {
        super(message, cause);
    }
}

package at.lombuda.exceptions;

/**
 * This class represents an exception, which is thrown, 
 * if uploaded data already exists in the backed data set.
 */
public class DataAlreadyExistsException extends Exception {
	private static final long serialVersionUID = -81641888969805565L;

	/**
	 * Constructs a DataAlreadyExistsException with the given detail message.
	 * @param message The detail message of the DataAlreadyExistsException.
	 */
	public DataAlreadyExistsException(String message) {
		super(message);
	}

	/**
	 * Constructs a DataAlreadyExistsException with the given root cause.
	 * @param cause The root cause of the DataAlreadyExistsException.
	 */
	public DataAlreadyExistsException(Throwable cause) {
		super(cause);
	}

	/**
	 * Constructs a DataAlreadyExistsException with the given detail message and root cause.
	 * @param message The detail message of the DataAlreadyExistsException.
	 * @param cause The root cause of the DataAlreadyExistsException.
	 */
	public DataAlreadyExistsException(String message, Throwable cause) {
		super(message, cause);
	}
}
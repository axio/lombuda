package at.lombuda.exceptions;

/**
 * This class represents an exception, which is thrown, 
 * if a mandatory property was not found.
 */
public class PropertyNotFoundException extends Exception {
	private static final long serialVersionUID = -6915137717032854761L;
	
	/**
     * Constructs a PropertyNotFoundException with the given detail message.
     * @param message The detail message of the PropertyNotFoundException.
     */
    public PropertyNotFoundException(String message) {
        super(message);
    }

    /**
     * Constructs a PropertyNotFoundException with the given root cause.
     * @param cause The root cause of the PropertyNotFoundException.
     */
    public PropertyNotFoundException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructs a PropertyNotFoundException with the given detail message and root cause.
     * @param message The detail message of the PropertyNotFoundException.
     * @param cause The root cause of the PropertyNotFoundException.
     */
    public PropertyNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}

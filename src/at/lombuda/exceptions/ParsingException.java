package at.lombuda.exceptions;

/**
 * This class represents an exception, which is thrown, 
 * if some error occurs during a parsing operation in 
 * this application.
 */
public class ParsingException extends Exception {
    private static final long serialVersionUID = -1777773449764567980L;

    /**
     * Constructs a ParsingException with the given detail message.
     * @param message The detail message of the ParsingException.
     */
    public ParsingException(String message) {
        super(message);
    }

    /**
     * Constructs a ParsingException with the given root cause.
     * @param cause The root cause of the ParsingException.
     */
    public ParsingException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructs a ParsingException with the given detail message and root cause.
     * @param message The detail message of the ParsingException.
     * @param cause The root cause of the ParsingException.
     */
    public ParsingException(String message, Throwable cause) {
        super(message, cause);
    }
}

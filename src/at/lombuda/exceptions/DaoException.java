package at.lombuda.exceptions;

/**
 * This class represents an exception, which is thrown, 
 * if a general error occurred in a data access object.
 */
public class DaoException extends Exception {
	private static final long serialVersionUID = -81641888969805565L;

	/**
	 * Constructs a DaoException with the given detail message.
	 * @param message The detail message of the DaoException.
	 */
	public DaoException(String message) {
		super(message);
	}

	/**
	 * Constructs a DaoException with the given root cause.
	 * @param cause The root cause of the DaoException.
	 */
	public DaoException(Throwable cause) {
		super(cause);
	}

	/**
	 * Constructs a DaoException with the given detail message and root cause.
	 * @param message The detail message of the DaoException.
	 * @param cause The root cause of the DaoException.
	 */
	public DaoException(String message, Throwable cause) {
		super(message, cause);
	}
}
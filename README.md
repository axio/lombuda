# What is this?

This repository contains all source files for the project website or software project called [**LOMBuDa.at** - Linked Open Municipal Budget Data for Austria](http://lombuda.at). It is a technical prototype for applying Linked Data to Austrian municipal budgets and was originally developed and published in the year 2017 as part of a Diploma Thesis in fulfilment of the requirements of the Master Programme in Business Informatics at the Faculty of Informatics of the [Vienna University of Technology](https://www.tuwien.ac.at/en/).

More details on this scientific work as well as a link to the work itself can be found on the project website at [**LOMBuDa.at**](http://lombuda.at).

# How can I run this?

This software project was developed with [Eclipse](http://www.eclipse.org/) as IDE in [Java 8](https://www.java.com/de/download/faq/java8.xml) as a web application using [Apache Tomcat v8.0](https://tomcat.apache.org/tomcat-8.0-doc/index.html) as webserver. All [third-party program libraries](http://lombuda.at/about#libraries) as well as a exemplary configuration file for development are included in this repository (see `WEB-INF/classes/example-app.properties`, which need to be filled with some obligatory settings and renamed to `app.properties` in order to work).

[Apache Jena Fuseki](https://jena.apache.org/documentation/fuseki2/) is used as the application's triplestore for Linked Data. This datastore need to be deployed and configured separately, or any arbitrary, remote, but compatible triplestore with a similar full-blown SPARQL endpoint (incl. the possibility for performing SPARQL Update querys) can be configured in the already mentioned configuration file.

Additionally, the project website incorporates a Linked Data interface called "Pubby", making all resources in the [namespace of **LOMBuDa.at**](http://data.lombuda.at/) dereferencable. "Pubby" is a third-party software and was heavily customised for this project website - the currently used source code is also shipped with this repository (see `Pubby.jar` in the root directory). For general information and information on its configuration, please refer to its own [project website](http://wifo5-03.informatik.uni-mannheim.de/pubby/).

# Contact

If you want to contact the developer of this project website and software project as well as the author of the underlying Diploma Thesis, please refer to the contact details on [this page](http://lombuda.at/about).

# Legal information

For information on this project's licensing and other legal information, please refer to [the respective file in this repository](LICENSE)  or [this page](http://lombuda.at/about#licence) for the complete information.

Developed by Paul Blasl. 
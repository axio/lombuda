<%@ tag description="Overall Page Template" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="pageName" required="false" %>
<%@ attribute name="isAdminPage" type="java.lang.Boolean" required="false" %>
<%@ attribute name="activeMenuItemNo" type="java.lang.Integer" required="false" %>
<%@ attribute name="skipGridContainer" type="java.lang.Boolean" required="false" %>
<%@ attribute name="loadYasgui" type="java.lang.Boolean" required="false" %>
<c:choose>
    <c:when test="${isAdminPage}">
        <c:set var="ico" value="/favicon-admin.ico"/>
        <c:set var="logo" value="/img/logo-admin.png"/>
        <c:set var="home" value="/admin/"/>
    </c:when>
    <c:otherwise>
        <c:set var="ico" value="/favicon.ico"/>
        <c:set var="logo" value="/img/logo.png"/>
        <c:set var="home" value="/"/>
    </c:otherwise>
</c:choose>
<!doctype html>
<html class="no-js" lang="en" dir="ltr">
	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="x-ua-compatible" content="ie=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <title><c:if test="${not empty pageName}">${pageName} | </c:if>LOMBuDa.at</title>
	    <link rel="shortcut icon" href="<c:url value="${ico}"/>" type="image/x-icon" />
	    <link rel="stylesheet" href="<c:url value="/css/foundation.css"/>" />
        <c:if test="${loadYasgui}">
        <link rel="stylesheet" href="<c:url value="/css/yasgui.min.css"/>" type='text/css'/>
        </c:if>
	    <link rel="stylesheet" href="<c:url value="/css/app.css"/>" />
	</head>
	<body>
		<div id="content-container">
	
			<!-- NAVIGATION -->
			<nav>
				<div data-sticky-container>
					<div data-sticky data-options="marginTop:0;">
					
                        <c:if test="${empty activeMenuItemNo && pageName == 'Home'}">
                            <div id="funnyStatus" style="position: absolute; top: 3px; left: 3px; 
                                width: 1px; height: 1px; background-color: black; z-index: 10000; display: none;"></div>
                            <div style="position: absolute; width: 1rem; height: 53px; z-index: 10000;" ondblclick="doSomethingFunny()"></div>
                        </c:if>
						<div class="title-bar" data-responsive-toggle="app-menu" data-hide-for="medium" style="display: none;">
							<button class="menu-icon" type="button" data-toggle="app-menu"></button>
							<div class="title-bar-title">Menu</div>
						</div>
	
						<div class="top-bar" id="app-menu">
							<ul class="vertical medium-horizontal dropdown menu menu-hover-lines"
								data-responsive-menu="accordion medium-dropdown">
                                        <li class="menu-text"><a href="<c:url value="${home}"/>"><img alt="LOMBuDa.at" src="<c:url value="${logo}"/>"></a></li>
								<c:choose>
                                    <c:when test="${isAdminPage}">
                                        <li<c:if test="${activeMenuItemNo == 1}"> class="active"</c:if>><a href="<c:url value="/admin/data"/>">Data Operations</a></li>
                                        <li<c:if test="${activeMenuItemNo == 2}"> class="active"</c:if>><a href="<c:url value="/admin/logs"/>">Logs</a></li>
                                    </c:when>
                                    <c:otherwise>
										<li<c:if test="${activeMenuItemNo == 1}"> class="active"</c:if>><a href="<c:url value="/sparql"/>">SPARQL</a></li>
										<li<c:if test="${activeMenuItemNo == 2}"> class="active"</c:if>><a href="<c:url value="http://data.lombuda.at"/>">Data</a></li>
										<li<c:if test="${activeMenuItemNo == 3}"> class="active"</c:if>><a href="<c:url value="/budget-profiles"/>">Budget Profiles</a></li>
										<li<c:if test="${activeMenuItemNo == 4}"> class="active"</c:if>><a href="<c:url value="/about"/>">About</a></li>
								    </c:otherwise>
                                </c:choose>
							</ul>
						</div>
					</div>
				</div>
			</nav>
	
			<!-- CONTENT -->
			<section id="main">
			    <c:choose><c:when test="${skipGridContainer}">
                    <jsp:doBody />
			    </c:when><c:otherwise>
                    <div class="grid-container">
                        <jsp:doBody />
                    </div>
                </c:otherwise></c:choose>
			</section>
	
			<!-- FOOTER -->
			<footer>
				<div class="grid-container full">
					<div class="grid-container">
						<div class="grid-x grid-padding-x">
							<div class="auto cell" style="margin: auto;">
								<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">
								     <img alt="Creative Commons Licence" 
								          style="border-width: 0; vertical-align: bottom; margin-right: 0.4rem;" 
								          src="https://i.creativecommons.org/l/by/4.0/80x15.png" /></a>This work is licensed under the 
								<a rel="license" href="http://creativecommons.org/licenses/by/4.0/" target="_blank">Creative Commons Attribution 4.0 International License</a>.
							</div>
							<div class="cell large-4" style="text-align: right;">
								<a onclick="jumpTo();" id="btt" style="display: none;">&#8639; Back to top &#8638;<br /></a>
								<a href="<c:url value="/about"/>">About</a> // ${currentYear} - <span class="page-title">LOMBuDa.at</span>
							</div>
						</div>
					</div>
				</div>
			</footer>
		</div>
	
		<script src="<c:url value="/js/vendor/jquery.js"/>"></script>
		<script src="<c:url value="/js/vendor/what-input.js"/>"></script>
		<script src="<c:url value="/js/vendor/foundation.js"/>"></script>
		<script src="<c:url value="/js/jquery.timeago.js"/>"></script>
		<script src="<c:url value="/js/app.js"/>"></script>
        <c:if test="${loadYasgui}">
             <script src='<c:url value="/js/yasgui.min.js"/>'></script>
             <script src='<c:url value="/js/app.yasgui.js"/>'></script>
        </c:if>
	</body>
</html>
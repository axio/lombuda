<%@tag description="message row template" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@attribute name="datasetStatistic" required="true" type="at.lombuda.entities.DatasetStatistic"%>
<div class="callout">
    <h5>How much data is stored here?</h5>
    <ul class="stats-list">
        <c:choose><c:when test="${not empty datasetStatistic}">
        <li><fmt:formatNumber value="${datasetStatistic.numOfMunicipalities}" type="number" minFractionDigits="0" maxFractionDigits="0"/> <span class="stats-list-label">municipalities</span></li>
        <li><fmt:formatNumber value="${datasetStatistic.numOfFiscalYears}" type="number" minFractionDigits="0" maxFractionDigits="0"/> <span class="stats-list-label">fiscal years</span></li>
        <li><fmt:formatNumber value="${datasetStatistic.numOfObservations}" type="number" minFractionDigits="0" maxFractionDigits="0"/> <span class="stats-list-label">data records</span></li>
        </c:when><c:otherwise>
        <li>N/A</li>
        </c:otherwise></c:choose>
    </ul>
</div>
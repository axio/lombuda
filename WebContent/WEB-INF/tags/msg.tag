<%@tag description="message row template" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@attribute name="msg" required="true" type="java.lang.String"%>
<%@attribute name="msgStatus" required="true" type="at.lombuda.entities.enums.MessageType"%>
<%@attribute name="hideCloseBtn" required="false" type="java.lang.Boolean"%>

<c:if test="${(not empty msg) && (not empty msgStatus)}">
    <div class="${msgStatus.alertStyleClassName} message callout" data-closable>
        <p>${msg}</p>
        <c:if test="${!hideCloseBtn}">
            <button class="close-button" aria-label="Dismiss message" type="button" data-close><span aria-hidden="true">&times;</span></button>
        </c:if>
    </div>
</c:if>
<%@tag description="inline back to top-link template" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@attribute name="anchorId" required="false" type="java.lang.String"%>

<a class="inline-btt" onclick="jumpTo(<c:if test="${not empty anchorId}">'${anchorId}'</c:if>);">&#8639; Back to top &#8638;</a>
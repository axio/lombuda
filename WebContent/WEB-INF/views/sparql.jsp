<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<t:template pageName="SPARQL Endpoint" activeMenuItemNo="1" loadYasgui="true">
    <jsp:body>
              <div class="grid-x grid-padding-x">
                <div class="large-12 cell">
                    <nav aria-label="You are here:" role="navigation">
                        <ul class="breadcrumbs">
                            <li><a href="<c:url value="/"/>">Home</a></li>
                            <li>
                                <span class="show-for-sr">Current: </span> SPARQL
                            </li>
                        </ul>
                    </nav>
                </div>
              </div>

              <div class="grid-x grid-padding-x">
                <div class="large-12 cell">
                  <h1>SPARQL Endpoint</h1>
                </div>
              </div>

              <div class="grid-x grid-padding-x">
                <div class="large-8 medium-8 cell">
                    <h5 class="subheader">A central point for structured data retrieval</h5>
                    <p style="text-align: justify">One common way to retrieve and query Linked Data, which is structured as <span data-tooltip aria-haspopup="true" 
                    class="has-tip bottom" data-disable-hover="false" tabindex="1" title="Resource Description Framework">RDF</span> graph, is SPARQL. Most applications
                    serving Linked Data offer a so-called “SPARQL endpoint” as starting point for querying and retrieving results from underlying datasets as well as
                    in general for automated data retrievals.</p>
                    <p style="text-align: justify">This application's SPARQL endpoint resides right at the URL of this very page. For reasons of convenience
                    and usability, this page is shown by the application's web server (instead of querying the underlying dataset) in order to present a human-friendly
                    and -usable interface. Therefore, below also a Query Editor is available to create and transmit SPARQL queries directly on this endpoint including
                    an appropriate display of respective result sets including the possibility to download them as <span data-tooltip aria-haspopup="true" 
                    class="has-tip bottom" data-disable-hover="false" tabindex="4" title="comma-separated values">CSV</span> files.</p>
                    <p style="text-align: justify">By the way, this Query Editor as well as its result parser – some really outstanding pieces of technology –
                    is one of the reused third-party libraries by this application: the <i>Yet Another Sparql GUI</i> (YASGUI). More information on that is available on
                    <a href="<c:url value="/about#libraries"/>">another page</a> of this website.</p>

                    <hr style="margin-bottom: 0.75rem; margin-top: 1rem;" />

                    <h4>Editor for direct Endpoint Queries</h4>
                    <div id="yasgui-container" style="background-image: url('<c:url value="/img/hourclass.png"/>');"></div>
                </div>

                <div class="large-4 medium-4 cell">
                  <div class="callout">
                    <h5>What is SPARQL?</h5>
                    <p style="text-align: justify;">The recursive acronym SPARQL (pronounced like “sparkle”) stands for <i>SPARQL Protocol and RDF Query Language</i>.
                    It become prevalent for querying sets of <span data-tooltip aria-haspopup="true" class="has-tip bottom" data-disable-hover="false" tabindex="10" 
                    title="Resource Description Framework">RDF</span> statements nowadays and, although, SPARQL resembles certain characteristics of querying languages used
                    for relational databases (like SQL), it is designed to operate on data that is organised in a directed, labelled graph – more specifically,
                    RDF’s data model – as sets of “subject-predicate-object” triples. So in fact, SPARQL has more in common with querying languages for “document-key-value”
                    stores (like NoSQL) than with others.</p>
                    <a href="https://en.wikipedia.org/wiki/SPARQL" target="_blank" class="small button">Learn more about SPARQL on Wikipedia</a>
                  </div>
                  <div class="callout">
                    <h5>How can I use this endpoint?</h5>
                    <p style="text-align: justify;">In order to perform actual queries on this endpoint – for example with your own application, you need to pass your
                    (<span data-tooltip aria-haspopup="true" class="has-tip bottom" data-disable-hover="false" tabindex="11" title="percent-encoding, e.g. '%3C' for '<'">URL-encoded</span>)
                    SPARQL request as an URL query parameter <code>query</code> in an HTTP <code>GET</code> or <code>POST</code> request to the <span data-tooltip 
                    aria-haspopup="true" class="has-tip bottom" data-disable-hover="false" tabindex="12" title="http://lombuda.at/sparql?query={SPARQL QUERY}">URL of
                    this very page</span>. For large queries, you can pass your (uncoded) SPARQL query via an HTTP <code>POST</code> request body, while declaring the
                    HTTP <code>Content-Type</code> header as <code>application/sparql-query</code>. You can control the output format by using the HTTP <code>Accept</code> header
                    field (e.g. <code>text/ttl</code> will result in a response in <a href="https://en.wikipedia.org/wiki/Turtle_(syntax)" target="_blank">Turtle</a> syntax).</p>
                    <a href="<c:url value="/sparql?query=DESCRIBE+%3Chttp%3A%2F%2Fdata.lombuda.at%2FOGHD%3E"/>" target="_blank" class="small button">Make an example query</a>
                  </div>
                </div>
              </div>
    </jsp:body>
</t:template>
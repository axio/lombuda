<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<t:template pageName="Budget Profiles" activeMenuItemNo="3">
    <jsp:body>
              <div class="grid-x grid-padding-x">
                <div class="large-12 cell">
                    <nav aria-label="You are here:" role="navigation">
                        <ul class="breadcrumbs">
                            <li><a href="<c:url value="/"/>">Home</a></li>
                            <li>
                                <span class="show-for-sr">Current: </span> Budget Profiles
                            </li>
                        </ul>
                    </nav>
                </div>
              </div>
              
              <div class="grid-x grid-padding-x">
                <div class="large-12 cell">
                  <h1>Budget Profiles of Municipalities</h1>
                </div>
              </div>

              <script>
	              var slices = ${municipalitiesJs};
	              function selectedMunicipality() {
	            	  var mSelect = document.getElementById("municipalities");
	            	  var selectedMunicipality = mSelect.options[mSelect.selectedIndex].value;

	            	  var fyGrp = document.getElementById("fiscalYearsGrp");
                      var fySel = document.getElementById("fiscalYears");
                      resetSelect("fiscalYears", " -- select a year -- ");
                      var fiscalYears = slices[selectedMunicipality];
                      for(var i = 0; i < fiscalYears.length; ++i) {
                    	    var fyStr = fiscalYears[i];
                    	    var fyOption = document.createElement("option");
                    	    fyOption.text = fyStr;
                    	    fyOption.value = fyStr;
                            fySel.add(fyOption);
                      }

                      fyGrp.classList.remove('hide');
	              }
	              
	              function resetSelect(selectId, text) {
	            	  var select = document.getElementById(selectId);
	            	  select.options.length = 0;

                      var awaiting = document.createElement("option");
                      awaiting.text = text;
                      awaiting.value = text;
                      awaiting.selected = true;
                      awaiting.disabled = true;
                      awaiting.classList.add("hide");
                      select.add(awaiting);
	              }
	              
                  function submit(formId) {
                      document.getElementById(formId).submit();
                  }
                  
                  function select(select, value) {
                      var length = select.options.length;
                      for (i = 0; i < length; i++) {
                    	  var opt = select.options[i];
                    	  if(opt.value == value)
                    		  opt.selected = true;
                    	  else 
                              opt.selected = false;
                      }
                  }
                  
                  function toogleFullBudgetProfile() {
                	  var rows = document.getElementsByClassName('extendable-row');
                      var length = rows.length;
                      for (i = 0; i < length; i++) {
                    	  rows[i].classList.toggle('hide');
                      }
                      showBtt();
                  }
              </script>

              <div class="grid-x grid-padding-x">
                <div class="large-12 medium-12 cell">
                  <h5 class="subheader">An overview about municipal finances according to the VRV</h5>

                  <p style="text-align: justify">Below one can select a municipality and a respective fiscal year available in the dataset of
                  <span class="page-title">LOMBuDa.at</span> for generation of a so-called “<span data-tooltip aria-haspopup="true" class="has-tip bottom" 
                  data-disable-hover="false" tabindex="10" title="in German 'Rechnungsquerschnitt'">budget profile</span>”. The applicable legislation for Austrian
                  municipalities (to be precise, the <span data-tooltip aria-haspopup="true" class="has-tip bottom" data-disable-hover="false" tabindex="11" 
                  title="German abbreviation for 'Voranschlags- und Rechnungsabschlussverordnung'">VRV</span>) requires them to publish this very table in each of the annually
                  created <span data-tooltip aria-haspopup="true" class="has-tip bottom" data-disable-hover="false" tabindex="12" title="in German 'Voränschlage'">budgets</span>
                  and <span data-tooltip aria-haspopup="true" class="has-tip bottom" data-disable-hover="false" tabindex="13" title="in German 'Rechnungsabschlüsse'">statements
                  of account</span>. It should give a short overview about all the figures and numbers in these documents.
                  Please note, that the budget profiles on this very page always resemble respective data in municipal statements of account <i>only</i>.</p>

                  <hr/>

                </div>
                <div class="large-12 medium-12 cell">
                <form id="budget-profile-select-form" method="POST" action="<c:url value="/budget-profiles"/>" accept-charset="UTF-8">
                <div class="grid-x grid-padding-x">
                <div class="large-2 medium-2 cell"></div>
                <div class="large-3 medium-3 cell">
                  <label class="inline"><strong>Municipality:</strong>
                  <select id="municipalities" name="municipality" onchange="selectedMunicipality();" style="width: 11rem;">
                        <option class="hide" disabled selected> -- select a municipality -- </option>
                    <c:forEach items="${municipalities}" var="municipality">
                        <option value="${municipality}">${municipality}</option>
                    </c:forEach>
                  </select></label>
                </div>
                <div class="large-3 medium-3 cell">
                  <label id="fiscalYearsGrp" class="inline hide"><strong>Fiscal Year:</strong>
                    <select id="fiscalYears" name="fiscalYear" onchange="submit('budget-profile-select-form');" style="width: 8rem;"></select></label>
                </div>
                <c:if test="${not empty budgetProfileList}">
                <div class="large-3 medium-3 cell">
                    <input id="showFullBudgetProfile" type="checkbox" onchange="toogleFullBudgetProfile();">
                    <label for="showFullBudgetProfile" style="margin-left: 0.15rem;">Show full budget profile</label>
                </div>
                </c:if>
                <div class="large-2 medium-2 cell"></div>
                </div>
                </form>
                </div>
                <div class="large-12 medium-12 cell">
                
                  <c:if test="${not empty municipality && not empty fiscalYear && empty budgetProfileList}">
                    <div class="success alert callout" style="text-align: center;"><p>An unexpected internal error occurred and prevented the application from getting the selected budget profile. Please try again.</p></div>
                  </c:if>
                  <c:if test="${not empty budgetProfileList}">
	                <table class="small hover budget-profile">
	                  <thead>
	                    <tr>
	                      <th class="no-wrap center" style="width: 3.3em;">&#8470;</th>
	                      <th>Description</th>
	                      <th class="center" style="width: 11em;">Sum<br/>OB + EB<sup>1)</sup></th>
	                      <th class="center" style="width: 11em;">thereof<br/>A85-89<sup>2)</sup></th>
                          <th class="center" style="width: 11em;">Sum w/o<br/>A85-89</th>
	                    </tr>
	                  </thead>
	                  <tbody>
	                   <fmt:setLocale value = "de_AT"/>
                       <c:set var="colspan" value="4" scope="request" />
			           <c:forEach items="${budgetProfileList}" var="agg">
			            <c:if test="${agg.index=='II'}"><c:set var="endSection" value="end" scope="request" /><c:set var="colspan" value="2" scope="request" /></c:if>
				        <tr class="bp-${fn:toLowerCase(agg.type)}<c:if test="${agg.type == 'LINE' || agg.type == 'H2'}"> extendable-row hide</c:if>">
				            <c:choose>
					            <c:when test="${agg.type == 'H1'}"><td class="no-wrap center">${agg.index}.</td></c:when>
					            <c:otherwise><td class="no-wrap center">${agg.index}</td></c:otherwise>
				            </c:choose>
				            <c:choose>
                                <c:when test="${agg.type == 'H1' || agg.type == 'H2'}"><td colspan="${colspan}">${agg.label}</td></c:when>
                                <c:when test="${fn:contains(agg.label, ':')}"><c:set var="label" value="${fn:split(agg.label, ':')}" />
                                    <td><span class="balance-id">${label[0]}:</span>${label[1]}<c:if test="${agg.index=='94'}"><sup>3)</sup></c:if></td>
                                </c:when>
                                <c:otherwise><td>${agg.label}<c:choose>
                                <c:when test="${agg.index=='70'}"><sup>4)</sup></c:when>
                                <c:when test="${agg.index=='71'}"><sup>5)</sup></c:when>
                                <c:when test="${agg.index=='99'}"><sup>6)</sup></c:when>
                                </c:choose></td></c:otherwise>
                            </c:choose>
                            <c:if test="${agg.type != 'H1' && agg.type != 'H2'}">
	                            <td class="no-wrap right<c:if test="${agg.type == 'LINE' && agg.amount==0}"> null</c:if>">
	                                <fmt:formatNumber value="${agg.amount}" pattern="#,##0.00"/>&nbsp;EUR
	                            </td>
	                            <c:if test="${empty endSection}">
		                            <td class="no-wrap right<c:if test="${agg.type == 'LINE' && agg.amountForPublicCorporations==0}"> null</c:if>">
		                                <fmt:formatNumber value="${agg.amountForPublicCorporations}" pattern="#,##0.00"/>&nbsp;EUR
		                            </td>
		                            <td class="no-wrap right<c:if test="${agg.type == 'LINE' && agg.amountWithoutPublicCorporations==0}"> null</c:if>">
		                                <fmt:formatNumber value="${agg.amountWithoutPublicCorporations}" pattern="#,##0.00"/>&nbsp;EUR
		                            </td>
                                </c:if>
                            </c:if>
				        </tr>
			           </c:forEach>
			          </tbody>
                    </table>
                    <ol class="footnotes budget-profile">
                      <li>Every row equals the total sum of all cash flows in a municipal budget - not separating the <span data-tooltip aria-haspopup="true"
                          class="has-tip bottom" data-disable-hover="false" tabindex="50" title="in German 'ordentlicher Haushalt (o.H.)'">ordinary budget (OB)</span>
                          from the <span data-tooltip aria-haspopup="true" class="has-tip bottom" data-disable-hover="false" tabindex="51"
                          title="in German 'außerordentlicher Haushalt (ao.H.)'">extraordinary budget (EB)</span> like in the rest of <span class="page-title">
                          LOMBuDa.at</span>'s dataset.</li>
                      <li>These are the sums of all cash flows classified as such ones for <span data-tooltip aria-haspopup="true" class="has-tip bottom"
                          data-disable-hover="false" tabindex="52"  title="functional classification sections - in German 'Ansatzabschnitte'">approach sections 85 to 89</span>
                          associated with public corporations.</li>
                      <li>This balance aggregates balances #1, #2 and #3 (&#8470;91-93). Please note, it does <i>not</i> incorporate annual transfers and transfers between
                          <span data-tooltip aria-haspopup="true" class="has-tip bottom" data-disable-hover="false" tabindex="53"
                          title="ordinary budget - in German 'ordentlicher Haushalt'">OB</span> &amp; <span data-tooltip aria-haspopup="true" class="has-tip bottom"
                          data-disable-hover="false" tabindex="54" title="extraordinary budget - in German 'außerordentlicher Haushalt'">EB</span> like the administrative
                          annual result (&#8470;99).</li>
                      <li>This line of the budget profile is an aggregation of balances #1 and #2 (&#8470;92&amp;91) of the third column of figure (sums w/o <span
                          data-tooltip aria-haspopup="true" class="has-tip bottom" data-disable-hover="false" tabindex="55" 
                          title="functional classifications - in German 'Ansätze' - associated with public corporations">A85-89</span>).</li>
                      <li>This line of the budget profile equals the balance #4 (&#8470;94) for the second column of figures (sums for <span
                          data-tooltip aria-haspopup="true" class="has-tip bottom" data-disable-hover="false" tabindex="56" 
                          title="functional classifications - in German 'Ansätze' - associated with public corporations">A85-89</span>).</li>
                      <li>The administrative annual result has to be always equal zero. This resembles the fact that the total budget has to be balanced at the end of each
                          fiscal period; no residual values must remain.</li>
                    </ol>
                  </c:if>
                  
                  <hr style="margin-top: 0.25rem;" />
                  
                </div>
                <div class="large-4 medium-4 cell">
                    <div class="callout">
                        <h5>What is this all about?</h5>
                        <p style="text-align: justify">The <span data-tooltip aria-haspopup="true" class="has-tip bottom" data-disable-hover="false" tabindex="20" 
                        title="in German 'Rechnungsquerschnitte'">budget profiles</span>, that can be generated on this very page, provide an easy, convenient mean
                        to verify the dataset of <span class="page-title">LOMBuDa.at</span> (as well as the underlying data model). The original and official profiles
                        published by municipalities themselves are often released online as PDF files (or in a similar way) by now and can be easily compared with the
                        number and figures on this page. For easier orientation for every aggregation or line, respectively, regulated index numbers are shown leftmost.
                        These indices are standardised by the <span data-tooltip aria-haspopup="true" class="has-tip bottom" data-disable-hover="false" tabindex="21" 
                        title="German abbreviation for 'Voranschlags- und Rechnungsabschlussverordnung'">VRV</span> directly. So, the index numbers in all of the official
                        documents and their respective data can be easily matched with those on this page.<br/>
                        Overall, this possibility, to generate one of the more important (if not the most important) part of municipal financial documents according
                        to the VRV, should prove the integrity of the provided data model behind <span class="page-title">LOMBuDa.at</span> in a technical and semantic way.</p>
                        <a href="<c:url value="/#municipal-budgets"/>" class="small button">Learn more about the VRV</a>
                    </div>
                </div>
                <div class="large-4 medium-4 cell">
                    <div class="callout">
                        <h5>Where does this data come from?</h5>
                        <p style="text-align: justify">The data originates from machine-readable documents (mostly <span data-tooltip aria-haspopup="true" class="has-tip bottom"
                        data-disable-hover="false" tabindex="30" title="files with comma-separated values in tabular format">CSV files</span>) published by other data sources 
                        on behalf of municipalities or by public bodies and their online platforms themselves. This data was transformed as part of this work into <a 
                        href="<c:url value="/#linked-data"/>">Linked Data</a> and is available via a <a href="<c:url value="/sparql"/>">SPARQL</a> endpoint as well as <a
                        href="<c:url value="http://data.lombuda.at/OGHD/2015/60101/RA/EXP/000/070"/>">dereferenceable data</a> on <span class="page-title">LOMBuDa.at</span>.<br/>
                        The original source for every data record is declared in each of the respective <a 
                        href="<c:url value="http://data.lombuda.at/ontology/slice/fiscalDoc"/>">data slices</a> including the applicable licence. By the way, only data
                        sources with respective licenses are used, which are compatible with the <a href="<c:url value="/about#licence"/>">license</a> of
                        <span class="page-title">LOMBuDa.at</span>.<br/>
                        For the sake of simplicity, the data itself or the budget profiles generated on this page, respectively, always represents those ones from municipal
                        <span data-tooltip aria-haspopup="true" class="has-tip bottom" data-disable-hover="false" tabindex="31"
                        title="in German 'Rechnungsabschlüsse'">statements of account</span> only, although, also <span data-tooltip aria-haspopup="true" class="has-tip bottom"
                        data-disable-hover="false" tabindex="32" title="in German 'Voränschlage'">budgets</span> are modelled and therefore, could be easily described in the
                        future.</p>
                        <c:choose>
                            <c:when test="${not empty dataSliceUrl}"><a href="<c:url value="${dataSliceUrl}"/>" class="small button">Show me this data set in its raw format</a></c:when>
                            <c:otherwise><a href="<c:url value="http://data.lombuda.at/OGHD/2015/60101/RA"/>" class="small button">Show me some raw data set</a></c:otherwise>
                        </c:choose>
                    </div>
                </div>
                <div class="large-4 medium-4 cell">
                    <div class="callout">
                        <h5>How does it come to this?</h5>
                        <p style="text-align: justify">All aggregations generated after selecting a municipality and a respective fiscal year available in the dataset of
                        <span class="page-title">LOMBuDa.at</span> are made in one go via a predefined <span data-tooltip aria-haspopup="true" class="has-tip bottom"
                        data-disable-hover="false" tabindex="40" title="a query written in the SPARQL Protocol and RDF Query Language">SPARQL query</span>. In the first
                        step, this query is sent to the project website's own <a href="<c:url value="/sparql"/>">SPARQL endpoint</a>. To be more precise, this query
                        provides the data for all rows, which aggregate the various <span data-tooltip aria-haspopup="true" class="has-tip bottom" data-disable-hover="false" 
                        tabindex="41" title="3-digit groups of economic classification - in German 'Posten'">account groups</span> of the respective budget for the given
                        municipality and fiscal year. Note that, these rows are only visible, if and only if “show full budget profile” is selected. All others rows, which
                        provide sums and balances, are calculated programmatically before presenting the respective budget profile on this very page.<br/>
                        The separate data for <span data-tooltip aria-haspopup="true" class="has-tip bottom" data-disable-hover="false" tabindex="42" 
                        title="functional classifications - in German 'Ansätze' - associated with public corporations">approach sections 85 to 89</span> are aggregated
                        by a similar query and subsequent procedures resulting in the second column of figures. Eventually, the last one column is simply the difference
                        between the first and the second column of figures.</p>
                        <a href="<c:url value="/budget-profile/downloadQuery"/>" class="small button">Show me the overall aggregating query</a>
                    </div>
                </div>
              </div>
              <c:if test="${not empty municipality && not empty fiscalYear}">
              <script>
                 var m = ${municipality};
                 var fy = ${fiscalYear};
                 select(document.getElementById("municipalities"),m);
                 selectedMunicipality();
                 select(document.getElementById("fiscalYears"),fy);
              </script>
              </c:if>

    </jsp:body>
</t:template>
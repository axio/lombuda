<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<t:template pageName="Home" skipGridContainer="true">
    <jsp:body>
        <div class="marketing-site-hero">
                <div class="marketing-site-hero-content">
                    <div class="marketing-site-hero-text">
                        <h1>Linked Open Municipal Budget Data for Austria</h1>
                        <p style="text-align: justify">Hi there!</p>
                        <p style="text-align: justify">You maybe wonder, what's this all about? In a nutshell, this project web site provides a comprehensible RDF vocabulary
                        for budget data from Austrian municipalities and exemplary Linked Data based on it.<br/>
                        <i>RDF? Linked Data? Municipal budgets?</i><br/>
                        Hm... You got confused. I understand.</p>
                        <a onclick="jumpTo('headline-anchor');" class="round hollow button">learn more</a>
                    </div>
                </div>
            </div>
            <div class="grid-container">
              <div class="grid-x grid-padding-x">
                <div class="large-12 cell" style="margin-bottom: 0.5rem;">
                  <div id="headline-anchor" style="position: absolute; top: 99vh;"></div>
                  <h1>Linked Open Municipal Budget Data for Austria</h1>
                </div>
              </div>

              <div class="grid-x grid-padding-x">
                <div class="large-8 medium-8 cell">
                  <h4>Welcome!</h4>
                  <p style="text-align: justify">On this as well as all other pages of this project’s website, you will find information about a vocabulary and
                  respective data for <a onclick="jumpTo('municipal-budgets');">local government finances</a> in Austria, based on so-called
                  “<a onclick="jumpTo('linked-data');">Linked</a> <a onclick="jumpTo('open-data');">Open</a> Data”. The used approach and its technologies combine
                  state-of-the-art and current best practices in this field of Information Science. This website and its application was conceptualised, developed and
                  published originally in the year 2017 as part of a Diploma Thesis in Business Informatics at the <a href="http://www.informatik.tuwien.ac.at/english"
                  target="_blank">Faculty of Informatics</a> of the <a href="https://www.tuwien.ac.at/en" target="_blank">Vienna University of Technology</a>. You can
                  learn more about this work as well as the overall, pioneering concept of so-called “Linked Open Budget Data” on this very website. Read and study the
                  given <a href="http://data.lombuda.at">data</a> to get a glimpse into the World Wide Web’s future, building not only a <i>Web of Documents</i>, but
                  also a real <i>Web of Data</i>...</p>
                  
                  <hr />
                  
                  <h4>Technical overview</h4>
                  <p style="text-align: justify">Not until the recent technical developments and progresses with respect to information technologies, the possibilities
                  but also the needs to gain budgetary transparency of governmental finances are greater than ever. The goal to publish information about budgets of
                  governmental authorities in a reasonable, comprehensive, transparent and open way induces challenges not only with respect to the data itself but also
                  to the embedded budgetary processes and systems.</p>
                  <div style="text-align: center;margin-bottom: 0.8rem;">
                    <img src="img/chevron.png" alt="Combination of thematic concepts by LOMBuDa.at" usemap="#chevron-map"/>
                  </div>
                  <p style="text-align: justify">The approach presented and implemented on this project’s website and its application expands state-of-the-art approaches and
                  international best practices regarding <a onclick="jumpTo('open-data');">Open</a> and <a onclick="jumpTo('linked-data');">Linked</a> Data towards
                  <a onclick="jumpTo('municipal-budgets');">local government finances</a> in Austria for generating a new level of best practise and transparency compared to
                  the status quo in this part of the world.<br/>
                  In the following, the overall concepts of these three elements used for this project shall be discussed in a nutshell. For more information, please have a
                  look into the underlying <a data-toggle="thesisModal">Diploma Thesis</a>, which is the primary origin of this website and the following texts after all.</p>

				  <map name="chevron-map">
					  <area shape="poly" coords="71,1,159,1,159,78,115,103,71,78" onclick="jumpTo('municipal-budgets');" alt="Local Government Finances">
					  <area shape="poly" coords="68,84,112,110,112,160,45,198,2,123" onclick="jumpTo('linked-data');" alt="Linked Data">
					  <area shape="poly" coords="162,84,118,110,118,160,184,198,228,123" onclick="jumpTo('open-data');" alt="Open Data">
				  </map>

                  <t:inlineBtt anchorId="headline-anchor"/>
                  <h6>Local Government Finances</h6><div id="municipal-budgets" style="position: relative; top: -5.5rem;"></div>
                  <p style="text-align: justify">This project’s domain is about local government finances or – to be more precise – the budgetary data of Austrian
                  municipalities. Public finances are – of course – a complex and wide-spread theme. However, in its core (relevant for this project) it is about all
                  revenues and expenditures made by public bodies or, in case of this project, Austrian municipalities. The relevant legislation for this topic is the
                  so-called “Voranschlags- und Rechnungsabschlussverordnung” (VRV) or Budgeting and Accounts regulation. There exist two relevant versions of this regulation
                  nowadays: the <a href="https://www.ris.bka.gv.at/GeltendeFassung.wxe?Abfrage=Bundesnormen&Gesetzesnummer=10005022" target="_blank">VRV 1997</a>,
                  still applicable to today’s budgetary documents of Austrian municipalities, and the 
                  <a href="https://www.ris.bka.gv.at/GeltendeFassung.wxe?Abfrage=Bundesnormen&Gesetzesnummer=20009319" target="_blank">VRV 2015</a>, which will become
                  mandatory by no later than 2010. Both regulations require all Austrian municipalities to publish two budgetary documents, part of the overall
                  so-called “budget cycle” depicted below and relevant for this project: <i>budgets</i> and <i>statements of account</i>.</p>
                  <div style="text-align: center;margin-bottom: 0.3rem;">
                    <img src="img/budget-cycle.png" alt="The general budget cycle in the public administration"/>
                  </div>
                  <p style="text-align: justify">A <i>budget</i> is a report about a financial plan, where scheduled projects are depicted with all of their financial
                  consequences. The budget is usually made in the first step of the budget cycle and prior to the applying fiscal period. In contrast, a
                  <i>statement of account</i> – under the terms of the VRV – is a report about the realisation of a financial plan, whether and to what extent a
                  municipality has performed its duties in accordance with the preliminary budget.</p>
                  <p style="text-align: justify">In order to be able to compare municipal budgets in the first place, all local government finances need to be
                  classified according to the VRV. This is done in a <span data-tooltip aria-haspopup="true" class="has-tip bottom" data-disable-hover="false"
                  tabindex="10" title="so-called 'approaches' - in German 'Ansätze'">functional</span> and an <span data-tooltip aria-haspopup="true"
                  class="has-tip bottom" data-disable-hover="false" tabindex="11" title="so-called 'accounts' - in German 'Posten'">economic</span> as well as a
                  <span data-tooltip aria-haspopup="true" class="has-tip bottom" data-disable-hover="false" tabindex="12"
                  title="so-called 'budgetary indicators' - in German 'Haushaltskennzeichen'">fiscal</span> way – resulting in a (at least) 7 digit long code
                  associated with each cash flow in a municipal budget:</p>
                  <div style="text-align: center;margin-bottom: 0.8rem;">
                    <img src="img/budgetary-classifications.png" alt="Classification of revenues and expenditures in municipal budgets according to VRV"/>
                  </div>
                  <p style="text-align: justify">These codes as well as respective data associated with them are implemented by this project as Linked Data:
                  the respective definition of these code lists are defined as <a onclick="jumpTo('linked-data');">Linked Data</a>
                  <a href="http://data.lombuda.at/ontology/code/VRV/budgetary-indicators">here</a> for the fiscal classification,
                  <a href="http://data.lombuda.at/ontology/code/VRV/approaches">here</a> for the functional one
                  and, last but not least, <a href="http://data.lombuda.at/ontology/code/VRV/accounts">here</a> for the economic one.</p>
                  <p style="text-align: justify">In the end, the VRV also regulates a segment of all budgets and statements of account necessary for gaining
                  a general overview about a municipal budget: the so-called “Rechnungsquerschnitt” or budget profile. This important part of local government
                  finances’ statistics was also implemented based on <a onclick="jumpTo('linked-data');">Linked Data</a> in the course of this project and various
                  such budget profiles can be queried on <a href="<c:url value="/budget-profiles"/>">another page</a> of this website.</p>

                  <t:inlineBtt anchorId="headline-anchor"/>
                  <h6>Linked Data</h6><div id="linked-data" style="position: relative; top: -5.5rem;"></div>
                  <p style="text-align: justify">As complex as this project’s domain with <a onclick="jumpTo('municipal-budgets');">local government finances</a>
                  can get as diverse its underlying technical concept with so-called “Linked Data” may prove. If you want to understand this innovative technologic
                  principle used by this project, this section of this website shall be considered carefully.</p>
                  <p style="text-align: justify">In general, down to the present day most of the data distributed via the <a href="https://en.wikipedia.org/wiki/World_Wide_Web"
                  target="_blank">World Wide Web</a> is “just” a <i>Web of Documents</i> or, to be more precise, <a href="https://en.wikipedia.org/wiki/Hypertext"
                  target="_blank">Hypertext</a>. However, as such hypertexts concentrate on human-readable, eponymous <i>texts</i> instead of actual data, this data
                  is still often published in <i>Documents on the Web</i>  (instead of in a <i>Web of Data</i>) – like Austrian budgetary data in PDF files or
                  Microsoft Excel spreadsheets. Linked Data should provide such a <i>Web of Data</i> in addition to the current <i>Web of Documents</i>. The actual aim
                  of Linked Data is to overcome issues by former data storage &amp; distribution techniques: Today, in most cases, data is associated and, thereby,
                  incorporated by specific applications. Metadata and data schema information are incorporated as such by those applications, instead of being separated
                  from the actual application logic. A database or an export of its data is built specifically for certain applications, usually. For example, column
                  headings in the ubiquitous <span data-tooltip aria-haspopup="true" class="has-tip bottom" data-disable-hover="false" tabindex="21"
                  title="Comma-Separated-Value">CSV</span> format are not really immediately useful, as the actual meaning of those textual designators is ambiguous and
                  can differ from document to document. Furthermore, while looking at such a CSV file, humans <i>may</i> be able to derive some value from those column
                  headings; a program, which is not explicitly built for this respective data schema, is <i>not</i> necessarily able to do so. That’s why in the end,
                  <i>Documents on the Web</i>  including their incorporated data are <i>not</i> (easily) reusable or even machine readable for third-party programs.
                  In contrast to that, Linked Data – composing a <i>Web of Data</i> – <i>is</i>. Therefore, Linked Data becomes increasingly important when it comes to
                  distributing data in an open way.</p>
                  <p style="text-align: justify">To understand Linked Data in its entirety, it is necessary to understand the related term of the “Semantic Web” as well.
                  The Semantic Web can be understood as the effort to give the information (and the data) on the World Wide Web self-defined semantics and meaning.
                  The idea is explicitly <i>not</i> to replace all current websites (and/or the data on them), but to enrich or extend them in a way, so that
                  (especially automated) information retrieval is simplified.</p>
                  <p style="text-align: justify">The Semantic Web itself is made up by various technological standards also providing the foundation of its sibling
                  “Linked Data”. These standards are commonly associated to several conceptual layers – each layer basing upon its underlying counterpart. This layer
                  model illustrating the conceptual architecture of the Semantic Web is nowadays known as “Semantic Web Layer Cake” and is depicted below including exemplary
                  technologies for the different layers, which – for the sake of simplicity – match those used for Linked Data in this very project.</p>
                  <div style="text-align: center;margin-bottom: 0.8rem;">
                    <img src="img/semantic-web-cake.png" alt="The Semantic Web Layer Cake"/>
                  </div>
                  <p style="text-align: justify">Linked Data is composed mainly by the lower and intermediate layers of this “Semantic Web Layer Cake”. Although no deeper
                  explanation is given here on the respective technologies (feel free to search relevant literature on the web), one crucial part should be discussed
                  in a nutshell nevertheless: the “Resource Description Framework” (RDF).</p>
                  <p style="text-align: justify">RDF is a datamodel for objects (so-called “resources”) and relations between them and provides simple semantics for this
                  datamodel. The idea behind RDF is to make “statements” about resources. Each such RDF statement consists of a <i>subject</i>, a <i>predicate</i> and an
                  <i>object</i>: the <i>subject</i> names the resource being described, the <i>predicate</i> what property of the resource is going to be described and
                  last but not least, the <i>object</i> holds the value of this property itself. This combination is known as RDF “triple”. For example, the sentence
                  “the sky is blue” can be expressed as an RDF statement or triple – with “the sky” as subject, “is” as predicate and “blue” as the object. It is important
                  to note, that RDF names especially subjects and predicates not arbitrarily and freely, but uses <span data-tooltip aria-haspopup="true" class="has-tip bottom"
                  data-disable-hover="false" tabindex="22" title="Uniform Resource Identifier">URI</span>s instead for referencing things in a globally unique and consistent
                  way. Such things can be resources or real-world objects, respectively, like a place or a book, but also abstract concepts, like relationships between those
                  resources. URIs are used to <i>reference</i> those actually meant things in an abstract, but also machine-readable way. Additionally, an object can be a
                  literal (like “blue” in “the sky is blue”), but also a URI itself, referencing other resources (like “Peter” in “Paul knows Peter”, assuming that “Peter”
                  is a resource which is described further in additional statements).</p>
                  <p style="text-align: justify">All in all, such RDF triples form intrinsically a labelled, unidirectional multigraph. In other words, RDF’s subjects,
                  predicates and objects can be represented graphically; with subjects and objects as nodes and predicates as directed edges – all of them labelled accordingly
                  with URIs and/or literals. Such an RDF graph can have multiple, distinct edges between two nodes, whereas these two nodes do not necessarily have to be
                  distinct as well. The later type of graph is also called a “multigraph”.</p>
                  <p style="text-align: justify">By convention, resources are represented in illustrated RDF graphs as ellipses, in contrast to literals, which are normally
                  depicted as rectangles. So, they are easily distinguishable. Below such an exemplary RDF graph is depicted, illustrating the main concepts of RDF just
                  explained.</p>
                  <div style="text-align: center;margin-bottom: 0.8rem;">
                    <img src="img/rdf.png" alt="graphical representation of exemplary RDF triples"/>
                  </div>
                  <p style="text-align: justify">Of course, there is much more to learn about RDF and overlying technologies. However, for the sake of the discussion at this
                  point the explanations up to now should prove to be sufficient. Anyway, in order to understand how the budgetary data is modelled in this very project, its
                  composition and used RDF vocabulary needs to be explained as well.</p>
                  <p style="text-align: justify">Most of the statistical data (as well as the budgetary data on this website) is nowadays organised digitally in
                  multi-dimensional so-called “data cubes”. The primary features of such a (three dimensional and simple) data cube are illustrated in the picture below.</p>
                  <div style="text-align: center;margin-bottom: 0.8rem;">
                    <img src="img/data-cube.png" alt="Structure and elements of an illustrated data cube with three dimensions"/>
                  </div>
                  <p style="text-align: justify">An arbitrary <i>data set</i> originating the same thematic area composes a <i>data cube</i> with a virtually unlimited number
                  of individual <i>data records</i> comprising its elements. Each and every data record itself consists of various attributes and one or more measurands. The
                  record can be located or, to be more precise, is identified by an explicit number of its attributes or key indices. Each combination of these indices is
                  unique for every data record in the cube (e.g. the time and place the data was recorded and so on). Like the three variables <i>x</i>, <i>y</i> and <i>z</i>
                  are normally used to identify any point in a three-dimensional space, the mentioned indices identify every data record in the cube unambiguously and are
                  called likewise <i>dimensions</i>.</p>
                  <p style="text-align: justify">The advantage of organising data in this – of course – more complex way (in comparison e.g. with classical two dimensional
                  spreadsheets) is – among other things – that reality can be modelled and, as a consequence, described in a more correct and fine-grained way. The data
                  itself as well as analytics based on it are able to be much more meaningful, informative and, if nothing else, more detailed. Of course, this more complex
                  method needs more attention during modelling in order to work as intended.</p>
                  <p style="text-align: justify">For this purpose, the so-called “RDF Data Cube Vocabulary” is used explicitly for modelling the vocabulary for <a
                  onclick="jumpTo('municipal-budgets');">local government finances</a> in Austria and respective data. This Data Cube vocabulary is primarily focused on the
                  publication of multi-dimensional data as Linked Data. The actual, overall structure of the “RDF Data Cube Vocabulary” is depicted below.</p>
                  <div style="text-align: center;margin-bottom: 0.8rem;">
                    <img src="img/qb-overview.png" alt="Overview of all conceptual elements and their relationship in the 'RDF Data Cube Vocabulary'"/>
                  </div>
                  <p style="text-align: justify">The heart of this project’s application of this RDF vocabulary is – illustrated in the upper left corner – a <i>data set</i>
                  and its respective <i>data set definition</i>. Whether these concepts are used correctly – hence, whether a so-called “well-formed” data cube was created –
                  is answered by reviewing certain predefined “integrity constraints” – for the matter of this project this is done on <a
                  href="<c:url value="/data/ic"/>">this page</a>. However, for more information on these concepts in particular and this meta-vocabulary in general, please
                  refer to <a href="http://www.w3.org/TR/vocab-data-cube" target="_blank">its specification</a> by the World Wide Web Consortium.</p>

                  <t:inlineBtt anchorId="headline-anchor"/>
                  <h6>Open Data</h6><div id="open-data" style="position: relative; top: -5.5rem;"></div>
                  <p style="text-align: justify">Last but not least, the disclosure of digital data in general or its result, respectively, is commonly known as “Open Data”
                  – eponymous for the title of this project's website. The idea of Open Data’s concept is not new. It originates for digital contents from the idea of
                  “<a href="http://en.wikipedia.org/wiki/Open-source_model" target="_blank">Open Source</a>” software, whose source code is freely available for everyone.
                  The reason for creating such “Open Source” software in the first place was that developers did not want to start from scratch each time implementing a
                  new application. Instead, there was a certain need to build upon source code by others – if possible, without any (commercial or legal) hurdles. With
                  Open Data, the footsteps of “Open Source” software shall be followed for digital data in general, possibly achieving similar popularity as its 
                  forefather.</p>
                  <p style="text-align: justify">While the general concept of Open Data in particular seem to be quite obvious at first glance, an explicit definition is
                  not that easy. One of the commonly adopted definitions is the “<a href="http://opendefinition.org/od/2.1/en" target="_blank">Open Definition</a>” by
                  <a href="https://okfn.org" target="_blank">Open Knowledge International</a>. It describes Open Data as information that can be freely accessed, used,
                  modified, and shared by anyone for any purpose (subject, at most, to requirements that preserve provenance and openness).</p>
                  <p style="text-align: justify">Of course, the legal aspects of such Open Data are manifold. However, they are not tackled on this website in particular.
                  However, it shall be noted that respective <a href="<c:url value="/about#licence"/>">licensing</a> is also used by this project. For more information
                  about legislation on Open Data as well as the respective licenses, please refer to the underlying <a data-toggle="thesisModal">Diploma Thesis</a> of this
                  project.</p>
                  <p style="text-align: justify"><a onclick="jumpTo('linked-data');">Linked Data</a>, which is released under an open licence, which – in turn – does not
                  impede its reuse for free, is called “Linked Open Data” (LOD). The latter concept describes – at its best – structured, highly interconnected and syntactic
                  interoperable information or datasets, respectively, which are distributed over various data sources and repositories (like this project’s 
                  <a href="<c:url value="/sparql"/>">SPARQL endpoint</a>) within the same or different organisation and can be used by everyone without any restrictions.
                  As a consequence, LOD is an important mechanism for information management and integration; hence, in turn, it becomes more and more important in
                  state-of-the-art implementations of data interfaces and storages for Internet-based services and applications. In this way, data is not just published
                  somewhere <i>on</i> the World Wide Web, but instead <i>in</i> the Web by using LOD; this way, the goal to achieve a real &amp; infinite <i>Web of Data</i>
                  – additionally, to a <i>Web of Documents</i> – moves a little bit closer.</p>
                  <p style="text-align: justify">Representing that this process is more a progressive, stepwise transition rather than an encompassing adoption in one go,
                  famous <a href="http://en.wikipedia.org/wiki/Tim_Berners-Lee" target="_blank">Tim Berners-Lee</a> introduced a so-called “<a href="http://5stardata.info"
                  target="_blank">5-Star-Scheme</a>” for Linked Open Data. As depicted below, it classifies published datasets from protected, not reusable content (without
                  any star) through to real LOD composing an open <i>Web of Data</i> (gaining a total of five stars).</p>
                  <div style="text-align: center;margin-bottom: 0.8rem;">
                    <img src="img/5-star-scheme.png" alt="The 5-Star-Scheme for Linked Open Data"/>
                  </div>
                  <img style="float: right; margin: 0.5rem 0 0.5rem 1rem;" alt="5 star Linked Open Data" src="img/data-badge-5.png"/>
                  <p style="text-align: justify">As shown in detail, by this project’s underlying Diploma Thesis, the Linked Open Budget Data as well as its respective
                  vocabulary for local government finances for Austrian municipalities is compliant to 5-star-LOD: the available information is modelled as non-proprietary,
                  machine-readable and structured <span data-tooltip aria-haspopup="true" class="has-tip bottom" data-disable-hover="false" tabindex="31"
                  title="Resource Description Framework">RDF</span> data, where dereferenceable <span data-tooltip aria-haspopup="true" class="has-tip bottom"
                  data-disable-hover="false" tabindex="32" title="Uniform Resource Identifier">URI</span>s are used to identify given concepts as well as the data records
                  themselves. These URIs conform to overall requirements of <a onclick="jumpTo('linked-data');">Linked Data</a>. Furthermore, even Linked Data outside the
                  dataset’s namespace is referenced (e.g. for <a href="http://sws.geonames.org" target="_blank">identification of municipalities</a>). With that the given
                  project reaches full-blown, encompassing and comprehensive Linked Open Budget Data for Austrian municipalities and provides an unprecedented contribution
                  to the evolving <i>Web of Data</i> in Austria in the end.</p>
                  
                  <hr style="margin: 0.5em auto;" />
                  
                  <p style="text-align: justify" class="footnotes">On this page, all used figures are made for the purposes of this website itself – except the <a
                  href="https://www.w3.org/TR/vocab-data-cube" target="_blank">overview of the “RDF Data Cube Vocabulary”</a>, which was originally created by the
                  <span data-tooltip aria-haspopup="true" class="has-tip bottom" data-disable-hover="false" tabindex="51" title="World Wide Web Consortium">W3C</span>,
                  and the <a href="http://lab.linkeddata.deri.ie/2010/lod-badges" target="_blank">LOD star badges</a> by Richard Cyganiak. All figures can be used – as
                  all other parts of this website – freely under the <a rel="license" href="http://creativecommons.org/licenses/by/4.0/" target="_blank">CC-BY license</a>.
                  </p>
                </div>

                <div class="large-4 medium-4 cell">
                    <t:stat datasetStatistic="${datasetStatistic}"/>
                    <div class="callout">
                        <h5>Which Diploma Thesis is this?</h5>
                        <p style="text-align: justify">This website was originally developed and published in the year 2017 as part of a
                        Diploma Thesis in fulfilment of the requirements of the Master Programme in Business Informatics at the
                        <a href="http://www.informatik.tuwien.ac.at/english" target="_blank">Faculty of Informatics</a>
                        of the <a href="https://www.tuwien.ac.at/en" target="_blank">Vienna University of Technology</a>. Of course, it is publicly available
                        for everyone to gain further inside in this topic as well as the implemented approach shown with this project.</p>
                        <a class="small button" data-toggle="thesisModal">Show me how I can get this Diploma Thesis</a>
                    </div>
                    <div class="small thesis reveal" id="thesisModal" data-reveal data-close-on-click="true"
                            data-animation-in="fade-in" data-animation-out="fade-out">
                        <h4>How to get this Diploma Thesis...</h4>
                        <div class="warning message callout" data-closable>
                            <p>Currently, this work is not (yet) publicly available. The release can be expected in spring 2018.</p>
                        </div>
                        <a href="http://repositum.tuwien.ac.at/#" onclick="return false;" class="thumbnail disabled" style="float: left; margin-right: 1rem;">
                            <img src="img/title-page.png" alt="Title Page of the Diploma Thesis">
                        </a>
                        <img src="img/repositum-logo.png" alt="reposiTUm" class="disabled" style="float: right; margin-left: 1rem;">
                        <p class="disabled" style="text-align: justify">As a single point of contact, the Diploma Thesis serving as a foundation for this project’s website is 
                        hosted by the <a href="https://www.tuwien.ac.at/en" target="_blank">Vienna University of Technology</a> or its
                        <a href="http://repositum.tuwien.ac.at" target="_blank">University Library</a>, respectively. In this way the publication is archived,
                        provided with a <span data-tooltip aria-haspopup="true" class="has-tip bottom" data-disable-hover="false" tabindex="101"
                        title="uniform resource name (URN)">persistent identifier</span>, so that it can be reliably &amp; consistently referenced, and made
                        available online all over the globe for use in accordance with applicable copyright law.</p>
                        <p class="disabled" style="text-align: justify">The actual document is available via the button below in its PDF version <i>only</i> and can be used
                        freely by anyone without any restrictions as all scientific works on Austrian Universities. For more information on the document and
                        the work itself, please have a look at the respective
                        <a href="http://repositum.tuwien.ac.at/#" onclick="return false;" target="_blank">page</a>
                        on the website of the University Library.</p>
                        <a href="http://repositum.tuwien.ac.at/#" onclick="return false;" class="round hollow button disabled" target="_blank">Download</a>
                        
                        <button class="close-button" data-close aria-label="Close reveal" type="button">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="callout">
                        <h5>Which technologies are used here?</h5>
                        <p style="text-align: justify">This project’s website uses a variety of different technologies as well as third-party applications, which are
                        <span data-tooltip aria-haspopup="true" class="has-tip bottom" data-disable-hover="false" tabindex="91"
                        title="software whose source code is publicly available">open source</span>, for implementing Linked Open Data in an appropriate, sophisticated way.
                        Of course, all used components are documented on this website as well as the terms under which they are used and their respective source.</p>
                        <a href="<c:url value="/about#third-party-apps"/>" class="small button">Learn more about these used technologies</a>
                    </div>
                </div>
              </div>
            </div>
    </jsp:body>
</t:template>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<t:template pageName="Integrity Check" activeMenuItemNo="2">
    <jsp:body>
              <div class="grid-x grid-padding-x">
                <div class="large-12 cell">
                    <nav aria-label="You are here:" role="navigation">
                        <ul class="breadcrumbs">
                            <li><a href="<c:url value="/"/>">Home</a></li>
                            <li><a href="<c:url value="http://data.lombuda.at"/>">Data</a></li>
                            <li>
                                <span class="show-for-sr">Current: </span> Integrity Check
                            </li>
                        </ul>
                    </nav>
                </div>
              </div>
              
              <div class="grid-x grid-padding-x">
                <div class="large-12 cell">
                  <h1>Data Cube Integrity Check</h1>
                </div>
              </div>

              <div class="grid-x grid-padding-x">
                <div class="large-8 medium-8 cell">
                  <h5 class="subheader">A confirmation of the consistent structure of the dataset</h5>

                  <p style="text-align: justify">The table below shows a live report on the status of the integrity of the currently saved
                  <a href="<c:url value="http://data.lombuda.at"/>">dataset</a> or data cube of <span class="page-title">LOMBuDa.at</span>
                  according to the used <a target="_blank" href="<c:url value="https://www.w3.org/TR/vocab-data-cube/"/>">RDF Data Cube Vocabulary</a> by
                  <span data-tooltip aria-haspopup="true" class="has-tip bottom" data-disable-hover="false" tabindex="10" title="World Wide Web Consortium">W3C</span>.
                  This integrity is checked via various predefined constraints, which are listed in the presented table and are executed automatically via prewritten
                  <a href="<c:url value="/sparql"/>">SPARQL</a> <span data-tooltip aria-haspopup="true" class="has-tip bottom" data-disable-hover="false" tabindex="11" 
                  title="Used to provide a simple True/False result for a query on a SPARQL endpoint.">ASK</span> queries. To have a look at those queries, click on
                  “download query” in the respective row of the table. In a nutshell, this page verifies and shows the semantic consistency of the dataset at a glance.</p>

                  <table class="small hover" style="width: 80%;margin: 0.7rem auto 0;">
                        <thead>
                            <tr>
                                <th class="no-wrap" style="width: 4em;">&nbsp;IC-#</th>
                                <th>Brief Description of the Integrity Constraint</th>
                                <th class="no-wrap center" style="width: 4em;">Status</th>
                                <th class="no-wrap center" style="width: 4em;">SPARQL Query</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:choose><c:when test="${not empty ics}">
	                            <c:forEach items="${icNames}" var="icName">
		                            <tr>
		                                <td class="no-wrap">${icName}<c:if test="${icName=='IC-03' || icName=='IC-08'}"><sup>1)</sup></c:if><c:if test="${icName=='IC-19c'}"><sup>2)</sup></c:if><c:if test="${icName=='IC-20' || icName=='IC-21'}"><sup>3)</sup></c:if></td>
		                                <td>${ics[icName].description}</td>
		                                <td class="no-wrap center"><c:choose><c:when test="${ics[icName].violated}"><span class="label warning">NOK</span></c:when><c:otherwise><span class="label success">OK</span></c:otherwise></c:choose></td>
		                                <td class="no-wrap center"><a href="<c:url value="/data/ic/download/${icName}"/>">DOWNLOAD QUERY</a></td>
		                            </tr>
	                            </c:forEach>
                            </c:when><c:otherwise>
                                <tr>
                                    <td colspan="4" class="center">No integrity check results found. Please try again later. Sorry for that.</td>
                                </tr>
                            </c:otherwise></c:choose>
                        </tbody>
                  </table>
                  <ol class="footnotes">
	                  <li>These integrity constraints have been changed to work also on an “abbreviated” data cube - in fact, in their current form they are stricter
	                      composed as specified.</li>
	                  <li>This integrity constraint was introduced in addition to the other ones to check specifics of this application's data set in more detail -
	                      it does <i>not</i> origin from the <span data-tooltip aria-haspopup="true" class="has-tip bottom" data-disable-hover="false" tabindex="12"
	                      title="World Wide Web Consortium">W3C</span>.</li>
	                  <li>These integrity constraints are <i>not</i> executed to their full extend as specified by the <span data-tooltip aria-haspopup="true" 
	                      class="has-tip bottom" data-disable-hover="false" tabindex="13" title="World Wide Web Consortium">W3C</span>. However, as they do not apply
	                      to the application's data set (as the checked elements are not used at all), they will always validate successfully.</li>
                  </ol>
                </div>
                <div class="large-4 medium-4 cell">
                    <div class="callout">
                        <h5>What are “integrity constraints”?</h5>
                        <p style="text-align: justify">As specified in the <span data-tooltip aria-haspopup="true" class="has-tip bottom" data-disable-hover="false" tabindex="14"
                          title="World Wide Web Consortium">W3C</span> Recommendation of the used <a target="_blank" href="<c:url value="https://www.w3.org/TR/vocab-data-cube/"/>">
                          RDF Data Cube Vocabulary</a>, an instance of such a data cube (like the <a href="<c:url value="http://data.lombuda.at"/>">one</a> of
                          <span class="page-title">LOMBuDa.at</span>) should conform to a set of integrity constraints in order to be valid according to this vocabulary.
                          For example, it is tested whether all defined and required attributes are present for each data record. These integrity constraints are defined as
                          narrative prose and <a href="<c:url value="/sparql"/>">SPARQL</a> <span data-tooltip aria-haspopup="true" class="has-tip bottom"
                          data-disable-hover="false" tabindex="15" title="Used to provide a simple True/False result for a query on a SPARQL endpoint.">ASK</span> queries by
                          the <span data-tooltip aria-haspopup="true" class="has-tip bottom" data-disable-hover="false" tabindex="16" title="World Wide Web Consortium">W3C</span>.
                          Indeed, the latter ones are used for the checks presented on this very page.</p>
                        <a target="_blank" href="https://www.w3.org/TR/vocab-data-cube/#wf" class="small button">Go to the integrity constraints' definition by the W3C</a>
                    </div>
                    <div class="callout">
                        <h5>What is a data cube?</h5>
                        <p style="text-align: justify">The <a href="<c:url value="http://data.lombuda.at"/>">data set</a> of <span class="page-title">LOMBuDa.at</span> is
                        organised as a so-called “data cube”. This is a multi-dimensional array of values, commonly used to represent data along some measures of interest.
                        Every dimension of the cube represents a new measure; whereas the cells in the cube represent the facts of interest or the data records, respectively.</p>
                        <a href="<c:url value="/#linked-data"/>" class="small button">Learn more about this application's data cube</a>
                    </div>
                    
                    <div class="callout">
                        <h5>What is an “abbreviated” cube?</h5>
                        <p style="text-align: justify">The used <a target="_blank" href="<c:url value="https://www.w3.org/TR/vocab-data-cube/"/>">RDF Data Cube Vocabulary</a>
                        allows to associate common parts of data for a set of observations (dimensions, attributes &amp; measurands) to other, higher levels of the data
                        structure (like in an observation group or data slice). This prevents possible overheads in transmission and storage through less data redundancies
                        in such cubes. Such a data cube - like the <a href="<c:url value="http://data.lombuda.at"/>">one</a> of <span class="page-title">LOMBuDa.at</span>
                        - is called “abbreviated”.<br/>
                        In contrast, if all data of a cube is attached to each and every data record, even if this leads to redundancy within the data structure, it is called
                        “normalised”.</p>
                        <a target="_blank" href="https://www.w3.org/TR/vocab-data-cube/#normalize" class="small button">
                        Go to the definition of normalised &amp; abbreviated cubes</a>
                    </div>
                </div>
              </div>

    </jsp:body>
</t:template>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<t:template pageName="Administration" isAdminPage="true">
    <jsp:body>
              <div class="grid-x grid-padding-x">
                <div class="large-12 cell">
                    <nav aria-label="You are here:" role="navigation">
                        <ul class="breadcrumbs">
                            <li><a href="<c:url value="/"/>">Home</a></li>
                            <li>
                                <span class="show-for-sr">Current: </span> Administration
                            </li>
                        </ul>
                    </nav>
                </div>
              </div>
              <div class="grid-x grid-padding-x">
                <div class="large-12 cell">
                  <h1>Administration Area</h1>
                </div>
              </div>

              <div class="grid-x grid-padding-x">
                <div class="large-8 medium-8 cell">
                    <h5>General Server Information</h5>
                    <div class="grid-x grid-padding-x">
                        <div class="large-4 medium-4 cell"><p><strong>Server Info:</strong></p></div>
                        <div class="large-8 medium-8 cell"><p>${context.serverInfo}</p></div>
                        <div class="large-4 medium-4 cell"><p><strong>Context startup date:</strong></p></div>
                        <div class="large-8 medium-8 cell"><p><fmt:formatDate value="${context.startDate}" pattern="E', 'dd.MM.yyyy' - 'HH:mm" timeZone="Europe/Vienna" /> (<time class="timeago" datetime="<fmt:formatDate pattern="yyyy-MM-dd'T'HH:mm:ssZ" value="${context.startDate}" />">running for ${context.runTimeStr}</time>)</p></div>
                        <div class="large-4 medium-4 cell"><p><strong>Memory used:</strong></p></div>
                        <div class="large-8 medium-8 cell"><p><fmt:formatNumber value="${context.usedMemory}" type="number" minFractionDigits="1" maxFractionDigits="1"/> MB  (<fmt:formatNumber value="${(context.usedMemory/context.totalMemory)}" type="percent" maxFractionDigits="0"/> of total memory) - <a href="<c:url value="/admin/runGc"/>">run garbage collector</a><c:if test="${not empty collectedSpace}"><br><fmt:formatNumber value="${collectedSpace}" type="number" minFractionDigits="1" maxFractionDigits="1"/> MB collected!</c:if></p></div>
                        <div class="large-4 medium-4 cell"><p><strong>Maximum memory space:</strong></p></div>
                        <div class="large-8 medium-8 cell"><p><fmt:formatNumber value="${context.maxMemory}" type="number" minFractionDigits="1" maxFractionDigits="1"/> MB</p></div>
                        <div class="large-4 medium-4 cell"><p><strong># of visitors so far:</strong></p></div>
                        <div class="large-8 medium-8 cell"><p><fmt:formatNumber value="${visitorCount}" type="number" minFractionDigits="0" maxFractionDigits="0"/></p></div>
                    </div>

                    <h5>Service Status Information</h5>
                    <div class="grid-x grid-padding-x">
                        <div class="large-4 medium-4 cell"><p><strong>General service layer status:</strong></p></div>
                        <div class="large-8 medium-8 cell"><p><c:choose><c:when test="${serviceLayerInitialized && empty serviceLayerDelayedInitialized && !serviceLayerInitialisationError}"><span class="label warning">INITIALISING</span></c:when><c:when test="${serviceLayerInitialized && serviceLayerDelayedInitialized}"><span class="label success">OK</span></c:when><c:when test="${serviceLayerShutdown}"><span class="label alert">SHUTDOWN</span></c:when><c:otherwise><span class="label alert">ERROR</span></c:otherwise></c:choose></p></div>
                        <div class="large-4 medium-4 cell"><p><strong>GeoNames web service status:</strong></p></div>
                        <div class="large-8 medium-8 cell"><p><c:choose><c:when test="${!isGeoNamesWebServiceReachable && !serviceLayerDelayedInitialized}"><span class="label warning">AWAITING TEST CALL</span></c:when><c:when test="${isGeoNamesWebServiceReachable}"><span class="label success">REACHABLE</span></c:when><c:otherwise><span class="label alert">NOT REACHABLE</span></c:otherwise></c:choose> - <a href="<c:url value="/admin/geoNamesTest"/>">perform test call</a><c:if test="${isGeoNamesWebServiceTestSuccess}"> - test call successful!</c:if></p></div>
                    </div>

                    <h6>SPARQL Endpoint</h6>
                    <div class="grid-x grid-padding-x">
                        <div class="large-4 medium-4 cell"><p><strong>URL:</strong></p></div>
                        <div class="large-8 medium-8 cell"><pre>${sparqlEndpointUrl}</pre></div>
                        <div class="large-4 medium-4 cell"><p><strong>Status:</strong></p></div>
                        <div class="large-8 medium-8 cell"><p><c:choose><c:when test="${not isSparqlEndpointOffline}"><span class="label success">ONLINE</span></c:when><c:otherwise><span class="label alert">OFFLINE</span></c:otherwise></c:choose></p></div>
                        <div class="large-4 medium-4 cell"><p><strong>Last query execution:</strong></p></div>
                        <div class="large-8 medium-8 cell"><p><c:choose><c:when test="${not empty lastSparqlQueryExecutionDate}"><fmt:formatDate value="${lastSparqlQueryExecutionDate}" pattern="E', 'dd.MM.yyyy' - 'HH:mm" timeZone="Europe/Vienna" /> (<time class="timeago" datetime="<fmt:formatDate pattern="yyyy-MM-dd'T'HH:mm:ssZ" value="${lastSparqlQueryExecutionDate}" />">${lastSparqlQueryExecutionDateStr} ago</time>)</c:when><c:otherwise>N/A</c:otherwise></c:choose></p></div>
                        <div class="large-4 medium-4 cell"><p><strong>Last update execution:</strong></p></div>
                        <div class="large-8 medium-8 cell"><p><c:choose><c:when test="${not empty lastSparqlUpdateExecutionDate}"><fmt:formatDate value="${lastSparqlUpdateExecutionDate}" pattern="E', 'dd.MM.yyyy' - 'HH:mm" timeZone="Europe/Vienna" /> (<time class="timeago" datetime="<fmt:formatDate pattern="yyyy-MM-dd'T'HH:mm:ssZ" value="${lastSparqlUpdateExecutionDate}" />">${lastSparqlUpdateExecutionDateStr} ago</time>)</c:when><c:otherwise>N/A</c:otherwise></c:choose></p></div>
                        <c:if test="${not empty datasetStatistic}">
                        <div class="large-4 medium-4 cell"><p><strong># of performed application requests:</strong></p></div>
                        <div class="large-8 medium-8 cell"><p><fmt:formatNumber value="${numOfSparqlRequests}" type="number" minFractionDigits="0" maxFractionDigits="0"/></p></div>
                        <c:if test="${not empty numOfForwardedSparqlRequests}">
                        <div class="large-4 medium-4 cell"><p><strong># of forwarded requests:</strong></p></div>
                        <div class="large-8 medium-8 cell"><p><fmt:formatNumber value="${numOfForwardedSparqlRequests}" type="number" minFractionDigits="0" maxFractionDigits="0"/></p></div>
                        </c:if>
                        </c:if>
                    </div>

                    <h6>Data Set Metrics</h6>
                    <div class="grid-x grid-padding-x">
                        <c:if test="${not empty datasetStatistic}">
                        <div class="large-4 medium-4 cell"><p><strong>Current dataset statistic creation time:</strong></p></div>
                        <div class="large-8 medium-8 cell"><p><fmt:formatDate value="${datasetStatistic.creationTime}" pattern="E', 'dd.MM.yyyy' - 'HH:mm" timeZone="Europe/Vienna" /> (<time class="timeago" datetime="<fmt:formatDate pattern="yyyy-MM-dd'T'HH:mm:ssZ" value="${lastSparqlQueryExecutionDate}" />">${datasetStatistic.creationTimeIntervalStr} ago</time>) - <a href="<c:url value="/admin/refreshStatistics"/>">refresh statistics</a></p></div>
                        </c:if>
                        <div class="large-4 medium-4 cell"><p><strong># of available data slices:</strong></p></div>
                        <div class="large-8 medium-8 cell"><p><fmt:formatNumber value="${numOfAvailableDataSlices}" type="number" minFractionDigits="0" maxFractionDigits="0"/> statements of account - <a href="<c:url value="/admin/refreshAvailableDataSlices"/>">refresh</a></p></div>
                        <div class="large-4 medium-4 cell"><p><strong>Data Set Integrity:</strong></p></div>
                        <div class="large-8 medium-8 cell"><p><c:choose><c:when test="${empty isIcOK}"><span class="label warning">AWAITING INITIAL CHECK</span></c:when><c:when test="${isIcOK}"><span class="label success">OK</span></c:when><c:otherwise><span class="label alert">NOK</span></c:otherwise></c:choose> (<a href="<c:url value="/data/ic"/>">show more</a>) <c:if test="${not empty isIcOK}"> - <a href="<c:url value="/admin/checkIntegrity"/>">perform check</a></c:if></p></div>
                    </div>
                </div>
                <div class="large-4 medium-4 cell">
                    <t:stat datasetStatistic="${datasetStatistic}"/>
                    <div class="callout">
                        <h5>Where is the rest of the page?</h5>
                        <p style="text-align: justify">This is the administration area of <span class="page-title">LOMBuDa.at</span>. Here in this self-contained area of this web site reside protected information and functions which are likely to compromise overall application functionality and stability, if they would be used by unauthorized personnel. The rest of this web site is open to the public, where no indication of this administration area exists.</p>
                        <a href="<c:url value="/" />" class="small button">Go to the Public Area</a>
                    </div>
                </div>
              </div>
    </jsp:body>
</t:template>
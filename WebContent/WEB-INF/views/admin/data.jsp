<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<t:template pageName="Data Operations" isAdminPage="true" activeMenuItemNo="1">
    <jsp:body>
              <div class="grid-x grid-padding-x">
                <div class="large-12 cell">
                    <nav aria-label="You are here:" role="navigation">
                        <ul class="breadcrumbs">
                            <li><a href="<c:url value="/"/>">Home</a></li>
                            <li><a href="<c:url value="/admin"/>">Administration</a></li>
                            <li>
                                <span class="show-for-sr">Current: </span> Data Operations
                            </li>
                        </ul>
                    </nav>
                </div>
              </div>
              <div class="grid-x grid-padding-x">
                <div class="large-12 cell">
                  <h1>Data Operations</h1>
                </div>
              </div>

	          <script> 
		          function submitAndCheckFileInput(inputId, formId) {
		              if( document.getElementById(inputId).files.length == 0 ){
		                  alert("Please select at least one file for upload!");
		              } else {
		                  document.getElementById(formId).submit();
		              }
		          }
                  function submitAndCheckFileInputAndRadio(inputId, radioId, formId) {
                	  var form_elements = document.getElementById(formId).elements;
                	  var selectedType = form_elements[radioId].value;
                      if( document.getElementById(inputId).files.length == 0 ){
                          alert("Please select a file for import!");
                      } else if(selectedType == null || selectedType == '') {
                          alert("Please select the type of the code list for import!");
                      } else {
                          document.getElementById(formId).submit();
                      }
                  }
		          function submitAndCheckButton(formId, buttonId) {
		        	  if(!document.getElementById(buttonId).classList.contains('disabled'))
		        		  document.getElementById(formId).submit();
                  }
                  function enableButton(buttonId) {
                	  document.getElementById(buttonId).classList.toggle('disabled');
                  }
                  
                  function enableInput(inputId) {
                	  document.getElementById(inputId).disabled = false;
                  }
                  function disableInput(inputId, clearInput) {
                      document.getElementById(inputId).disabled = true;
                      if(clearInput)
                    	  document.getElementById(inputId).value = '';
                  }
                  function toggleInput(inputId, causingId, clearInput) {
                	  var causeValue = document.getElementById(causingId).value;
                	  if(causeValue != null && causeValue != '')
                		  disableInput(inputId, clearInput);
                	  else
                		  enableInput(inputId);
                  }
                  
	
			      function filloutPreset(preset) {
			    	  var btnGrp = document.getElementById('preset-group');
			    	  
			    	  var btn = null;
			    	  for (var i = 0; i < btnGrp.childNodes.length; i++) {
			    	      btn = btnGrp.childNodes[i];
			    	      console.log(btn);
			    	      if(btn.tagName == 'A' && btn.classList.contains('selected'))
			    	    	  btn.classList.remove('selected');
			    	  }
			    	  
			    	  if(preset != 'reset')
			    		  document.getElementById(preset+'-preset').classList.add('selected');
			    	  
			    	   switch (preset) {
			    	      case 'oh':
                              document.getElementById('buda-cn-src-url').value = 'http://offenerhaushalt.at';
                              document.getElementById('buda-cn-src-desc').value = 'This derived data originates from \"OffenerHaushalt.at\" by \"KDZ - Zentrum für Verwaltungsforschung\" and is used under CC-BY-3.0-AT.';
			    	    	  fill('buda-cn-year','jahr','buda-option-cn-year');
                              fill('buda-cn-muni','gkz','buda-option-cn-muni');
                              document.getElementById('buda-option-stat-ra').checked = true;
                              document.getElementById('buda-option-bce-separ').checked = true;
                              fill('buda-cn-bi','haushaltskonto-hinweis');
                              fill('buda-cn-fc','haushaltskonto-ansatz');
                              fill('buda-cn-ec','haushaltskonto-post');
                              fill('buda-cn-amount','soll-rj');
                              
                              disableInput('buda-year',true);
                              disableInput('buda-muni-nr',true);
                              disableInput('buda-muni-name',true);
                              disableInput('buda-cn-stat',true);
                              disableInput('buda-cn-bc',true);
                              disableInput('buda-cn-type',true);
			    	      break;
                          case 'reset':
                              document.getElementById('data-import').reset();
                          break;
			    	      default:
			    	    	  console.log("NOT SET");
			    	      break;
				       }
				  }
			      function fill(inputId,value,radioId) {
                      document.getElementById(inputId).value = value;
                      enableInput(inputId);
                      if(radioId != null)
                    	  document.getElementById(radioId).checked = true;
			      }

                  var optFields = ["buda-cn-desc"];
			      function checkAndSubmit(formId) {
			    	  var form = document.getElementById(formId);
			    	  var inputs = form.getElementsByTagName("input"), input = null, radioNames = new Set();
			    	  for(var i = 0, len = inputs.length; i < len; i++) {
		    	        input = inputs[i];
		    	        if(isInputEmpty(input))
		    	        	return;
		    	        if(input.type == 'radio') {
		    	        	radioNames.add(input.name);
		    	        }
		    	      }
			    	  
			    	  var it = radioNames.values();
			    	  var entry;
			    	  while (!(radioName = it.next()).done) {
                          var radioGrp = document.getElementsByName(radioName.value);
                          if(!isRadioGroupSelected(radioName.value, radioGrp))
                        	  return;
			    	  }
			    	  
			    	  var txtArea =  document.getElementById('buda-cn-src-desc');
			    	  if(isInputEmpty(txtArea))
			    		  return;
			    	  
			    	  document.getElementById(formId).submit();
			      }
			      function isInputEmpty(input) {
			    	  if(!input.disabled && !input.value && optFields.indexOf(input.id) == -1 ) {
                          input.focus();
                          if(input.type=='file')
                              alert("Please select a file for import!");
                          else
                              alert("Please fill out all necessary fields!\n(missing "+input.name+")");
                          return true;
                      }
			    	  return false;
			      }
			      function isRadioGroupSelected(radioName, radioGrp) {
                      for (var i = 0, length = radioGrp.length; i < length; i++) {
                    	  if(radioGrp[i].checked)
                    		  return true;
                      }
                      alert("Please make all decisions!\n(missing "+radioName+")");
                      return false
			      }
		      </script>
	          
              <div class="grid-x grid-padding-x">
                <div class="large-8 medium-8 cell">
                    <t:msg msgStatus="${msgStatus}" msg="${msg}"/>
                    
                    <h5>Import of Municipal Budget Data</h5>
                    <form id="data-import" method="POST" action="<c:url value="/admin/data/upload"/>" enctype="multipart/form-data" accept-charset="UTF-8">
                        <input multiple type="hidden" name="type" value="DATA_IMPORT"/>
	                    <div class="grid-x grid-padding-x"><div class="large-12 medium-12 cell"><p style="text-align: justify">At this point data on municipal budgets can be
	                    uploaded. The data will be directly transformed and imported into the backed dataset (in contrast, to all other upload forms on this page). In order to
	                    support this operation, various information need to be given directly in the form below or, if not, within the additionally given file.<br/>
	                    The uploaded file itself needs to be in <span data-tooltip aria-haspopup="true" class="has-tip bottom" data-disable-hover="false" tabindex="10" 
	                    title="comma-separated values - in this case only ';' are allowed">CSV</span> format with (named) columns for (at least) the recorded, aggregated
	                    <span data-tooltip aria-haspopup="true" class="has-tip bottom" data-disable-hover="false" tabindex="11" 
	                    title="running target values in Euro with ',' (comma) as decimal mark">amount of cash flow</span> for the given fiscal year including the respective
	                    budgetary classification for this cash flow, which can be given in a <span data-tooltip aria-haspopup="true" class="has-tip bottom"
	                    data-disable-hover="false" tabindex="12" title="approach, account and (optional) budgetary indicator classifications in separate columns">separated</span> 
	                    or <span data-tooltip aria-haspopup="true" class="has-tip bottom" data-disable-hover="false" tabindex="13"
	                    title="all classifications in one column, but separated by dots ('.')">combined</span> way. In case no budgetary indicator is given alongside the
	                    other budgetary classifications, the <span data-tooltip aria-haspopup="true" class="has-tip bottom" data-disable-hover="false" tabindex="14" 
	                    title="'REV' revenue & 'EXP' for expenditure">fiscal type</span> of a aggregated cash flow must be given explicitly. Budgetary classifications for 
	                    <span data-tooltip aria-haspopup="true" class="has-tip bottom" data-disable-hover="false" tabindex="15" 
	                    title="Voranschlags- und Rechnungsabschlussverordnung">VRV</span>'s <span data-tooltip aria-haspopup="true" class="has-tip bottom" 
	                    data-disable-hover="false" tabindex="16" title="functional classifications - in German 'Ansätze'">approaches</span> and <span data-tooltip 
	                    aria-haspopup="true" class="has-tip bottom" data-disable-hover="false" tabindex="17" title="economic classifications - in German 'Posten'">accounts</span>
	                    are supported in arbitrary length; however, only the first three mandatory decades of these approaches and accounts are saved - if multiple entries for
	                    one classification are found (considering each only three decades), the respective entries are aggregated automatically during import.<br/>If a dataset
	                    with numbers for a specific fiscal state and specific fiscal year are already present for a specific municipality, the dataset will not imported. By the
	                    way, multiple such datasets can be given at once; however, their non-fixed values (e.g. the fiscal year) must be given in the uploaded file as a
	                    consequence.</p></div>
	                        <div class="large-4 medium-4 cell"><p><label for="budget-data-file"><strong>Select a file for import:</strong></label></p></div>
	                        <div class="large-8 medium-8 cell"><input type="file" accept=".csv" id="budget-data-file" name="files" style="margin-bottom: 0;"/></div>
	                        <div class="large-4 medium-4 cell"><p><strong>Set import presets:</strong></p></div>
	                        <div class="large-8 medium-8 cell"><div class="secondary small button-group" id="preset-group" style="margin-bottom: 0.3rem;">
	                               <a onclick="filloutPreset('reset');" id="reset-preset" class="button">Empty</a>
	                               <a onclick="filloutPreset('oh');" id="oh-preset" class="button">OffenerHaushalt.at</a><a class="button disabled">Other</a>
	                        </div></div>
	                        <div class="large-4 medium-4 cell"><p><label for="buda-cn-src-url"><span data-tooltip aria-haspopup="true" class="has-tip bottom" data-disable-hover="false" tabindex="18" title="mandatory source URL which will be recorded for the complete imported dataset"><strong>Source URL:</strong></span></label></p></div>
	                        <div class="large-8 medium-8 cell"><input type="text" class="inline lone" style="width: 80%;" id="buda-cn-src-url" name="budaCnSrcUrl" placeholder="source URL"/></div>
	                        <div class="large-4 medium-4 cell"><p><label for="buda-cn-src-desc"><span data-tooltip aria-haspopup="true" class="has-tip bottom" data-disable-hover="false" tabindex="18" title="mandatory source description (incl. license declaration) which will be recorded for the complete imported dataset"><strong>Source description:</strong></span></label></p></div>
	                        <div class="large-8 medium-8 cell"><textarea class="inline lone" style="min-width: 75%; width: 80%;min-height: 50px;max-height: 10rem;" id="buda-cn-src-desc" name="budaCnSrcDesc" placeholder="source description"></textarea></div>
	                        <div class="large-4 medium-4 cell"><p><label for="buda-option-cn-year"><span data-tooltip aria-haspopup="true" class="has-tip bottom" data-disable-hover="false" tabindex="18" title="the original calendar year of the budgetary data - must to be always in four-digit representation"><strong>Fiscal year:</strong></span></label></p></div>
	                        <div class="large-8 medium-8 cell">
	                            <input type="radio" class="flat" name="budaOpYear" value="given" id="buda-option-year" onclick="enableInput('buda-year');disableInput('buda-cn-year',true);"/><input type="text" class="inline" id="buda-year" name="budaYear" placeholder="YEAR" maxlength="4" size="1" disabled/><br/>
	                            <input type="radio" class="flat" name="budaOpYear" value="column" id="buda-option-cn-year" onclick="enableInput('buda-cn-year');disableInput('buda-year',true);"/><input type="text" class="inline" id="buda-cn-year" name="budaCnYear" placeholder="COLUMN NAME" size="20" disabled/></div>
	                        <div class="large-4 medium-4 cell"><p><label for="buda-option-cn-muni"><span data-tooltip aria-haspopup="true" class="has-tip bottom" data-disable-hover="false" tabindex="19" title="the German name of the municipality or its five-digit-long code ('Gemeindekennziffer') according to Statistik Austria"><strong>Municipality:</strong></span></label></p></div>
	                        <div class="large-8 medium-8 cell">
	                            <input type="radio" class="flat" name="budaOpMuni" value="given_name" id="buda-option-muni-name" onclick="enableInput('buda-muni-name');disableInput('buda-muni-nr',true);disableInput('buda-cn-muni',true);"/><input type="text" class="inline" id="buda-muni-name" name="budaMuniName" placeholder="municipality name" size="40" disabled/><br/>
	                            <input type="radio" class="flat" name="budaOpMuni" value="given_nr" id="buda-option-muni-nr" onclick="enableInput('buda-muni-nr');disableInput('buda-muni-name',true);disableInput('buda-cn-muni',true);"/><input type="text" class="inline" id="buda-muni-nr" name="budaMuniNr" placeholder="code" maxlength="5" size="3" disabled/><br/>
	                            <input type="radio" class="flat" name="budaOpMuni" value="column" id="buda-option-cn-muni" onclick="enableInput('buda-cn-muni');disableInput('buda-muni-name',true);disableInput('buda-muni-nr',true);"/><input type="text" class="inline" id="buda-cn-muni" name="budaCnMuni" placeholder="COLUMN NAME" size="20" disabled/></div>
	                        <div class="large-4 medium-4 cell"><p><label for="buda-option-cn-stat"><strong>Fiscal State:</strong></label></p></div>
	                        <div class="large-8 medium-8 cell">
	                            <input type="radio" class="flat" name="budaOpStat" value="given_va" id="buda-option-stat-va" onclick="disableInput('buda-cn-stat',true);"/><label for="buda-option-stat-va"><span data-tooltip aria-haspopup="true" class="has-tip bottom" data-disable-hover="false" tabindex="20" title="the annual preliminary budget - in German 'Voranschlag'">budget</span></label><br/>
	                            <input type="radio" class="flat" name="budaOpStat" value="given_ra" id="buda-option-stat-ra" onclick="disableInput('buda-cn-stat',true);"/><label for="buda-option-stat-ra"><span data-tooltip aria-haspopup="true" class="has-tip bottom" data-disable-hover="false" tabindex="21" title="the annual concluding statement of account - in German 'Rechnungsabschluss'">statement of account</span></label><br/>
	                            <input type="radio" class="flat" name="budaOpStat" value="column" id="buda-option-cn-stat" onclick="enableInput('buda-cn-stat');"/><input type="text" class="inline" id="buda-cn-stat" name="budaCnStat" placeholder="COLUMN NAME" size="20" disabled/></div>
	                        <div class="large-4 medium-4 cell"><p><label for="buda-option-bce-combo"><strong>Encoding of budgetary classifications:</strong></label></p></div>
	                        <div class="large-8 medium-8 cell">
	                            <input type="radio" class="flat" name="budaOpBce" value="combo" id="buda-option-bce-combo" onclick="enableInput('buda-cn-bc');disableInput('buda-cn-bi',true);disableInput('buda-cn-type',true);disableInput('buda-cn-fc',true);disableInput('buda-cn-ec',true);"/><label for="buda-option-bce-combo"><span data-tooltip aria-haspopup="true" class="has-tip bottom" data-disable-hover="false" tabindex="22" title="all classifications in one column, but separated by dots ('.')">combined</span></label><br/>
	                            <input type="radio" class="flat" name="budaOpBce" value="separ" id="buda-option-bce-separ" onclick="enableInput('buda-cn-bi');enableInput('buda-cn-type');enableInput('buda-cn-fc');enableInput('buda-cn-ec');disableInput('buda-cn-bc',true);"/><label for="buda-option-bce-separ"><span data-tooltip aria-haspopup="true" class="has-tip bottom" data-disable-hover="false" tabindex="23" title="approach, account and (optional) budgetary indicator classifications in separate columns">separated</span></label></div>
	                        <div class="large-4 medium-4 cell"><p><label for="buda-cn-bc"><span data-tooltip aria-haspopup="true" class="has-tip bottom" data-disable-hover="false" tabindex="24" title="e.g. '1.426.614'"><strong>Budgetary Classification:</strong></span></label></p></div>
	                        <div class="large-8 medium-8 cell"><input type="text" class="inline lone" id="buda-cn-bc" name="budaCnBc" placeholder="column name"size="23" disabled/></div>
	                        <div class="large-4 medium-4 cell"><p><label for="buda-cn-bi"><span data-tooltip aria-haspopup="true" class="has-tip bottom" data-disable-hover="false" tabindex="25" title="single-digit classification according to VRV - in German 'Haushaltshinweis'"><strong>Budgetary Indicator:</strong></span></label></p></div>
	                        <div class="large-8 medium-8 cell"><input type="text" class="inline lone" id="buda-cn-bi" name="budaCnBi" placeholder="column name"size="23" oninput="toggleInput('buda-cn-type', 'buda-cn-bi', true);" disabled/></div>
	                        <div class="large-4 medium-4 cell"><p><label for="buda-cn-type"><span data-tooltip aria-haspopup="true" class="has-tip bottom" data-disable-hover="false" tabindex="26" title="simple fiscal classification: 'REV' revenue & 'EXP' for expenditure"><strong>Fiscal type:</strong></span></label></p></div>
	                        <div class="large-8 medium-8 cell"><input type="text" class="inline lone" id="buda-cn-type" name="budaCnType" placeholder="column name"size="23" oninput="toggleInput('buda-cn-bi', 'buda-cn-type', true);" disabled/></div>
	                        <div class="large-4 medium-4 cell"><p><label for="buda-cn-fc"><span data-tooltip aria-haspopup="true" class="has-tip bottom" data-disable-hover="false" tabindex="27" title="at least three digit functional classification - in German 'Ansatz'"><strong>Approach classification:</strong></span></label></p></div>
	                        <div class="large-8 medium-8 cell"><input type="text" class="inline lone" id="buda-cn-fc" name="budaCnFc" placeholder="column name"size="23" disabled/></div>
	                        <div class="large-4 medium-4 cell"><p><label for="buda-cn-ec"><span data-tooltip aria-haspopup="true" class="has-tip bottom" data-disable-hover="false" tabindex="28" title="at least three digit economic classification - in German 'Posten'"><strong>Account classification:</strong></span></label></p></div>
	                        <div class="large-8 medium-8 cell"><input type="text" class="inline lone" id="buda-cn-ec" name="budaCnEc" placeholder="column name"size="23" disabled/></div>
	                        <div class="large-4 medium-4 cell"><p><label for="buda-cn-amount"><span data-tooltip aria-haspopup="true" class="has-tip bottom" data-disable-hover="false" tabindex="29" title="running target value in Euro with ',' (comma) as decimal mark"><strong>Aggregated amount of cash flow:</strong></span></label></p></div>
	                        <div class="large-8 medium-8 cell"><input type="text" class="inline lone" id="buda-cn-amount" name="budaCnAmount" placeholder="column name"size="23"/></div>
	                        <div class="large-4 medium-4 cell"><p><label for="buda-cn-desc"><span data-tooltip aria-haspopup="true" class="has-tip bottom" data-disable-hover="false" tabindex="30" title="optional description of the respective aggregated data record"><strong>Data record description:</strong></span></label></p></div>
	                        <div class="large-8 medium-8 cell"><input type="text" class="inline lone" id="buda-cn-desc" name="budaCnDesc" placeholder="column name"size="23"/></div>
	                        <div class="large-4 medium-4 cell"></div>
	                        <div class="large-8 medium-8 cell"><a onclick="checkAndSubmit('data-import');" class="small button">Import</a></div>
	                    </div>
                    </form>
                    
                    <hr/>
                    
                    <h5>Automatic rebuild of the backed dataset</h5>
                    <form id="rebuild" method="POST" action="<c:url value="/admin/data/rebuild"/>">
                        <div class="grid-x grid-padding-x">
                            <div class="large-12 medium-12 cell"><p style="text-align: justify">The application can perform an automatic rebuild of the backed dataset.
                            During this operation the data store is reset and the predefined and locally stored data structure definition is loaded. 
                            All former data stored in the backed dataset is lost!</p></div>
                            <div class="large-4 medium-4 cell"><p><strong>I understand that the backed dataset will get reset during this operation:</strong></p></div>
                            <div class="large-8 medium-8 cell"><input id="safeguard" type="checkbox" onclick="enableButton('rebuild-button');"/><label for="safeguard">I understand</label></div>
                            <div class="large-4 medium-4 cell"></div>
                            <div class="large-8 medium-8 cell">
                               <a onclick="submitAndCheckButton('rebuild', 'rebuild-button');" id="rebuild-button" 
                                   class="small animated alert button disabled">Rebuild dataset</a>
                            </div>
                        </div>
                    </form>
                    
                    <h5>Upload for predefined Data Structure Definition</h5>
                    <form id="dsd-upload" method="POST" action="<c:url value="/admin/data/upload"/>" enctype="multipart/form-data">
                        <input multiple type="hidden" name="type" value="DSD_UPLOAD"/>
                        <div class="grid-x grid-padding-x">
                            <div class="large-12 medium-12 cell"><p style="text-align: justify">At this point essential files containing a predefined RDF data structure definition for the application's data cube can be uploaded onto the server for subsequent rebuilds of the backed dataset.
                            The files are automatically classified by their names. Currently following file names are accepted for upload: <span class="pre">code-lists.ttl</span>, <span class="pre">code-lists-approaches.ttl</span>, <span class="pre">code-lists-accounts.ttl</span>, <span class="pre">dsd.ttl</span> and <span class="pre">dataset.ttl</span>.</p></div>
                            <div class="large-4 medium-4 cell"><p><label for="files"><strong>Select one or multiple files for upload:</strong></label></p></div>
                            <div class="large-8 medium-8 cell"><input multiple type="file" accept=".ttl, .rdf, .n3, .jsonld, .rj, .rt, .nt, .nq, .trig, .trdf, .trix" id="files" name="files" style="margin-bottom: 0.2rem;"/></div>
                            <div class="large-4 medium-4 cell"></div>
                            <div class="large-8 medium-8 cell"><a onclick="submitAndCheckFileInput('files','dsd-upload');" class="small button">Upload</a></div>
                        </div>
                    </form>
                    
                    <h5>Import for predefined VRV Code Lists</h5>
                    <form id="code-list-upload" method="POST" action="<c:url value="/admin/data/upload"/>" enctype="multipart/form-data">
                        <input multiple type="hidden" name="type" value="CODE_LIST_IMPORT"/>
	                    <div class="grid-x grid-padding-x">
	                        <div class="large-12 medium-12 cell"><p style="text-align: justify">At this point the full &amp; raw <span data-tooltip aria-haspopup="true" class="has-tip bottom" data-disable-hover="false" tabindex="41" title="Voranschlags- und Rechnungsabschlussverordnung">VRV</span> code lists for all (mandatory) <span data-tooltip aria-haspopup="true" class="has-tip bottom" data-disable-hover="false" tabindex="42" title="functional classification - in German 'Ansätze'">approach</span> and <span data-tooltip aria-haspopup="true" class="has-tip bottom" data-disable-hover="false" tabindex="43" title="economic classification - in German 'Posten'">account</span> classifications as well as the <span data-tooltip aria-haspopup="true" class="has-tip bottom" data-disable-hover="false" tabindex="44" title="aggregating classifications - in German 'Rechnungsquerschnitt Kennzahlen'">budget profile indices</span> of respective municipal cash flows can be imported directly onto the server for subsequent rebuilds of the application's backed dataset. Therefore, respective <span data-tooltip aria-haspopup="true" class="has-tip bottom" data-disable-hover="false" tabindex="45" title="comma-separated values - in this case only ';' are allowed">CSV</span> lists are accepted, which contain at least following (named) columns: <span class="pre">code</span> and <span class="pre">label_XX</span> (where <span class="pre">XX</span> is any language tag according to <a target="_blank" href="http://tools.ietf.org/html/bcp47">IETF's BCP 47</a>, e.g. "en" or "de"). One or multiple different label columns are accepted for coverage of multiple languages. Additionally, for budget profile indices the columns <span data-tooltip aria-haspopup="true" class="has-tip bottom" data-disable-hover="false" tabindex="46" title="abbreviating fiscal type - 'REV' or 'EXP'"><span class="pre">ft</span></span> and <span data-tooltip aria-haspopup="true" class="has-tip bottom" data-disable-hover="false" tabindex="47" title="abbreviating economic classification - any 3-digit economic classificiation"><span class="pre">ec</span></span> are mandatory for building correct associations with the respective economic classifications for subsequent aggregations.</p></div>
	                        <div class="large-4 medium-4 cell"><p><label for="code-list-file"><strong>Select a file for import:</strong></label></p></div>
	                        <div class="large-8 medium-8 cell"><input type="file" accept=".csv" id="code-list-file" name="files" style="margin-bottom: 0;"/></div>
	                        <div class="large-4 medium-4 cell"><p><strong>Select the type of the code list:</strong></p></div>
	                        <div class="large-8 medium-8 cell">
	                            <input type="radio" class="flat" name="codeListType" value="approaches" id="code-list-type-approach"/><label for="code-list-type-approach">approach classifications</label><br/>
	                            <input type="radio" class="flat" name="codeListType" value="accounts" id="code-list-type-account"/><label for="code-list-type-account">account classifications</label><br/>
                                <input type="radio" class="flat" name="codeListType" value="bpi" id="code-list-type-budget-profile-indices"/><label for="code-list-type-budget-profile-indices">budget profile indices</label>
	                        </div>
	                        <div class="large-4 medium-4 cell"></div>
	                        <div class="large-8 medium-8 cell"><a onclick="submitAndCheckFileInputAndRadio('code-list-file','codeListType','code-list-upload');" class="small button">Import</a></div>
	                    </div>
                    </form>
                </div>
                <div class="large-4 medium-4 cell">
                    <c:if test="${not empty showRefresh && showRefresh}">
                        <div class="warning callout">
                            <h5>Refresh data caches</h5>
                            <p style="text-align: justify">The application recognised, that the dataset has received new data. It is recommended to refresh all data caches after such operations, if they were successful. Otherwise the caches will be updated only with significant delay.<br/>Please check and start a complete cache update below.</p>
                            <a href="<c:url value="/admin/data/cache/refresh" />" class="small warning button">Refresh data caches</a>
                        </div>
                    </c:if>
                    <t:stat datasetStatistic="${datasetStatistic}"/>
                    <c:if test="${not empty datasetStatistic}">
                        <div class="callout">
                            <h5>Present classification codes</h5>
                            <p style="text-align: justify">The application's data model holds a variety of different codes for classification of individual items of municipal budgets. Current these are - including their amount:</p>
                            <ul class="small">
                            <c:forEach items="${datasetStatistic.codeCounts}" var="codeCount">
                                <li><b><c:choose><c:when test="${codeCount.codeName == 'rq-index'}">budget profile indices</c:when><c:otherwise>${codeCount.codeName} codes</c:otherwise></c:choose></b>: <fmt:formatNumber value="${codeCount.count}" type="number" minFractionDigits="0" maxFractionDigits="0"/></li>
                            </c:forEach>
                            </ul>
                        </div>
                    </c:if>
                    <div class="callout">
                        <h5>Present default dataset definitions</h5>
                        <p style="text-align: justify">The application stores files locally containing RDF models of the data structure definition for the application's data cube i.a. in case the backed dataset should be rebuilt.</p>
                        <c:choose><c:when test="${empty storedFilenames}">
                        <p style="text-align: justify">However currently no such files could be found on the server.</p>
                        </c:when><c:otherwise>
                        <p style="text-align: justify">Currently these files comprise of the following ones:</p>
                            <ul class="small">
	                        <c:forEach items="${storedFilenames}" var="filename">
                                <li><a target="_blank" href="<c:url value="/admin/data/download/${filename}"/>">${filename}</a></li>
                            </c:forEach>
                            </ul>
                        </c:otherwise></c:choose>
                    </div>
                </div>
              </div>
    </jsp:body>
</t:template>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<t:template pageName="Log Display" isAdminPage="true" activeMenuItemNo="2">
    <jsp:body>
              <div class="grid-x grid-padding-x">
                <div class="large-12 cell">
                    <nav aria-label="You are here:" role="navigation">
                        <ul class="breadcrumbs">
                            <li><a href="<c:url value="/"/>">Home</a></li>
                            <li><a href="<c:url value="/admin/"/>">Administration</a></li>
                            <li>
                                <span class="show-for-sr">Current: </span> Log Display
                            </li>
                        </ul>
                    </nav>
                </div>
              </div>

              <div class="grid-x grid-padding-x">
                <div class="large-7 medium-7 cell">
                  <h1>Log Display</h1>
                </div>
                <div class="large-5 medium-5 cell">
                    <div class="header-form">
                      <form method="get">
                        <label>Select Log File: </label>
                        <select name="logfile" onchange="this.form.submit()">
			              <c:forEach items="${logFilenames}" var="logFilename">
			                <option value="${logFilename}" <c:if test="${logfile == logFilename}">selected="selected"</c:if>>${logFilename}</option>
			              </c:forEach>
                        </select>
                      </form>
                    </div>
                </div>
                <div class="large-12 medium-12 cell">
                    <div class="log" id="log">${log}</div>
                </div>
              </div>
            <script type="text/javascript">
                var logDisplay = document.getElementById('log');
                logDisplay.scrollTop = logDisplay.scrollHeight - logDisplay.clientHeight;
            </script>
    </jsp:body>
</t:template>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<t:template pageName="About" activeMenuItemNo="4">
    <jsp:body>
              <div class="grid-x grid-padding-x">
                <div class="large-12 cell">
                    <nav aria-label="You are here:" role="navigation">
                        <ul class="breadcrumbs">
                            <li><a href="<c:url value="/"/>">Home</a></li>
                            <li>
                                <span class="show-for-sr">Current: </span> About
                            </li>
                        </ul>
                    </nav>
                </div>
              </div>
              
              <div class="grid-x grid-padding-x">
                <div class="large-12 cell">
                  <h1>About <span class="page-title">LOMBuDa.at</span></h1>
                </div>
              </div>

              <div class="grid-x grid-padding-x">
                <div class="large-8 medium-8 cell">
                  <h5 class="subheader">Imprint &amp; disclosure according to &sect;25 Austrian Media Law</h5>
                  <div class="grid-x grid-padding-x">
                    <div class="large-2 medium-3 cell"><p><b>Media owner:</b></p></div>
                    <div class="large-10 medium-9 cell"><p>Paul Blasl</p></div>
                    <div class="large-2 medium-3 cell"><p><b>Media location:</b></p></div>
                    <div class="large-10 medium-9 cell"><p>Vienna, Austria</p></div>
                    <div class="large-2 medium-3 cell"><p><b>Contact:</b></p></div>
                    <div class="large-10 medium-9 cell"><p><a href="&#109;&#97;&#x69;&#x6c;&#x74;&#111;&#58;&#x77;&#x65;&#x62;&#x6d;&#97;&#115;&#x74;&#101;&#x72;&#64;&#x6c;&#x6f;&#x6d;&#98;&#x75;&#x64;&#x61;&#46;&#97;&#116;">&#x77;&#x65;&#98;&#x6d;&#x61;&#115;&#x74;&#x65;&#114;&#x40;&#x6c;&#111;&#109;&#x62;&#x75;&#x64;&#97;&#x2e;&#97;&#116;</a></p></div>
                    <div class="large-2 medium-3 cell"><p><b>Media purpose:</b></p></div>
                    <div class="large-10 medium-9 cell"><p>Technical prototype for applying Linked Data to Austrian municipal budgets</p></div>
                    <div class="large-12 medium-12 cell">
                        <p style="text-align: justify;">This website was originally developed and published in the year 2017 as part of a
                        Diploma Thesis in fulfilment of the requirements of the Master Programme in
                        Business Informatics at the <a href="http://www.informatik.tuwien.ac.at/english" target="_blank">Faculty of Informatics</a>
                        of the <a href="https://www.tuwien.ac.at/en" target="_blank">Vienna University of Technology</a>. More details on
                        this scientific work as well as a link to the work itself can be found on this and other parts of this website.<br/>
                        More legal notices can be found <a onclick="jumpTo('licence');">later</a> on this very page.</p>
                    </div>
                   </div>
                   
                  <hr />
                  
                  <h4>Technical information</h4>
                  
                  <h6>General Application Architecture</h6>
                  <p style="text-align: justify">The actual implementation of this project's website is based on <a href="https://go.java" target="_blank">Java</a> 8 or,
                  to be more precise, a respective web application. The needed runtime environment is provided by Apache Software Foundation’s
                  <a href="http://tomcat.apache.org" target="_blank">Tomcat</a> in its Version 8, where all of the following components beside the actual website are hosted.
                  This runtime environment acts as a web server and – for the time being – is operating from a virtual machine in the <span data-tooltip aria-haspopup="true"
                  class="has-tip bottom" data-disable-hover="false" tabindex="10" title="Amazon Web Services">AWS</span> cloud.
                  </p>
                  <div style="text-align: center;margin-bottom: 0.8rem;">
                    <img src="img/architecture.png" alt="Overall software architecture of LOMBuDa.at"/>
                  </div>
                  <p style="text-align: justify">In the depicted figure above, the overall software architecture of this project with all important components is shown
                  in an abstract way within this environment: <a onclick="jumpTo('jena');">Apache Fuseki</a> as <span data-tooltip aria-haspopup="true" class="has-tip bottom" 
                  data-disable-hover="false" tabindex="11" title="SPARQL Protocol and RDF Query Language">SPARQL</span> server with an integrated, persistent <span
                  data-tooltip aria-haspopup="true" class="has-tip bottom" data-disable-hover="false" tabindex="12"
                  title="a purpose-built database for the storage and retrieval of RDF statements">triplestore</span> is connected with this project's application via
                  its full-blown SPARQL endpoint. <a onclick="jumpTo('pubby');">Pubby</a> in turn serves as Linked Data frontend of this application, while the application
                  itself encapsulates a public <a href="<c:url value="/sparql"/>">SPARQL endpoint</a> as well as the means to automatically generate authentic
                  <a href="<c:url value="/budget-profiles"/>">budget profiles</a> with the underlying, provided data on this website.</p>
                  
                  <h6>Used third-party applications &amp; libraries</h6><div id="third-party-apps" style="position: relative; top: -5.5rem;"></div>
                  <p style="text-align: justify">
                    This section should be a courtesy "one-stop" summary of the third party software used in this web application. 
                    Links provided may, however, be disabled or otherwise become usable over time for reasons outside of this website's control.
                  </p>

                  <h6 class="subheader">Apache Jena &amp; Fuseki</h6><div id="jena" style="position: relative; top: -5.5rem;"></div>
                  <p style="text-align: justify"><img style="float: right; margin-left: 1rem;" alt="Apache Jena" src="img/apache-jena.png"/>
                    For the actual implementation of <a href="<c:url value="/#linked-data"/>">Linked Data</a>, handling of this <span data-tooltip aria-haspopup="true"
                    class="has-tip bottom" data-disable-hover="false" tabindex="20" title="Resource Description Framework">RDF</span> data in particular as well as
                    for building a Semantic Web application in general, the popular Open Source framework called “<a href="http://jena.apache.org" target="_blank">Jena</a>”
                    by the Apache Software Foundation is used. This framework comprises various technical components needed for full-blown Linked Data applications – nearly
                    all of them are also required for the implementation of this project. In a nutshell, these components are:</p>
                  <ul class="small" style="margin-bottom: 0.4rem;">
                      <li>an <i>RDF API</i> (Jena’s core API for parsing, processing and serialisation of RDF graphs)</li>
                      <li><i>ARQ</i> (a SPARQL query engine compliant with its most recent specification version 1.1)</li>
                      <li><i>TDB</i> (a native, high performance and persistent <span data-tooltip aria-haspopup="true" class="has-tip bottom" data-disable-hover="false"
                          tabindex="21" title="a purpose-built database for the storage and retrieval of RDF statements">triplestore</span> for RDF graphs)</li>
                      <li><i>Fuseki</i> (depending on its configuration, a standalone SPARQL server or an independent web application – incorporating TDB as data store and
                          providing a full-blown <a href="<c:url value="/sparql"/>">SPARQL endpoint</a>)</li>
                      <li>A <i>ontology API</i> as well as an <i>inference API</i> including a rule based Semantic Web <i>reasoner</i></li>
                  </ul>
                  <p style="text-align: justify">All listed components except the last one, which – as a Semantic Web technology – goes beyond the scope of this project,
                    is used in the technical implementation.
                    Additionally, beside other syntaxes that can be integrated dynamically by the individual applications, Jena’s RDF API is able to read and write, or can be
                    used for both parsing and serialisation, respectively, with a wide variety of RDF syntaxes:</p>
                  <ul class="small" style="margin-bottom: 0.4rem;list-style-type: none;columns: 3;">
                      <li>Turtle</li>
                      <li>RDF/XML</li>
                      <li>N-Triples</li>
                      <li>JSON-LD</li>
                      <li>RDF/JSON</li>
                      <li>TriG</li>
                      <li>TriX</li>
                      <li>N-Quads</li>
                      <li>RDF Binary</li>
                  </ul>
                  <p style="text-align: justify">Jena is maintained and developed by the Apache Software Foundation as open source software since November 2010
                  down to the present day and can be used under the <a href="http://www.apache.org/licenses/LICENSE-2.0" target="_blank">Apache Licence, Version 2.0</a>.</p>

                  <h6 class="subheader">Pubby</h6><div id="pubby" style="position: relative; top: -5.5rem;"></div>
                  <p style="text-align: justify">In order to provide Linked Open Data as such, <span data-tooltip aria-haspopup="true" class="has-tip bottom"
                  data-disable-hover="false" tabindex="30" title="the ability to obtain something, at which a pointer (the URI) points">dereferenceable</span>, useful
                  <span data-tooltip aria-haspopup="true" class="has-tip bottom" data-disable-hover="false" tabindex="31"
                  title="Uniform Resource Identifier">URI</span>s are needed. The independent Linked Data frontend for SPARQL endpoints called “<a
                  href="http://wifo5-03.informatik.uni-mannheim.de/pubby" target="_blank">Pubby</a>” is used by this project for this purpose as well as for separation of
                  RDF’s URIs <i>identifying</i> resources from those <i>describing</i> the latter – including a respective technical concept and implementation for
                  redirecting requests from the former to the latter with sophisticated <a href="https://en.wikipedia.org/wiki/Content_negotiation"
                  target="_blank">content negotiation</a>.
                  </p>
                  <div style="text-align: center;margin-bottom: 0.8rem;">
                    <img src="img/pubby-architecture.png" alt="Data provision architecture of Pubby"/>
                  </div>
                  <p style="text-align: justify">As depicted above, this Linked Data frontend makes URIs within a given namespace of Linked Data, which is provided by a
                  third-party, local or remote <a href="<c:url value="/sparql"/>">SPARQL endpoint</a>, <span data-tooltip aria-haspopup="true" class="has-tip bottom"
                  data-disable-hover="false" tabindex="32" title="the ability to obtain something, at which a pointer (the URI) points">dereferenceable</span>. Pubby itself
                  is implemented as a Java web application that passes client request towards a given webserver domain or URI to the mentioned SPARQL endpoint, and responses
                  – via respective <a href="https://en.wikipedia.org/wiki/Content_negotiation" target="_blank">content negotiation</a> – appropriately to the original request
                  directly, while other SPARQL clients can still freely access the very same SPARQL endpoint Pubby uses as well. This enables transparent publication of
                  Linked Data of a given domain incorporating all the requirements incurred by the definition of <a href="http://5stardata.info"
                  target="_blank">Linked Open Data</a>.</p>
                  <p style="text-align: justify">Pubby was originally developed by Richard Cyganiak (with the involvement of Chris Bizer) at the <a
                  href="http://www.fu-berlin.de" target="_blank">Free University of Berlin</a> starting in 2007. It is available as a <a 
                  href="https://github.com/cygri/pubby" target="_blank">GitHub project</a> as well as a open source software under the terms of the
                  <a href="http://www.apache.org/licenses/LICENSE-2.0" target="_blank">Apache Licence, Version 2.0</a>. Pubby was originally also used by the famous <a
                   href="http://wiki.dbpedia.org" target="_blank">DBpedia project</a>.</p>

                  <h6 class="subheader">Application Libraries</h6><div id="libraries" style="position: relative; top: -5.5rem;"></div>
                  <table class="small">
                        <thead>
                            <tr>
                                <th class="no-wrap">Library Name</th>
                                <th class="no-wrap">Version</th>
                                <th>Brief Description</th>
                                <th>Project Page</th>
                                <th class="no-wrap">Licence</th>
                                <th>Licence Terms</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="no-wrap">Spring Framework</td>
                                <td class="no-wrap">4.3.11</td>
                                <td>An open source application framework and inversion of control container for the Java platform.</td>
                                <td><a href="http://projects.spring.io/spring-framework/" target="_blank">LINK</a></td>
                                <td class="no-wrap">Apache 2.0</td>
                                <td><a href="http://www.apache.org/licenses/LICENSE-2.0" target="_blank">LINK</a></td>
                            </tr>
                            <tr>
                                <td class="no-wrap">JSTL</td>
                                <td class="no-wrap">1.2</td>
                                <td>The JavaServer Pages Standard Tag Library (JSTL) is a component of the Java EE Web application development platform. It extends the JSP specification by adding a tag library of JSP tags for common tasks.</td>
                                <td><a href="https://jstl.java.net/" target="_blank">LINK</a></td>
                                <td class="no-wrap">CDDL 1.1</td>
                                <td><a href="https://javaee.github.io/jstl-api/LICENSE" target="_blank">LINK</a></td>
                            </tr>
                            <tr>
                                <td class="no-wrap">YASGUI</td>
                                <td class="no-wrap">2.7.5</td>
                                <td>Yet Another Sparql GUI (YASGUI) consists of the JavaScript libraries YASQE (a SPARQL Query Editor) and YASR (a SPARQL Resultset Visualizer) for easy to use SPARQL frontend integration for third-party applications.</td>
                                <td><a href="http://doc.yasgui.org/" target="_blank">LINK</a></td>
                                <td class="no-wrap">MIT License</td>
                                <td><a href="https://github.com/OpenTriply/YASGUI.YASQE/blob/gh-pages/license.txt" target="_blank">LINK</a></td>
                            </tr>
                            <tr>
                                <td class="no-wrap">Apache Commons FileUpload</td>
                                <td class="no-wrap">1.3.3</td>
                                <td>The Apache Commons FileUpload package robust, high-performance library for processing file uploads to web applications.</td>
                                <td><a href="https://commons.apache.org/proper/commons-fileupload/" target="_blank">LINK</a></td>
                                <td class="no-wrap">Apache 2.0</td>
                                <td><a href="http://www.apache.org/licenses/LICENSE-2.0" target="_blank">LINK</a></td>
                            </tr>
                            <tr>
                                <td class="no-wrap">Apache Commons CSV</td>
                                <td class="no-wrap">1.5</td>
                                <td>The Apache Commons CSV package reads and writes files in variations of the Comma Separated Value (CSV) format.</td>
                                <td><a href="http://commons.apache.org/proper/commons-csv/" target="_blank">LINK</a></td>
                                <td class="no-wrap">Apache 2.0</td>
                                <td><a href="http://www.apache.org/licenses/LICENSE-2.0" target="_blank">LINK</a></td>
                            </tr>
                            <tr>
                                <td class="no-wrap">GeoNames Java Client</td>
                                <td class="no-wrap">1.1.13</td>
                                <td>The GeoNames Java library helps to easily access the GeoNames web services with Java.</td>
                                <td><a href="http://www.geonames.org/source-code/" target="_blank">LINK</a></td>
                                <td class="no-wrap">Apache 2.0</td>
                                <td><a href="http://www.apache.org/licenses/LICENSE-2.0" target="_blank">LINK</a></td>
                            </tr>
                            <tr>
                                <td class="no-wrap">log4j</td>
                                <td class="no-wrap">2.9.1</td>
                                <td>A Java-based logging utility framework.</td>
                                <td><a href="https://logging.apache.org/log4j/2.x/" target="_blank">LINK</a></td>
                                <td class="no-wrap">Apache 2.0</td>
                                <td><a href="http://www.apache.org/licenses/LICENSE-2.0" target="_blank">LINK</a></td>
                            </tr>
                            <tr>
                                <td class="no-wrap">SLF4J</td>
                                <td class="no-wrap">1.7.25</td>
                                <td>Simple Logging Facade for Java (SLF4J) provides a Java logging API by means of a simple facade pattern. The underlying logging backend is determined at runtime by adding the desired binding to the classpath.</td>
                                <td><a href="http://www.slf4j.org/" target="_blank">LINK</a></td>
                                <td class="no-wrap">MIT License</td>
                                <td><a href="http://www.slf4j.org/license.html" target="_blank">LINK</a></td>
                            </tr>
                        </tbody>
                   </table>
                  
                  <h6 class="subheader">Frontend Framework</h6><div id="frontend-headline" style="position: relative; top: -5.5rem;"></div>
                  <img style="float: right; margin-left: 1rem; margin-top: -2rem;" alt="Frontend Framework" src="img/foundation.png"/>
                  <p style="text-align: justify">
                    This website or the layout of this website, respectively, is based on Version 6 of the <a href="https://foundation.zurb.com/sites.html" target="_blank">Foundation Framework</a> for responsive frontends.
                    It is <a href="https://en.wikipedia.org/wiki/MIT_License" target="_blank">MIT licensed</a>, and free to use and modify. Foundation is widely adopted and contains various design templates in CSS, 
                    enhanced with its own JavaScript-library. 
                  </p>
                  
                  <hr />
                  
                  <h4>Legal information</h4><div id="licence" style="position: relative; top: -6.5rem;"></div>
                  
                  <h6>Licence &amp; Terms of Use</h6>
                  <p style="text-align: justify;"><img style="float: right; margin-left: 1rem;" alt="Creative Commons Attribution" src="img/cc.by.png"/>
                    This work in its entirety with its provided texts and information is published and licensed under the 
                        <a rel="license" href="http://creativecommons.org/licenses/by/4.0/" target="_blank">Creative Commons Attribution 4.0 International License</a> (CC-BY).
                    In order to cover legal aspects for data and databases in more appropriate way, all processed budgetary data and its compilation provided on this website and, 
                    especially, under the sub-domain <a href="http://data.lombuda.at/" target="_blank">data.lombuda.at</a> is made available within the terms of the 
                        <a rel="license" href="http://opendatacommons.org/licenses/by/1.0" target="_blank">Open Data Commons Attribution License</a> (ODC-BY). 
                        Beyond that, please note following comments with respect to the terms of (re-)use of content and data of this website:</p>
                  <ul class="small" style="text-align: justify;">
                      <li>Due to its licensing with CC-BY and ODC-BY, all users are free to copy or distribute the data, make it publicly accessible, use it commercially, as well as to adapt and 
                      edit the material or content provided on this website. However, under the mentioned licences, appropriate credit must be given to the author's name or this website, if doing so.</li>
                      <li>Parts of this website's implementation and its backend as well as the original data sources for provided municipal budgets were published originally by other parties under their own licences. 
                      These parties and licences are given within the respective datasets slices (like <a href="http://data.lombuda.at/OGHD/2015/90001/RA" target="_blank">here</a>) or at <a onclick="jumpTo('third-party-apps');">this part</a> of this very page 
                      for all used technologies and applications. However, it shall be noted that all reused content is deemed to be compatible to the mentioned licences of this very website (CC-BY &amp; ODC-BY) and, 
                      therefore, the respective third-party licences do not affect the licensing of this website in any way.</li>
                      <li>References and links to other Internet offers were selected with circumspection. However, their content, accuracy, up-to-dateness, completeness and availability are not in the sphere of influence 
                      of this website and its owner and so, no liability in this regard can be assumed at this place.</li>
                      <li>Furthermore, no guarantee or any liability is given for linked data or applications, in particular with regard to 
                      their up-to-dateness, suitability for a specific purpose or freedom from viruses.</li>
                      <li>The website will not be available during necessary, unannounced or announced maintenance periods or in the event of inevitable, technical issues.</li>
                      <li>All information on or from the provision of the data and applications shall be without warranty or indemnity.</li>
                      <li>The data may not be used for applications or publications that contravene applicable law or standards of decency.</li>
                  </ul>
                
                  
                  <h6>General Disclaimer</h6>
                  <p style="text-align: justify;">
                    The contents and data published on this website are the result of circumspect research; the technical services are programmed as best possible, in good faith and for general information purpose. 
                    However, there is no warrant about the completeness, reliability and accuracy of the result. All information is provided without any warranty. 
                    Any liability for damages resulting from the use of these contents or services shall be excluded and strictly at your own risk.
                  </p>
                  <p style="text-align: justify;">
                    For direct or indirect referrals to external websites (links) outside of this website's responsibility, liability only arises if its owner is aware of the relevant contents and if he would be in 
                    a position to prevent use in the event of unlawful contents and he can be reasonably expected to do so. Therefore, he hereby expressly declares that no illegal contents were recognisable on the 
                    linked websites at any time when setting up the links. This website and its owner has no influence whatsoever on the current and future design or on the contents of the linked websites. He therefore 
                    expressly dissociates itself from all contents of all linked websites amended after the links were set. This shall be applicable to all links and referrals provided on this website and its sub-domains. 
                    The provider of the website to which a link refers shall bear sole responsibility for any illegal, inaccurate or incomplete contents, particularly for any damage resulting from the use or non-use 
                    of information provided in such a manner, and not the party merely referring to the respective publication via links.
                  </p>
                  
                  <h6>Final clauses</h6>
                  <p style="text-align: justify;">
                    Should parts or individual formulations of any texts on this page not, no longer comply or not completely comply with the applicable laws, 
                    the contents and validity of the remaining parts shall remain unaffected.
                    In place of an invalid provision or to remedy a deficiency, an appropriate, legally admissible wording shall apply that most closely approximates 
                    what would have been the intended meaning and purpose of these terms of use, if the partial invalidity or deficiency had been known.<br/>
                    The use of all services, content and information on this website is governed exclusively by Austrian law.
                  </p>            
                </div>

                <div class="large-4 medium-4 cell">
                  <div class="callout">
                    <h5>Can I (re-)use this?</h5>
                    <p style="text-align: justify;">This project website and its content as well as all data available via the <a href="<c:url value="/sparql"/>" target="_blank">SPARQL endpoint</a> or the respective 
                    <a href="http://data.lombuda.at/" target="_blank">Linked Data domain</a> is licensed in an open way. Therefore, anyone can freely access, use, modify and share it for any purpose as long as 
                    proper attribution is made in any redistribution.</p>
                    <a onclick="jumpTo('licence');" class="small button">Learn more about this website's licensing</a>
                  </div>
                  <div class="callout">
                    <h5>Can I have the source code?</h5>
                    <p style="text-align: justify;">In order to support others in an appropriate way with the knowledge gained from the development and the research for this project, this website's source code 
                    as well as the one of its <a href="http://data.lombuda.at/" target="_blank">Linked Data frontend</a> is available as an open source project on <a href="http://gitlab.com/" target="_blank">GitLab</a>. 
                    It can be used freely by anyone under the same conditions like for all other parts of this website.</p>
                    <a href="<c:url value="https://gitlab.com/axio/lombuda"/>" target="_blank" class="small button">Go to this website's GitLab project</a>
                  </div>
                  <div class="callout">
                    <h5>Why is this website so pretty?</h5>
                    <p style="text-align: justify;">All pages of this project's website are designed by the means of templates from the 
                    <a href="https://foundation.zurb.com/sites.html" target="_blank">Foundation Framework</a>, which provides a coherent CSS 
                    and JavaScript compilation for responsive frontends. It is licensed - as all of parts of this project - in an open way.</p>
                    <a onclick="jumpTo('frontend-headline');" class="small button">Learn more about the Foundation Framework</a>
                  </div>
                </div>
              </div>
    </jsp:body>
</t:template>
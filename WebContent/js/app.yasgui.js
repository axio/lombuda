var yasgui = YASGUI(document.getElementById("yasgui-container"), {
	persistencyPrefix: false,
	yasqe:
	{
		value: 'PREFIX rdf:      <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\nPREFIX rdfs:     <http://www.w3.org/2000/01/rdf-schema#>\nPREFIX owl:      <http://www.w3.org/2002/07/owl#>\nPREFIX xsd:      <http://www.w3.org/2001/XMLSchema#>\nPREFIX skos:     <http://www.w3.org/2004/02/skos/core#>\nPREFIX dct:      <http://purl.org/dc/terms/>\nPREFIX cc:       <http://creativecommons.org/ns#>\nPREFIX qb:       <http://purl.org/linked-data/cube#>\nPREFIX dbr:      <http://dbpedia.org/resource/>\nPREFIX sdmx-subject:     <http://purl.org/linked-data/sdmx/2009/subject#>\nPREFIX sdmx-attribute:   <http://purl.org/linked-data/sdmx/2009/attribute#>\nPREFIX sdmx-code:        <http://purl.org/linked-data/sdmx/2009/code#>\nPREFIX lombuda:          <http://data.lombuda.at/ontology/> \nPREFIX lombuda-code:     <http://data.lombuda.at/ontology/code/>\nPREFIX lombuda-slice:    <http://data.lombuda.at/ontology/slice/> \nPREFIX lombuda-fs-code:  <http://data.lombuda.at/ontology/code/fiscal-states/>\nPREFIX lombuda-ft-code:  <http://data.lombuda.at/ontology/code/fiscal-types/>\nPREFIX lombuda-bi-code:  <http://data.lombuda.at/ontology/code/VRV/budgetary-indicators/>\nPREFIX lombuda-fc-code:  <http://data.lombuda.at/ontology/code/VRV/approaches/>\nPREFIX lombuda-ec-code:  <http://data.lombuda.at/ontology/code/VRV/accounts/>\nPREFIX lombudata:        <http://data.lombuda.at/>\n\nSELECT * WHERE {\n   ?subject ?predicate ?object .\n   FILTER (?subject = lombudata:OGHD)\n}',
		backdrop:false,
		createShareLink:null,
		height: 341,
		sparql:
		{
			endpoint:window.location.href,
			requestMethod:'GET'
		}
	},
	yasr: {
		outputPlugins: ["error", "boolean", "table", "rawResponse"],
		output: 'table'
	}
}
);

yasgui.current().yasr.options.getUsedPrefixes = yasgui.current().yasqe.getPrefixesFromQuery();

var queryDisplay = document.getElementsByClassName('CodeMirror-scroll')[0];
queryDisplay.scrollTop = queryDisplay.scrollHeight - queryDisplay.clientHeight;
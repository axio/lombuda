// general initialisation functions
$(document).foundation();

jQuery(document).ready(function() {
    jQuery("time.timeago").timeago();
});
		
// application specific functions
function jumpTo(elementId) {
	if(elementId != null && elementId !== undefined)
		document.getElementById(elementId).scrollIntoView({ behavior: 'smooth', block: 'start'});
	else
		document.body.scrollIntoView({ behavior: 'smooth', block: 'start'});
	
	if(window.getSelection) {
	  if(window.getSelection().empty) {  // Chrome
	    window.getSelection().empty();
	  } else if (window.getSelection().removeAllRanges) {  // Firefox
	    window.getSelection().removeAllRanges();
	  }
	} else if(document.selection) {  // IE?
	  document.selection.empty();
	}
	
	return false;
}

function hasScrollBar() {
	return document.body.scrollHeight > document.body.clientHeight + 200;
}

function showBtt() {
	var btt = document.getElementById('btt');
	if(btt != null && btt !== undefined )
		if(!hasScrollBar()) {
			btt.style.display = 'none';
		} else {
			btt.style.display = 'inline';
		}
}
showBtt();

var headlineAnchors = ["","headline-anchor","municipal-budgets","linked-data","open-data"];
var currentAnchor = 0;
var running = false;
function doSomethingFunny() {
	if(running) {
		console.log("Okay.. I'll stop.");
		running = false;
		document.getElementById('funnyStatus').style.display = 'none';

		return;
	}

	console.log("Start doin' something funny..! =)");
	running = true;
	currentAnchor = 0;
	document.getElementById('funnyStatus').style.display = '';

	doFunnyStuff();
}

var funnyWaitTimes = [2500,1700,1500,1600,1700];
function doFunnyStuff() {
	if(running) {
		var currentAnchorId = headlineAnchors[currentAnchor];
		var funnyWaitTime = funnyWaitTimes[currentAnchor];
		console.log("Funny step #"+currentAnchor+": "+currentAnchorId+" ("+funnyWaitTime+"ms)");

		if(currentAnchorId)
			jumpTo(headlineAnchors[currentAnchor]);
		else
			jumpTo();

		if(currentAnchor < headlineAnchors.length - 1)
			currentAnchor++;
		else
			currentAnchor = 0;

		setTimeout(doFunnyStuff, funnyWaitTime);
	}
}